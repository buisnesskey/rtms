﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web.Hosting;
using log4net;

namespace RTMS.Common
{
    public static class ExceptionHandler
    {
        static private readonly object LockLogObj = new object();
        private static log4net.ILog Log { get; set; }
        static ILog log = log4net.LogManager.GetLogger(typeof(ExceptionHandler));      
        /// <summary>
        /// This method writes Log..LogPath app settings must be defined in conf file
        /// </summary>
        /// <param name="ex">exception object</param>
        /// <param name="functionName">Function Name</param>
        public static void WriteLog(Exception ex, string functionName)
        {
            try
            {
                //string filename = "\\Logs\\date.txt";
                //string directoryName = Path.GetDirectoryName(filename);
                //if (!Directory.Exists(directoryName))
                //{
                //    Directory.CreateDirectory(directoryName);
                //}
                //File.AppendAllText(filename, "Hi");
                //
                //File.AppendAllText(HostingEnvironment.MapPath("~/temp/errors.txt"), "\n" + ex.Message);
                //File.AppendAllText(HostingEnvironment.ApplicationPhysicalPath+"/temp/errors.txt", "\n" + ex.Message);
                // Get File Path and File Name..
                #region Read FilePath from App.Config
                //string filePath = ConfigurationManager.AppSettings["LogPath"];
                //string fileName = DateTime.Now.ToString("yyyy_MM_dd") + "_log.txt";
                string filePath = @"H:\hshome\rtmsadmin\procurement.rtms.com.eg\temp\logs\";// ConfigurationManager.AppSettings["LogPath"] + DateTime.Now.ToString("dd-MM-yyyy") + "\\";
                string fileName = DateTime.Now.ToString("HH") + ".txt";
                #endregion

                #region Check If Directory Exist
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                #endregion

                // Initialize Empty Message..
                string message = string.Empty;
                // Write Time..
                message += "Time: " + DateTime.Now + Environment.NewLine;
                // Write Location..
                message += "Location: ";


                // Function Name
                message += functionName + Environment.NewLine;
                // Error Description
                message += "Message: " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Message: " + ex.InnerException.Message + Environment.NewLine;
                }
                message += "StackTrace: " + ex.StackTrace + Environment.NewLine;
                message += "---------------------------------------------------------------" + Environment.NewLine;
                
                // start writing on file..
                lock (LockLogObj)
                {
                    File.AppendAllText(filePath + fileName, message);
                    log.Error("ERROR :\t" + message);
                }
            }
            catch (Exception exception)
            {
                var method = MethodBase.GetCurrentMethod();
                var methodFullNameWithParams = string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray()));
                //WriteLog(exception, methodFullNameWithParams);
            }
        }

        /// <summary>
        /// this fuction is to write log not exception overloaded
        /// </summary>
        /// <param name="ex">string exception or param that you want to write</param>
        /// <param name="functionName">the inner function that you use this function on</param>
        public static void WriteLog(string ex, string functionName)
        {
            try
            {
                // Get File Path and File Name..
                #region Read FilePath from App.Config
                //string filePath = ConfigurationManager.AppSettings["LogPath"];
                //string fileName = DateTime.Now.ToString("yyyy_MM_dd") + "_log.txt";
                string filePath = ConfigurationManager.AppSettings["LogPath"] + DateTime.Now.ToString("dd-MM-yyyy") + "\\";
                string fileName = DateTime.Now.ToString("HH") + ".txt";

                #endregion
                #region Check If Directory Exist
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                #endregion
                // Initialize Empty Message..
                string message = string.Empty;
                // Write Time..
                message += "Time: " + DateTime.Now + Environment.NewLine;
                // Write Location..
                message += "Location: ";


                // Function Name
                message += functionName + Environment.NewLine;
                // Error Description
                message += "Message: " + ex + Environment.NewLine;
                message += "---------------------------------------------------------------" + Environment.NewLine;
                // start writing on file..
                lock (LockLogObj)
                {
                    File.AppendAllText(filePath + fileName, message);
                }
            }
            catch (Exception exception)
            {
                var method = MethodBase.GetCurrentMethod();
                var methodFullNameWithParams = string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray()));
                //WriteLog(exception, methodFullNameWithParams);
            }
        }

        public static void WriteLog(DirectoryInfo directoryInfo, Exception ex, string functionName)
        {
            try
            {
                // Get File Path and File Name..
                #region Read FilePath from App.Config
                //string filePath = ConfigurationManager.AppSettings["LogPath"];
                //string fileName = DateTime.Now.ToString("yyyy_MM_dd") + "_log.txt";
                #endregion

                #region Check If Directory Exist
                if (directoryInfo == null)
                {
                    WriteLog("Invalid paramter directoryInfo is NULL", functionName);
                    WriteLog(ex, functionName);
                }
                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                FileInfo fileInfo = new FileInfo(directoryInfo + DateTime.Now.ToString("HH") + ".txt");
                if (!fileInfo.Exists)
                {
                    FileStream fs = fileInfo.Create();
                    fs.Close();
                }

                string logTypeInfo = "Error      ";
                #endregion

                // Initialize Empty Message..
                string message = string.Empty;
                // Write Time..
                message += logTypeInfo + " " + "Time: " + DateTime.Now + Environment.NewLine;
                // Write Location..
                message += "Location: ";


                // Function Name
                message += functionName + Environment.NewLine;
                // Error Description
                message += "Message: " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception Message: " + ex.InnerException.Message + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "  Inner Inner Exception Message: " + ex.InnerException.InnerException.Message + Environment.NewLine;
                        if (ex.InnerException.InnerException.InnerException != null)
                        {
                            message += "  Inner Inner Inner Exception Message: " + ex.InnerException.InnerException.InnerException.Message + Environment.NewLine;
                        }
                    }
                }
                if (!string.IsNullOrWhiteSpace(ex.StackTrace))
                {
                    message += "StackTrace: " + ex.StackTrace + Environment.NewLine;
                }
                message += "---------------------------------------------------------------" + Environment.NewLine;
                // start writing on file..
                lock (LockLogObj)
                {
                    File.AppendAllText(fileInfo.FullName, message);
                }
            }
            catch (Exception exception)
            {
                var method = MethodBase.GetCurrentMethod();
                var methodFullNameWithParams = string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray()));
                //WriteLog(exception, methodFullNameWithParams);
            }
        }
        public static void WriteMessage(DirectoryInfo directoryInfo, RTMS.Common.Enums.LogTypeEnumList logType, string message, string functionName)
        {
            try
            {
                // Get File Path and File Name..
                #region Read FilePath from App.Config
                //string filePath = ConfigurationManager.AppSettings["LogPath"];
                string fileName = DateTime.Now.ToString("HH") + "_log.txt";
                #endregion

                #region Check If Directory Exist
                if (directoryInfo == null)
                {
                    WriteLog("Invalid paramter directoryInfo is NULL", functionName);
                    WriteLog(message, functionName);
                }
                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                FileInfo fileInfo = new FileInfo(directoryInfo + fileName);
                if (!fileInfo.Exists)
                {
                    FileStream fs = fileInfo.Create();
                    fs.Close();
                }

                string logTypeInfo = string.Empty;
                switch (logType)
                {
                    case RTMS.Common.Enums.LogTypeEnumList.Information:
                        logTypeInfo = "Information  ";
                        break;
                    case RTMS.Common.Enums.LogTypeEnumList.Warning:
                        logTypeInfo = "Warning    ";
                        break;
                    case RTMS.Common.Enums.LogTypeEnumList.Error:
                        logTypeInfo = "Error      ";
                        break;
                    default:
                        break;
                }
                #endregion

                // Initialize Empty Message..
                string mainMessage = string.Empty;
                // Write Time..
                mainMessage += logTypeInfo + " " + "Time: " + DateTime.Now + Environment.NewLine;
                // Write Location..
                mainMessage += "Location: ";


                // Function Name
                mainMessage += functionName + Environment.NewLine;
                // Error Description
                mainMessage += "Message: " + message + Environment.NewLine;

                mainMessage += "---------------------------------------------------------------" + Environment.NewLine;
                // start writing on file..
                lock (LockLogObj)
                {
                    File.AppendAllText(directoryInfo.FullName + fileName, mainMessage);
                }
            }
            catch (Exception exception)
            {
                var method = MethodBase.GetCurrentMethod();
                var methodFullNameWithParams = string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray()));
                //WriteLog(exception, methodFullNameWithParams);
            }
        }
        public static void WriteMessage(DirectoryInfo directoryInfo, RTMS.Common.Enums.LogTypeEnumList logType, string message)
        {
            var method = MethodBase.GetCurrentMethod();
            var methodFullNameWithParams = string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray()));
            try
            {
                if (directoryInfo == null)
                {
                    WriteLog("Invalid paramter directoryInfo is NULL", methodFullNameWithParams);
                }
                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                FileInfo fileInfo = new FileInfo(directoryInfo + DateTime.Now.ToString("HH") + "_log.txt");
                if (!fileInfo.Exists)
                {
                    FileStream fs = fileInfo.Create();
                    fs.Close();
                }

                string logTypeInfo = string.Empty;
                switch (logType)
                {
                    case RTMS.Common.Enums.LogTypeEnumList.Information:
                        logTypeInfo = "Information  ";
                        break;
                    case RTMS.Common.Enums.LogTypeEnumList.Warning:
                        logTypeInfo = "Warning    ";
                        break;
                    case RTMS.Common.Enums.LogTypeEnumList.Error:
                        logTypeInfo = "Error      ";
                        break;
                    default:
                        break;
                }
                string content = logTypeInfo + " " + DateTime.Now.ToString("dd-MM-yy hh:mm:ss tt") + Environment.NewLine;
                content += message + Environment.NewLine;
                content += "---------------------------------------------------------------" + Environment.NewLine;
                lock (LockLogObj)
                {
                    File.AppendAllLines(fileInfo.FullName, new string[] { content });
                }
            }
            catch (Exception exception)
            {
                //WriteLog(exception, methodFullNameWithParams);
            }
        }
        public static void WriteMessage(RTMS.Common.Enums.LogTypeEnumList logType, string message, string functionName)
        {
            try
            {
                // Get File Path and File Name..
                #region Read FilePath from App.Config
                string filePath = ConfigurationManager.AppSettings["LogPath"];
                string fileName = DateTime.Now.ToString("HH") + "_log.txt";
                #endregion
                #region Check If Directory Exist
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                #endregion

                string logTypeInfo = string.Empty;
                switch (logType)
                {
                    case RTMS.Common.Enums.LogTypeEnumList.Information:
                        logTypeInfo = "Information  ";
                        break;
                    case RTMS.Common.Enums.LogTypeEnumList.Warning:
                        logTypeInfo = "Warning    ";
                        break;
                    case RTMS.Common.Enums.LogTypeEnumList.Error:
                        logTypeInfo = "Error      ";
                        break;
                    default:
                        break;
                }

                // Initialize Empty Message..
                message = string.Empty;
                // Write Time..
                message += logTypeInfo + " " + "Time: " + DateTime.Now + Environment.NewLine;
                // Write Location..
                message += "Location: ";


                // Function Name
                message += functionName + Environment.NewLine;
                // Error Description
                message += "Message: " + message + Environment.NewLine;

                message += "---------------------------------------------------------------" + Environment.NewLine;
                // start writing on file..
                lock (LockLogObj)
                {
                    File.AppendAllText(filePath + fileName, message);
                }
            }
            catch (Exception exception)
            {
                var method = MethodBase.GetCurrentMethod();
                var methodFullNameWithParams = string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray()));
                //WriteLog(exception, methodFullNameWithParams);
            }
        }
        
    }
}