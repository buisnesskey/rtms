﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RTMS.Common
{
    public class Enums
    {
        public enum LogTypeEnumList
        {
            Information,
            Warning,
            Error
        }

        public enum SystemRoleEnum
        {
            Super = 1,
            Admin = 2,
            Customer = 3,
            Supervisor = 4
        }

        public enum StateEnum
        {
            Pending = 1,
            Waiting = 2,
            Processing = 3,
            Complete = 4,
            Canceled = 5,
            Alternative = 6
        }

        public enum GroupByEnum
        {
            Year = 1,
            Month = 2,
            Date = 3
        }

        public enum SortDirection
        {
            Ascending = 1,
            Descending = 2
        }

        public enum CurrencyEnum
        {
            EGP = 1,
            USD = 2,
            EUR = 3,
            TL = 4
        }
    }
}