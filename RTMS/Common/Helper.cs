﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace RTMS.Common
{
    public class Helper
    {
        public string ResolveServerUrl(string serverUrl, bool forceHttps)
        {
            string newUrl = string.Empty;
            try
            {
                if (serverUrl.IndexOf("://") > -1)
                    return serverUrl;

                newUrl = serverUrl;
                Uri originalUri = System.Web.HttpContext.Current.Request.Url;
                newUrl = (forceHttps ? "https" : originalUri.Scheme) +
                    "://" + originalUri.Authority + newUrl;
                return newUrl;
            }
            catch (Exception exception)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(exception, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
            }
            return newUrl;
        }

        public DataTable ToDataTableWithHeaders<T>(List<T> items, List<string> excludeColumns, Dictionary<string, string> dictionaryPropsWithHeaders)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            try
            {

                #region  Get all the properties
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                #endregion

                #region Exclude Columns
                List<PropertyInfo> propsList = Props.ToList();
                propsList.RemoveAll(p => excludeColumns.Contains(p.Name));
                #endregion

                #region Create Data Table Columns Headers
                foreach (var prop in propsList)
                {
                    dataTable.Columns.Add(dictionaryPropsWithHeaders[prop.Name]);
                }
                #endregion

                #region Fill Datatable With data
                DataRow row;

                if (items != null)
                {
                    foreach (T item in items)
                    {
                        row = dataTable.NewRow();

                        for (int i = 0; i < propsList.Count; i++)
                        {
                            //inserting property values to datatable rows
                            row[dictionaryPropsWithHeaders[propsList[i].Name]] = propsList[i].GetValue(item, null);
                        }
                        dataTable.Rows.Add(row);
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return dataTable;
        }

        public bool GenerateExcelSheetWithoutDownload(DataTable dataTable, string exportingSheetPath, out string exportingFileName)
        {
            #region Validate the parameters and Generate the excel sheet
            bool returnValue = false;
            //exportingFileName = Guid.NewGuid().ToString() + ".xlsx";
            exportingFileName = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss-ffffff") + ".xlsx";

            try
            {
                string excelSheetPath = string.Empty;
                #region Check If The directory is exist
                if (!Directory.Exists(exportingSheetPath))
                {
                    Directory.CreateDirectory(exportingSheetPath);
                }

                excelSheetPath = exportingSheetPath + exportingFileName;
                FileInfo fileInfo = new FileInfo(excelSheetPath);
                #endregion

                #region Write stream to the file
                MemoryStream ms = DataToExcel(dataTable);
                byte[] blob = ms.ToArray();
                if (blob != null)
                {
                    using (MemoryStream inStream = new MemoryStream(blob))
                    {
                        FileStream fs = new FileStream(excelSheetPath, FileMode.Create);
                        inStream.WriteTo(fs);
                        fs.Close();
                    }
                }
                //FileStream fs = new FileStream(excelSheetPath, FileMode.OpenOrCreate);
                //ms.WriteTo(fs);
                //fs.Close();
                ms.Close();
                returnValue = true;
                #endregion

            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
            }
            return returnValue;
            #endregion
        }

        public MemoryStream DataToExcel(DataTable dt)
        {
            MemoryStream ms = new MemoryStream();
            try
            {
                using (dt)
                {
                    //IWorkbook workbook = new HSSFWorkbook();//Create an excel Workbook
                    IWorkbook workbook = new XSSFWorkbook();//Create an excel Workbook
                    ISheet sheet = workbook.CreateSheet();//Create a work table in the table
                    IRow headerRow = sheet.CreateRow(0); //To add a row in the table
                    foreach (DataColumn column in dt.Columns)
                        headerRow.CreateCell(column.Ordinal).SetCellValue(column.Caption);
                    int rowIndex = 1;

                    if (dt != null)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            IRow dataRow = sheet.CreateRow(rowIndex);
                            foreach (DataColumn column in dt.Columns)
                            {
                                if (Regex.IsMatch(row[column].ToString(), @"^-*[0-9,\.]+$"))
                                    dataRow.CreateCell(column.Ordinal).SetCellValue(double.Parse(row[column].ToString()));
                                else
                                    dataRow.CreateCell(column.Ordinal).SetCellValue(row[column].ToString());
                            }
                            rowIndex++;
                        }
                    }
                    workbook.Write(ms);
                    ms.Flush();
                    //ms.Position = 0;
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return ms;
        }
    }
}