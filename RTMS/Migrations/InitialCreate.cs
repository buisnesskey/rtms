// Decompiled with JetBrains decompiler
// Type: RTMS.Migrations.InitialCreate
// Assembly: RTMS, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0A16F994-A518-4FEB-A56D-42E16C403459
// Assembly location: D:\rtms\bin\RTMS.dll

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Infrastructure;
using System.Resources;

namespace RTMS.Migrations
{
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed class InitialCreate : DbMigration, IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(InitialCreate));

        string IMigrationMetadata.Id
        {
            get
            {
                return "201610051603273_InitialCreate";
            }
        }

        string IMigrationMetadata.Source
        {
            get
            {
                return (string)null;
            }
        }

        string IMigrationMetadata.Target
        {
            get
            {
                return this.Resources.GetString("Target");
            }
        }

        public override void Up()
        {
            this.CreateTable("dbo.ItemTypes", c => new
            {
                Id = c.Int(new bool?(false), true, new int?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Code = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Name = c.String(new bool?(false), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                IsActive = c.Boolean(new bool?(false), new bool?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null)
            }, (object)null).PrimaryKey(t => t.Id, (string)null, 1 != 0, (object)null);
            this.CreateTable("dbo.TypeUnits", c => new
            {
                Id = c.Int(new bool?(false), true, new int?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Code = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Name = c.String(new bool?(false), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                IsActive = c.Boolean(new bool?(false), new bool?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null)
            }, (object)null).PrimaryKey(t => t.Id, (string)null, 1 != 0, (object)null);
            this.CreateTable("dbo.OrderItems", c => new
            {
                Id = c.Int(new bool?(false), true, new int?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Remarks = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Description = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Quantity = c.Double(new bool?(false), new double?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                DeliveredQuantity = c.Double(new bool?(false), new double?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Attachment = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                State = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                TypeText = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                UnitText = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                BuyingPrice = c.Decimal(new bool?(false), new byte?((byte)18), new byte?((byte)2), new Decimal?(), (string)null, (string)null, (string)null, false, (IDictionary<string, AnnotationValues>)null),
                SellingPrice = c.Decimal(new bool?(false), new byte?((byte)18), new byte?((byte)2), new Decimal?(), (string)null, (string)null, (string)null, false, (IDictionary<string, AnnotationValues>)null),
                CreateDate = c.DateTime(new bool?(false), new byte?(), new DateTime?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                CompleteDate = c.DateTime(new bool?(), new byte?(), new DateTime?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                OrderId = c.Int(new bool?(false), false, new int?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                SupplierId = c.Int(new bool?(), false, new int?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                ItemTypeId = c.Int(new bool?(), false, new int?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                UnitId = c.Int(new bool?(), false, new int?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null)
            }, (object)null).PrimaryKey(t => t.Id, (string)null, 1 != 0, (object)null).ForeignKey("dbo.ItemTypes", t => t.ItemTypeId, 0 != 0, (string)null, (object)null).ForeignKey("dbo.Orders", t => t.OrderId, 1 != 0, (string)null, (object)null).ForeignKey("dbo.Suppliers", t => t.SupplierId, 0 != 0, (string)null, (object)null).ForeignKey("dbo.TypeUnits", t => t.UnitId, 0 != 0, (string)null, (object)null).Index(t => t.OrderId, (string)null, 0 != 0, 0 != 0, (object)null).Index(t => t.SupplierId, (string)null, 0 != 0, 0 != 0, (object)null).Index(t => t.ItemTypeId, (string)null, 0 != 0, 0 != 0, (object)null).Index(t => t.UnitId, (string)null, 0 != 0, 0 != 0, (object)null);
            this.CreateTable("dbo.Orders", c => new
            {
                Id = c.Int(new bool?(false), true, new int?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                SerialNumber = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Description = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                CreateDate = c.DateTime(new bool?(false), new byte?(), new DateTime?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                UserId = c.String(new bool?(false), new int?(128), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null)
            }, (object)null).PrimaryKey(t => t.Id, (string)null, 1 != 0, (object)null).ForeignKey("dbo.AspNetUsers", t => t.UserId, 1 != 0, (string)null, (object)null).Index(t => t.UserId, (string)null, 0 != 0, 0 != 0, (object)null);
            this.CreateTable("dbo.AspNetUsers", c => new
            {
                Id = c.String(new bool?(false), new int?(128), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Name = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Phone = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                ContactPerson = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Address = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Email = c.String(new bool?(), new int?(256), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                EmailConfirmed = c.Boolean(new bool?(false), new bool?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                PasswordHash = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                SecurityStamp = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                PhoneNumber = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                PhoneNumberConfirmed = c.Boolean(new bool?(false), new bool?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                TwoFactorEnabled = c.Boolean(new bool?(false), new bool?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                LockoutEndDateUtc = c.DateTime(new bool?(), new byte?(), new DateTime?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                LockoutEnabled = c.Boolean(new bool?(false), new bool?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                AccessFailedCount = c.Int(new bool?(false), false, new int?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                UserName = c.String(new bool?(false), new int?(256), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null)
            }, (object)null).PrimaryKey(t => t.Id, (string)null, 1 != 0, (object)null).Index(t => t.UserName, "UserNameIndex", 1 != 0, 0 != 0, (object)null);
            this.CreateTable("dbo.AspNetUserClaims", c => new
            {
                Id = c.Int(new bool?(false), true, new int?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                UserId = c.String(new bool?(false), new int?(128), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                ClaimType = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                ClaimValue = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null)
            }, (object)null).PrimaryKey(t => t.Id, (string)null, 1 != 0, (object)null).ForeignKey("dbo.AspNetUsers", t => t.UserId, 1 != 0, (string)null, (object)null).Index(t => t.UserId, (string)null, 0 != 0, 0 != 0, (object)null);
            this.CreateTable("dbo.AspNetUserLogins", c => new
            {
                LoginProvider = c.String(new bool?(false), new int?(128), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                ProviderKey = c.String(new bool?(false), new int?(128), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                UserId = c.String(new bool?(false), new int?(128), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null)
            }, (object)null).PrimaryKey(t => new
            {
                LoginProvider = t.LoginProvider,
                ProviderKey = t.ProviderKey,
                UserId = t.UserId
            }, (string)null, 1 != 0, (object)null).ForeignKey("dbo.AspNetUsers", t => t.UserId, 1 != 0, (string)null, (object)null).Index(t => t.UserId, (string)null, 0 != 0, 0 != 0, (object)null);
            this.CreateTable("dbo.AspNetUserRoles", c => new
            {
                UserId = c.String(new bool?(false), new int?(128), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                RoleId = c.String(new bool?(false), new int?(128), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null)
            }, (object)null).PrimaryKey(t => new
            {
                UserId = t.UserId,
                RoleId = t.RoleId
            }, (string)null, 1 != 0, (object)null).ForeignKey("dbo.AspNetUsers", t => t.UserId, 1 != 0, (string)null, (object)null).ForeignKey("dbo.AspNetRoles", t => t.RoleId, 1 != 0, (string)null, (object)null).Index(t => t.UserId, (string)null, 0 != 0, 0 != 0, (object)null).Index(t => t.RoleId, (string)null, 0 != 0, 0 != 0, (object)null);
            this.CreateTable("dbo.Suppliers", c => new
            {
                Id = c.Int(new bool?(false), true, new int?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Name = c.String(new bool?(false), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                ContactPerson = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Phone = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Address = c.String(new bool?(), new int?(), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                IsActive = c.Boolean(new bool?(false), new bool?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null)
            }, (object)null).PrimaryKey(t => t.Id, (string)null, 1 != 0, (object)null);
            this.CreateTable("dbo.AspNetRoles", c => new
            {
                Id = c.String(new bool?(false), new int?(128), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                Name = c.String(new bool?(false), new int?(256), new bool?(), new bool?(), (string)null, (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null)
            }, (object)null).PrimaryKey(t => t.Id, (string)null, 1 != 0, (object)null).Index(t => t.Name, "RoleNameIndex", 1 != 0, 0 != 0, (object)null);
            this.CreateTable("dbo.TypeUnitItemTypes", c => new
            {
                TypeUnit_Id = c.Int(new bool?(false), false, new int?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null),
                ItemType_Id = c.Int(new bool?(false), false, new int?(), (string)null, (string)null, (string)null, (IDictionary<string, AnnotationValues>)null)
            }, (object)null).PrimaryKey(t => new
            {
                TypeUnit_Id = t.TypeUnit_Id,
                ItemType_Id = t.ItemType_Id
            }, (string)null, 1 != 0, (object)null).ForeignKey("dbo.TypeUnits", t => t.TypeUnit_Id, 1 != 0, (string)null, (object)null).ForeignKey("dbo.ItemTypes", t => t.ItemType_Id, 1 != 0, (string)null, (object)null).Index(t => t.TypeUnit_Id, (string)null, 0 != 0, 0 != 0, (object)null).Index(t => t.ItemType_Id, (string)null, 0 != 0, 0 != 0, (object)null);
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles", (object)null);
            this.DropForeignKey("dbo.OrderItems", "UnitId", "dbo.TypeUnits", (object)null);
            this.DropForeignKey("dbo.OrderItems", "SupplierId", "dbo.Suppliers", (object)null);
            this.DropForeignKey("dbo.OrderItems", "OrderId", "dbo.Orders", (object)null);
            this.DropForeignKey("dbo.Orders", "UserId", "dbo.AspNetUsers", (object)null);
            this.DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers", (object)null);
            this.DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers", (object)null);
            this.DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers", (object)null);
            this.DropForeignKey("dbo.OrderItems", "ItemTypeId", "dbo.ItemTypes", (object)null);
            this.DropForeignKey("dbo.TypeUnitItemTypes", "ItemType_Id", "dbo.ItemTypes", (object)null);
            this.DropForeignKey("dbo.TypeUnitItemTypes", "TypeUnit_Id", "dbo.TypeUnits", (object)null);
            this.DropIndex("dbo.TypeUnitItemTypes", new string[1]
      {
        "ItemType_Id"
      }, (object)null);
            this.DropIndex("dbo.TypeUnitItemTypes", new string[1]
      {
        "TypeUnit_Id"
      }, (object)null);
            this.DropIndex("dbo.AspNetRoles", "RoleNameIndex", (object)null);
            this.DropIndex("dbo.AspNetUserRoles", new string[1]
      {
        "RoleId"
      }, (object)null);
            this.DropIndex("dbo.AspNetUserRoles", new string[1]
      {
        "UserId"
      }, (object)null);
            this.DropIndex("dbo.AspNetUserLogins", new string[1]
      {
        "UserId"
      }, (object)null);
            this.DropIndex("dbo.AspNetUserClaims", new string[1]
      {
        "UserId"
      }, (object)null);
            this.DropIndex("dbo.AspNetUsers", "UserNameIndex", (object)null);
            this.DropIndex("dbo.Orders", new string[1] { "UserId" }, (object)null);
            this.DropIndex("dbo.OrderItems", new string[1]
      {
        "UnitId"
      }, (object)null);
            this.DropIndex("dbo.OrderItems", new string[1]
      {
        "ItemTypeId"
      }, (object)null);
            this.DropIndex("dbo.OrderItems", new string[1]
      {
        "SupplierId"
      }, (object)null);
            this.DropIndex("dbo.OrderItems", new string[1]
      {
        "OrderId"
      }, (object)null);
            this.DropTable("dbo.TypeUnitItemTypes", (object)null);
            this.DropTable("dbo.AspNetRoles", (object)null);
            this.DropTable("dbo.Suppliers", (object)null);
            this.DropTable("dbo.AspNetUserRoles", (object)null);
            this.DropTable("dbo.AspNetUserLogins", (object)null);
            this.DropTable("dbo.AspNetUserClaims", (object)null);
            this.DropTable("dbo.AspNetUsers", (object)null);
            this.DropTable("dbo.Orders", (object)null);
            this.DropTable("dbo.OrderItems", (object)null);
            this.DropTable("dbo.TypeUnits", (object)null);
            this.DropTable("dbo.ItemTypes", (object)null);
        }
    }
}
