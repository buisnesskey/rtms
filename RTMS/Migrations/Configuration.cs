using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using RTMS.Core;
using RTMS.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace RTMS.Migrations
{
	internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
	{
		public Configuration()
		{
			base.AutomaticMigrationsEnabled = true;
			base.AutomaticMigrationDataLossAllowed = true;
			base.ContextKey = "RTMS";
		}

		protected override void Seed(ApplicationDbContext context)
		{
			RoleManager<IdentityRole> manager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
			ApplicationUserManager applicationUserManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
			applicationUserManager.UserValidator = new UserValidator<ApplicationUser>(applicationUserManager)
			{
				AllowOnlyAlphanumericUserNames = false,
				RequireUniqueEmail = false
			};
			applicationUserManager.PasswordValidator = new PasswordValidator
			{
				RequiredLength = 5,
				RequireNonLetterOrDigit = false,
				RequireDigit = false,
				RequireLowercase = false,
				RequireUppercase = false
			};
			if (!context.Roles.Any((IdentityRole r) => r.Name == SystemRoles.Super))
			{
				manager.Create(new IdentityRole
				{
					Name = SystemRoles.Super
				});
			}
			if (!context.Roles.Any((IdentityRole r) => r.Name == SystemRoles.Admin))
			{
				manager.Create(new IdentityRole
				{
					Name = SystemRoles.Admin
				});
			}
			if (!context.Roles.Any((IdentityRole r) => r.Name == SystemRoles.Customer))
			{
				manager.Create(new IdentityRole
				{
					Name = SystemRoles.Customer
				});
			}
            if (!context.Roles.Any((IdentityRole r) => r.Name == SystemRoles.Supervisor))
            {
                manager.Create(new IdentityRole
                {
                    Name = SystemRoles.Supervisor
                });
            }
			if (!context.Users.Any((ApplicationUser u) => u.UserName == "super@rtms.com.tr"))
			{
				ApplicationUser applicationUser = new ApplicationUser
				{
					UserName = "super@rtms.com.tr",
					Email = "super@rtms.com.tr",
					Name = "Super"
				};
				applicationUserManager.Create(applicationUser, "12345");
				applicationUserManager.AddToRole(applicationUser.Id, SystemRoles.Super);
			}
			if (!context.Users.Any((ApplicationUser u) => u.UserName == "admin@rtms.com.tr"))
			{
				ApplicationUser applicationUser2 = new ApplicationUser
				{
					UserName = "admin@rtms.com.tr",
					Email = "admin@rtms.com.tr",
					Name = "admin"
				};
				applicationUserManager.Create(applicationUser2, "12345");
				applicationUserManager.AddToRole(applicationUser2.Id, SystemRoles.Admin);
			}
			if (!context.Users.Any((ApplicationUser u) => u.UserName == "admin2@rtms.com.tr"))
			{
				ApplicationUser applicationUser3 = new ApplicationUser
				{
					UserName = "admin2@rtms.com.tr",
					Email = "admin2@rtms.com.tr",
					Name = "admin2"
				};
				applicationUserManager.Create(applicationUser3, "12345");
				applicationUserManager.AddToRole(applicationUser3.Id, SystemRoles.Admin);
			}
			if (!context.Users.Any((ApplicationUser u) => u.UserName == "admin3@rtms.com.tr"))
			{
				ApplicationUser applicationUser4 = new ApplicationUser
				{
					UserName = "admin3@rtms.com.tr",
					Email = "admin3@rtms.com.tr",
					Name = "admin3"
				};
				applicationUserManager.Create(applicationUser4, "12345");
				applicationUserManager.AddToRole(applicationUser4.Id, SystemRoles.Admin);
			}
			if (!context.Users.Any((ApplicationUser u) => u.UserName == "customer1@rtms.com.tr"))
			{
				ApplicationUser applicationUser5 = new ApplicationUser
				{
					UserName = "customer1@rtms.com.tr",
					Email = "customer1@rtms.com.tr",
					Name = "Customer 1",
					Phone = "012347681",
					ContactPerson = "Contact Person1",
					Address = "Address 1"
				};
				applicationUserManager.Create(applicationUser5, "12345");
				applicationUserManager.AddToRole(applicationUser5.Id, SystemRoles.Customer);
			}
			if (!context.Users.Any((ApplicationUser u) => u.UserName == "customer2@rtms.com.tr"))
			{
				ApplicationUser applicationUser6 = new ApplicationUser
				{
					UserName = "customer2@rtms.com.tr",
					Email = "customer2@rtms.com.tr",
					Name = "Customer 2",
					Phone = "012349832681",
					ContactPerson = "Contact Person 2",
					Address = "Address 2"
				};
				applicationUserManager.Create(applicationUser6, "12345");
				applicationUserManager.AddToRole(applicationUser6.Id, SystemRoles.Customer);
			}
			if (!context.Users.Any((ApplicationUser u) => u.UserName == "customer3@rtms.com.tr"))
			{
				ApplicationUser applicationUser7 = new ApplicationUser
				{
					UserName = "customer3@rtms.com.tr",
					Email = "customer3@rtms.com.tr",
					Name = "Customer 3",
					Phone = "01234768109",
					ContactPerson = "Contact Person3",
					Address = "Address 3"
				};
				applicationUserManager.Create(applicationUser7, "12345");
				applicationUserManager.AddToRole(applicationUser7.Id, SystemRoles.Customer);
			}
            if (!context.Users.Any((ApplicationUser u) => u.UserName == "supervisor1@rtms.com.tr"))
            {
                ApplicationUser applicationUser8 = new ApplicationUser
                {
                    UserName = "supervisor1@rtms.com.tr",
                    Email = "supervisor1@rtms.com.tr",
                    Name = "supervisor 3",
                    Phone = "01234768109",
                    ContactPerson = "Contact Person3",
                    Address = "Address 3"
                };
                applicationUserManager.Create(applicationUser8, "12345");
                applicationUserManager.AddToRole(applicationUser8.Id, SystemRoles.Supervisor);
            }
			if (context.TypeUnits.Count<TypeUnit>() == 0 && context.ItemTypes.Count<ItemType>() == 0)
			{
				TypeUnit typeUnit = new TypeUnit
				{
					Name = "Unit 1",
					Code = "Code 1",
					IsActive = true
				};
				TypeUnit typeUnit2 = new TypeUnit
				{
					Name = "Unit 2",
					Code = "Code 2",
					IsActive = true
				};
				TypeUnit typeUnit3 = new TypeUnit
				{
					Name = "Unit 3",
					Code = "Code 3",
					IsActive = true
				};
				context.TypeUnits.Add(typeUnit);
				context.TypeUnits.Add(typeUnit2);
				context.TypeUnits.Add(typeUnit3);
				context.SaveChanges();
				ItemType itemType = new ItemType
				{
					Name = "Type 1",
					Code = "Code 1",
					IsActive = true
				};
				itemType.TypeUnits = new HashSet<TypeUnit>();
				itemType.TypeUnits.Add(typeUnit);
				itemType.TypeUnits.Add(typeUnit2);
				itemType.TypeUnits.Add(typeUnit3);
				ItemType itemType2 = new ItemType
				{
					Name = "Type 2",
					Code = "Code 2",
					IsActive = true
				};
				itemType2.TypeUnits = new HashSet<TypeUnit>();
				itemType2.TypeUnits.Add(typeUnit);
				itemType2.TypeUnits.Add(typeUnit2);
				itemType2.TypeUnits.Add(typeUnit3);
				ItemType itemType3 = new ItemType
				{
					Name = "Type 3",
					Code = "Code 3",
					IsActive = true
				};
				itemType3.TypeUnits = new HashSet<TypeUnit>();
				itemType3.TypeUnits.Add(typeUnit);
				itemType3.TypeUnits.Add(typeUnit2);
				itemType3.TypeUnits.Add(typeUnit3);
				context.ItemTypes.Add(itemType);
				context.ItemTypes.Add(itemType2);
				context.ItemTypes.Add(itemType3);
				context.SaveChanges();
			}
			if (context.Suppliers.Count<Supplier>() == 0)
			{
				context.Suppliers.Add(new Supplier
				{
					Name = "Supplier 1"
				});
				context.Suppliers.Add(new Supplier
				{
					Name = "Supplier 2"
				});
				context.Suppliers.Add(new Supplier
				{
					Name = "Supplier 3"
				});
				context.SaveChanges();
			}
		}
	}
}
