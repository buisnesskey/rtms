using System;
using System.IO;
using System.Runtime.InteropServices;

namespace SqlServerTypes
{
	internal class Utilities
	{
		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr LoadLibrary(string libname);

		public static void LoadNativeAssemblies(string rootApplicationPath)
		{
			string expr_20 = (IntPtr.Size > 4) ? Path.Combine(rootApplicationPath, "SqlServerTypes\\x64\\") : Path.Combine(rootApplicationPath, "SqlServerTypes\\x86\\");
			Utilities.LoadNativeAssembly(expr_20, "msvcr100.dll");
			Utilities.LoadNativeAssembly(expr_20, "SqlServerSpatial110.dll");
		}

		private static void LoadNativeAssembly(string nativeBinaryPath, string assemblyName)
		{
			if (Utilities.LoadLibrary(Path.Combine(nativeBinaryPath, assemblyName)) == IntPtr.Zero)
			{
				throw new Exception(string.Format("Error loading {0} (ErrorCode: {1})", assemblyName, Marshal.GetLastWin32Error()));
			}
		}
	}
}
