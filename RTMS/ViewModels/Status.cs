using System;

namespace RTMS.ViewModels
{
	public class Status
	{
		public int WaitingCount
		{
			get;
			set;
		}

		public int ProcessingCount
		{
			get;
			set;
		}

		public int CompleteCount
		{
			get;
			set;
		}

		public int CanceledCount
		{
			get;
			set;
		}

		public int AlternativeCount
		{
			get;
			set;
		}

		public int AllCount
		{
			get;
			set;
		}

		public string FinalStatus
		{
			get;
			set;
		}

		public int FinalStatusRank
		{
			get;
			set;
		}

        public int PendingCount
        {
            get;
            set;
        }
	}
}
