using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using RTMS.Models;
using RTMS.Providers;
using System;

namespace RTMS
{
	public class Startup
	{
		public static OAuthAuthorizationServerOptions OAuthOptions
		{
			get;
			private set;
		}

		public static string PublicClientId
		{
			get;
			private set;
		}

		public void ConfigureAuth(IAppBuilder app)
		{
			app.CreatePerOwinContext(new Func<ApplicationDbContext>(ApplicationDbContext.Create));
			app.CreatePerOwinContext(new Func<IdentityFactoryOptions<ApplicationUserManager>, IOwinContext, ApplicationUserManager>(ApplicationUserManager.Create));
			Startup.PublicClientId = "self";
			Startup.OAuthOptions = new OAuthAuthorizationServerOptions
			{
				TokenEndpointPath = new PathString("/Token"),
				Provider = new ApplicationOAuthProvider(Startup.PublicClientId),
				AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
				AccessTokenExpireTimeSpan = TimeSpan.FromDays(14.0),
				AllowInsecureHttp = true
			};
			app.UseOAuthBearerTokens(Startup.OAuthOptions);
		}

		public void Configuration(IAppBuilder app)
		{
			this.ConfigureAuth(app);
		}
	}
}
