using Newtonsoft.Json.Serialization;
using System;
using System.Web.Http;

namespace RTMS
{
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
		{
			config.EnableCors();
			config.SuppressDefaultHostAuthentication();
			config.Filters.Add(new HostAuthenticationFilter("Bearer"));
			config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
			config.MapHttpAttributeRoutes();
			config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new
			{
				id = RouteParameter.Optional
			});
		}
	}
}
