using System;

namespace RTMS.Core
{
	public static class SystemRoles
	{
		public static string Super = "super";

		public static string Admin = "admin";

        public static string Customer = "customer";

        public static string Supervisor = "supervisor";
	}
}
