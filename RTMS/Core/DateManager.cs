using System;
using System.Globalization;
using System.Web.Configuration;

namespace RTMS.Core
{
	public class DateManager
	{
		public static TimeZoneInfo LocalTimeZone = TimeZoneInfo.CreateCustomTimeZone("LocalTime", new TimeSpan(int.Parse(WebConfigurationManager.AppSettings["UtcOffset"]), 0, 0), "LocalTime", "LocalTime");

		public static IFormatProvider CultureEN = new CultureInfo("en-US", true);

		public static IFormatProvider CultureAR = new CultureInfo("ar-EG", true);

		public static DateTime? GetLocalDate(DateTime? utcDate)
		{
			DateTime? result;
			if (!utcDate.HasValue)
			{
				result = null;
				return result;
			}
			try
			{
				result = new DateTime?(TimeZoneInfo.ConvertTimeFromUtc(utcDate.Value, DateManager.LocalTimeZone));
			}
			catch
			{
				result = null;
			}
			return result;
		}

		public static string GetForView(string date)
		{
			if (string.IsNullOrEmpty(date))
			{
				return "";
			}
			string result;
			try
			{
				result = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(date), DateManager.LocalTimeZone).ToString(DateManager.CultureAR);
			}
			catch
			{
				result = "";
			}
			return result;
		}

		public static string GetForView(string date, string format)
		{
			if (string.IsNullOrEmpty(date))
			{
				return "";
			}
			string result;
			try
			{
				if (string.IsNullOrEmpty(format))
				{
					result = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(date), DateManager.LocalTimeZone).ToString(DateManager.CultureAR);
				}
				else
				{
					result = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(date), DateManager.LocalTimeZone).ToString(format, DateManager.CultureAR);
				}
			}
			catch
			{
				result = "";
			}
			return result;
		}

		public static string GetForSave(string date, string format)
		{
			if (string.IsNullOrEmpty(date))
			{
				return "";
			}
			string result;
			try
			{
				if (string.IsNullOrEmpty(format))
				{
					result = TimeZoneInfo.ConvertTimeToUtc(DateTime.Parse(date), DateManager.LocalTimeZone).ToString();
				}
				else
				{
					result = TimeZoneInfo.ConvertTimeToUtc(DateTime.ParseExact(date, format, DateManager.CultureEN), DateManager.LocalTimeZone).ToString();
				}
			}
			catch
			{
				result = "";
			}
			return result;
		}

		public static string GetRSSFormat(string date)
		{
			if (string.IsNullOrEmpty(date))
			{
				return "";
			}
			string result;
			try
			{
				result = DateTime.Parse(date).ToString("R");
			}
			catch
			{
				result = "";
			}
			return result;
		}

		public static string GetCurrentDateView(string format)
		{
			string result;
			try
			{
				if (string.IsNullOrEmpty(format))
				{
					result = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, DateManager.LocalTimeZone).ToString("d-M-yyyy", DateManager.CultureAR);
				}
				else
				{
					result = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, DateManager.LocalTimeZone).ToString(format, DateManager.CultureAR);
				}
			}
			catch
			{
				result = "";
			}
			return result;
		}
	}
}
