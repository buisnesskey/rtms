using RTMS.Common;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Reflection;
using System.Linq;
namespace RTMS.Core
{
	public class MailHelper
	{
		public static bool SendMail(string address, string subject, string body, string attachmentPath)
		{
			bool result = false;
			MailMessage mailMessage = new MailMessage();
			mailMessage.To.Add(new MailAddress(address));
			mailMessage.Subject = subject;
			mailMessage.Body = body;
			if (!string.IsNullOrEmpty(attachmentPath))
			{
				Attachment item = new Attachment(attachmentPath);
				mailMessage.Attachments.Add(item);
			}
			SmtpClient smtpClient = new SmtpClient();
			try
			{
				smtpClient.Send(mailMessage);
				result = true;
			}
			catch (Exception ex)
			{
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
			}
			using (IEnumerator<Attachment> enumerator = mailMessage.Attachments.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					enumerator.Current.Dispose();
				}
			}
			mailMessage.Attachments.Dispose();
			mailMessage.Dispose();
			mailMessage = null;
			return result;
		}

        public static bool SendMail(string address, string subject, string body)
        {
            bool result = false;
            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(new MailAddress(address));
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            SmtpClient smtpClient = new SmtpClient();
            try
            {
                smtpClient.Send(mailMessage);
                result = true;
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            //using (IEnumerator<Attachment> enumerator = mailMessage.Attachments.GetEnumerator())
            //{
            //    while (enumerator.MoveNext())
            //    {
            //        enumerator.Current.Dispose();
            //    }
            //}
            mailMessage.Attachments.Dispose();
            mailMessage.Dispose();
            mailMessage = null;
            return result;
        }
	}
}
