using System;

namespace RTMS.Core
{
	public static class State
	{
        public static string Pending = "pending";

        public static string Waiting = "waiting";

		public static string Processing = "processing";

		public static string Complete = "complete";

		public static string Canceled = "canceled";

		public static string Alternative = "alternative";
	}
}
