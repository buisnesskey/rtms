// Decompiled with JetBrains decompiler
// Type: RTMS.Core.Excel
// Assembly: RTMS, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0A16F994-A518-4FEB-A56D-42E16C403459
// Assembly location: D:\rtms\bin\RTMS.dll

using DocumentFormat.OpenXml.Spreadsheet;
using RTMS.Common;
using RTMS.Models;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace RTMS.Core
{
    public static class Excel
    {
        public static void SaveReceipt(Order order, ApplicationUser customer, string filename)
        {
            try
            {
                using (SLDocument slDocument = new SLDocument())
                {
                    slDocument.SetColumnWidth("A", 10.0);
                    slDocument.SetColumnWidth("B", 30.0);
                    slDocument.SetColumnWidth("C", 15.0);
                    slDocument.SetColumnWidth("D", 15.0);
                    slDocument.SetColumnWidth("E", 15.0);
                    SLStyle Style1 = new SLStyle();
                    Style1.SetPatternFill(PatternValues.Solid, System.Drawing.Color.LightGray, System.Drawing.Color.White);
                    Style1.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
                    Style1.SetVerticalAlignment(VerticalAlignmentValues.Center);
                    Style1.SetRightBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    Style1.SetLeftBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    Style1.SetTopBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    Style1.SetBottomBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    SLStyle Style2 = new SLStyle();
                    Style2.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
                    Style2.SetVerticalAlignment(VerticalAlignmentValues.Center);
                    SLStyle Style3 = new SLStyle();
                    Style3.SetTopBorder(BorderStyleValues.Double, System.Drawing.Color.Black);
                    Style3.SetBottomBorder(BorderStyleValues.Double, System.Drawing.Color.Black);
                    SLStyle Style4 = new SLStyle();
                    Style4.SetTopBorder(BorderStyleValues.None, System.Drawing.Color.Transparent);
                    Style4.SetBottomBorder(BorderStyleValues.None, System.Drawing.Color.Transparent);
                    Style1.SetRightBorder(BorderStyleValues.None, System.Drawing.Color.Transparent);
                    Style1.SetLeftBorder(BorderStyleValues.None, System.Drawing.Color.Transparent);
                    slDocument.MergeWorksheetCells("A1", "C3");
                    slDocument.MergeWorksheetCells("B4", "E4");
                    slDocument.MergeWorksheetCells("B5", "E5");
                    slDocument.MergeWorksheetCells("A6", "E6");
                    slDocument.MergeWorksheetCells("B7", "E7");
                    slDocument.MergeWorksheetCells("B7", "E8");
                    slDocument.SetCellValue("A1", "DELIVERY RECEIPT");
                    slDocument.SetCellStyle("A1", Style2);
                    slDocument.SetCellStyle("A4", "E4", Style3);
                    slDocument.SetCellValue("D1", "Order No");
                    slDocument.SetCellValue("D2", "Delivery No");
                    slDocument.SetCellValue("D3", "Date");
                    slDocument.SetCellValue("E1", order.SerialNumber);
                    slDocument.SetCellValue("E2", "");
                    slDocument.SetCellValue("E3", DateManager.GetLocalDate(new DateTime?(order.CreateDate)).Value.ToString("dd-MMM-yyyy"));
                    slDocument.SetCellValue("A4", "Company");
                    slDocument.SetCellValue("B4", ": RTMS MECHANICAL MAINTENANCE & CLEANING LLC");
                    slDocument.SetCellStyle("A4", "E4", Style3);
                    slDocument.SetCellValue("A5", "Office Address");
                    slDocument.SetCellValue("B5", ": 26, Orabi Street, Apt.2, Sarayat Maadi, Cairo, EGYPT");
                    slDocument.SetCellStyle("A5", "E5", Style3);
                    slDocument.SetCellValue("A6", "Customer Information");
                    slDocument.SetCellStyle("A6", "E6", Style1);
                    slDocument.SetCellValue("A7", "Company");
                    slDocument.SetCellValue("B7", customer.Name);
                    slDocument.SetCellValue("A8", "Delivery Address");
                    slDocument.SetCellValue("B8", customer.Address);
                    slDocument.MergeWorksheetCells("A9", "E9");
                    slDocument.SetCellValue("A10", "No");
                    slDocument.SetCellValue("B10", "material");
                    //slDocument.SetCellValue("C10", "Category");
                    slDocument.SetCellValue("C10", "Unit");
                    slDocument.SetCellValue("D10", "Requested");
                    slDocument.SetCellValue("E10", "Delivered");
                    List<OrderItem> list = order.Items.Where<OrderItem>((Func<OrderItem, bool>)(item =>
                    {
                        if (!(item.State == State.Complete))
                            return item.State == State.Alternative;
                        return true;
                    })).ToList<OrderItem>();
                    int num = 11;
                    double Data = 0.0;
                    int index = 0;
                    while (index < list.Count)
                    {
                        slDocument.SetCellStyle("A" + (object)num, "E" + (object)num, Style1);
                        slDocument.SetCellValue("A" + (object)num, index + 1);
                        slDocument.SetCellValue("B" + (object)num, list[index].Description);
                        //slDocument.SetCellValue("C" + (object)num, list[index].ItemTypeId.HasValue ? list[index].ItemType.Name : list[index].TypeText);
                        slDocument.SetCellValue("C" + (object)num, list[index].UnitId.HasValue ? list[index].Unit.Name : list[index].UnitText);
                        slDocument.SetCellValue("D" + (object)num, list[index].Quantity);
                        slDocument.SetCellValue("E" + (object)num, list[index].DeliveredQuantity);
                        Data += list[index].DeliveredQuantity;
                        ++index;
                        ++num;
                    }
                    slDocument.SetCellValue("D" + (object)num, "Total");
                    slDocument.SetCellValue("E" + (object)num, Data);
                    slDocument.SetCellValue("A" + (object)(num + 2), "Above mentioned items are received in good condition.");
                    slDocument.MergeWorksheetCells("A" + (object)(num + 3), "E" + (object)(num + 4));
                    slDocument.SetCellValue("A" + (object)(num + 5), "Received By:");
                    slDocument.SetCellValue("D" + (object)(num + 5), "Delivered By:");
                    slDocument.SetCellStyle("A" + (object)(num + 5), "E" + (object)(num + 5), Style4);
                    slDocument.MergeWorksheetCells("A" + (object)(num + 6), "E" + (object)(num + 7));
                    slDocument.SetCellValue("A" + (object)(num + 8), "Date:");
                    slDocument.SetCellValue("D" + (object)(num + 8), "Date:");
                    slDocument.SetCellStyle("A" + (object)(num + 8), "E" + (object)(num + 8), Style4);
                    slDocument.SaveAs(filename);
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
        }
        public static void SaveReceipt(List<Order> orders, List<int> ids, List<ApplicationUser> customers, string filename)
        {
            try
            {

                using (SLDocument slDocument = new SLDocument())
                {
                    slDocument.SetColumnWidth("A", 10.0);
                    slDocument.SetColumnWidth("B", 30.0);
                    slDocument.SetColumnWidth("C", 15.0);
                    slDocument.SetColumnWidth("D", 15.0);
                    slDocument.SetColumnWidth("E", 15.0);
                    SLStyle Style1 = new SLStyle();
                    Style1.SetPatternFill(PatternValues.Solid, System.Drawing.Color.LightGray, System.Drawing.Color.White);
                    Style1.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
                    Style1.SetVerticalAlignment(VerticalAlignmentValues.Center);
                    Style1.SetRightBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    Style1.SetLeftBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    Style1.SetTopBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    Style1.SetBottomBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    SLStyle Style2 = new SLStyle();
                    Style2.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
                    Style2.SetVerticalAlignment(VerticalAlignmentValues.Center);
                    SLStyle Style3 = new SLStyle();
                    Style3.SetTopBorder(BorderStyleValues.Double, System.Drawing.Color.Black);
                    Style3.SetBottomBorder(BorderStyleValues.Double, System.Drawing.Color.Black);
                    SLStyle Style4 = new SLStyle();
                    Style4.SetTopBorder(BorderStyleValues.None, System.Drawing.Color.Transparent);
                    Style4.SetBottomBorder(BorderStyleValues.None, System.Drawing.Color.Transparent);
                    Style1.SetRightBorder(BorderStyleValues.None, System.Drawing.Color.Transparent);
                    Style1.SetLeftBorder(BorderStyleValues.None, System.Drawing.Color.Transparent);
                    slDocument.MergeWorksheetCells("A1", "C3");
                    slDocument.MergeWorksheetCells("B4", "E4");
                    slDocument.MergeWorksheetCells("B5", "E5");
                    slDocument.MergeWorksheetCells("A6", "E6");
                    slDocument.MergeWorksheetCells("B7", "E7");
                    slDocument.MergeWorksheetCells("B7", "E8");
                    slDocument.SetCellValue("A1", "DELIVERY RECEIPT");
                    slDocument.SetCellStyle("A1", Style2);
                    slDocument.SetCellStyle("A4", "E4", Style3);
                    slDocument.SetCellValue("D1", "Order No");
                    slDocument.SetCellValue("D2", "Delivery No");
                    slDocument.SetCellValue("D3", "Date");
                    slDocument.SetCellValue("E1", string.Join(",",orders.Select(x=>x.SerialNumber)));
                    Guid orderGuid = Guid.NewGuid();
                    string orderGUIDPart = orderGuid.ToString().Split('-')[0];
                    slDocument.SetCellValue("E2", orders.Count() > 1 ? orderGUIDPart:"");
                    slDocument.SetCellValue("E3", DateManager.GetLocalDate(new DateTime?(orders[0].CreateDate)).Value.ToString("dd-MMM-yyyy"));
                    slDocument.SetCellValue("A4", "Company");
                    slDocument.SetCellValue("B4", ": RTMS MECHANICAL MAINTENANCE & CLEANING LLC");
                    slDocument.SetCellStyle("A4", "E4", Style3);
                    slDocument.SetCellValue("A5", "Office Address");
                    slDocument.SetCellValue("B5", ": 26, Orabi Street, Apt.2, Sarayat Maadi, Cairo, EGYPT");
                    slDocument.SetCellStyle("A5", "E5", Style3);
                    slDocument.SetCellValue("A6", "Customer Information");
                    slDocument.SetCellStyle("A6", "E6", Style1);
                    slDocument.SetCellValue("A7", "Company");
                    slDocument.SetCellValue("B7", string.Join(",",customers.Select(x=>x.Name)));
                    slDocument.SetCellValue("A8", "Delivery Address");
                    slDocument.SetCellValue("B8", string.Join(",",customers.Select(x=>x.Address)));
                    slDocument.MergeWorksheetCells("A9", "E9");

                    slDocument.SetCellValue("A10", "No");
                    slDocument.SetCellValue("B10", "material");
                    slDocument.SetCellValue("C10", "Category");
                    slDocument.SetCellValue("D10", "Unit");
                    slDocument.SetCellValue("E10", "Requested");
                    slDocument.SetCellValue("F10", "Delivered");
                    slDocument.SetCellValue("G10", "Order Number");
                    int iter = 1;
                    int num = 11;
                    double Data = 0.0;
                    foreach (var order in orders)
                    {
                        List<OrderItem> list = order.Items.Where<OrderItem>((Func<OrderItem, bool>)(item =>
                        {
                            if (ids.Contains(item.Id))
                                return true;
                            else
                                return false;
                        })).ToList<OrderItem>();
                        //int num = 11 * iter;
                        //iter++;
                        
                        int index = 0;
                        while (index < list.Count)
                        {
                            slDocument.SetCellStyle("A" + (object)num, "E" + (object)num, Style1);
                            slDocument.SetCellValue("A" + (object)num, index + 1);
                            slDocument.SetCellValue("B" + (object)num, list[index].Description);
                            slDocument.SetCellValue("C" + (object)num, list[index].ItemTypeId.HasValue ? list[index].ItemType.Name : list[index].TypeText);
                            slDocument.SetCellValue("D" + (object)num, list[index].UnitId.HasValue ? list[index].Unit.Name : list[index].UnitText);
                            slDocument.SetCellValue("E" + (object)num, list[index].Quantity);
                            slDocument.SetCellValue("F" + (object)num, list[index].DeliveredQuantity);
                            slDocument.SetCellValue("G" + (object)num, order.SerialNumber);
                            Data += list[index].DeliveredQuantity;
                            ++index;
                            ++num;
                        }

                        

                    }
                    slDocument.SetCellValue("E" + (object)num, "Total");
                    slDocument.SetCellValue("F" + (object)num, Data);
                    slDocument.SetCellValue("A" + (object)(num + 2), "Above mentioned items are received in good condition.");
                    slDocument.MergeWorksheetCells("A" + (object)(num + 3), "E" + (object)(num + 4));
                    slDocument.SetCellValue("A" + (object)(num + 5), "Received By:");
                    slDocument.SetCellValue("D" + (object)(num + 5), "Delivered By:");
                    slDocument.SetCellStyle("A" + (object)(num + 5), "E" + (object)(num + 5), Style4);
                    slDocument.MergeWorksheetCells("A" + (object)(num + 6), "E" + (object)(num + 7));
                    slDocument.SetCellValue("A" + (object)(num + 8), "Date:");
                    slDocument.SetCellValue("D" + (object)(num + 8), "Date:");
                    slDocument.SetCellStyle("A" + (object)(num + 8), "E" + (object)(num + 8), Style4);
                    slDocument.SaveAs(filename);

                }
            }
            catch (Exception exception)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(exception, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
            }
        }

        public static void SaveInvoice(Order order, ApplicationUser customer, string filename)
        {
            try
            {
                using (SLDocument sLDocument = new SLDocument())
                {
                    sLDocument.SetColumnWidth("A", 40.0);
                    sLDocument.SetColumnWidth("B", 15.0);
                    sLDocument.SetColumnWidth("C", 15.0);
                    sLDocument.SetColumnWidth("D", 15.0);
                    sLDocument.SetColumnWidth("E", 15.0);
                    SLStyle sLStyle = new SLStyle();
                    sLStyle.SetPatternFill(PatternValues.Solid, System.Drawing.Color.LightGray, System.Drawing.Color.White);
                    sLStyle.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
                    sLStyle.SetVerticalAlignment(VerticalAlignmentValues.Center);
                    sLStyle.SetRightBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    sLStyle.SetLeftBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    sLStyle.SetTopBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    sLStyle.SetBottomBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    SLStyle sLStyle2 = new SLStyle();
                    sLStyle2.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
                    sLStyle2.SetVerticalAlignment(VerticalAlignmentValues.Center);
                    SLStyle sLStyle3 = new SLStyle();
                    sLStyle3.SetTopBorder(BorderStyleValues.Double, System.Drawing.Color.Black);
                    sLStyle3.SetBottomBorder(BorderStyleValues.Double, System.Drawing.Color.Black);
                    sLDocument.MergeWorksheetCells("A1", "C3");
                    sLDocument.MergeWorksheetCells("B4", "E4");
                    sLDocument.MergeWorksheetCells("B5", "E5");
                    sLDocument.MergeWorksheetCells("A6", "E6");
                    sLDocument.MergeWorksheetCells("B7", "E7");
                    sLDocument.MergeWorksheetCells("B7", "E8");
                    sLDocument.SetCellValue("A1", "TAX INVOICE");
                    sLDocument.SetCellStyle("A1", sLStyle2);
                    sLDocument.SetCellStyle("A4", "E4", sLStyle3);
                    sLDocument.SetCellValue("D1", "Invoice No");
                    sLDocument.SetCellValue("D2", "Reference No");
                    sLDocument.SetCellValue("D3", "Date");
                    sLDocument.SetCellValue("E1", "");
                    sLDocument.SetCellValue("E2", order.SerialNumber);
                    sLDocument.SetCellValue("E3", DateManager.GetLocalDate(new DateTime?(order.CreateDate)).Value.ToString("dd-MMM-yyyy"));
                    sLDocument.SetCellValue("A4", "Company");
                    sLDocument.SetCellValue("B4", ": RTMS MECHANICAL MAINTENANCE & CLEANING LLC");
                    sLDocument.SetCellStyle("A4", "E4", sLStyle3);
                    sLDocument.SetCellValue("A5", "Office Address");
                    sLDocument.SetCellValue("B5", ": 26, Orabi Street, Apt.2, Sarayat Maadi, Cairo, EGYPT");
                    sLDocument.SetCellStyle("A4", "E4", sLStyle3);
                    sLDocument.SetCellValue("A6", "Customer Information");
                    sLDocument.SetCellStyle("A6", "E6", sLStyle);
                    sLDocument.SetCellValue("A7", "Company");
                    sLDocument.SetCellValue("B7", customer.Name);
                    sLDocument.SetCellValue("A8", "Delivery Address");
                    sLDocument.SetCellValue("B8", customer.Address);
                    sLDocument.MergeWorksheetCells("A9", "E9");
                    sLDocument.SetCellValue("A10", "material");
                    //sLDocument.SetCellValue("B10", "Category");
                    sLDocument.SetCellValue("B10", "Unit");
                    sLDocument.SetCellValue("C10", "Quantity");
                    sLDocument.SetCellValue("D10", "Cost");
                    sLDocument.SetCellValue("E10", "Total Cost");
                    //IEnumerable<OrderItem> arg_371_0 = order.Items;
                    //Func<OrderItem, bool> arg_371_1;
                    //if ((arg_371_1 = Excel.<>c.<>9__1_0) == null)
                    //{
                    //	arg_371_1 = (Excel.<>c.<>9__1_0 = new Func<OrderItem, bool>(Excel.<>c.<>9.<SaveInvoice>b__1_0));
                    //}
                    //List<OrderItem> list = arg_371_0.Where(arg_371_1).ToList<OrderItem>();

                    List<OrderItem> list = order.Items.Where<OrderItem>((Func<OrderItem, bool>)(item =>
                    {
                        if (!(item.State == State.Complete))
                            return item.State == State.Alternative;
                        return true;
                    })).ToList<OrderItem>();

                    int num = 11;
                    decimal d = new decimal(0, 0, 0, false, 1);
                    decimal num2 = new decimal(0, 0, 0, false, 1);
                    decimal num3 = new decimal(0, 0, 0, false, 1);
                    decimal num4 = new decimal(0, 0, 0, false, 1);
                    decimal num5 = new decimal(0, 0, 0, false, 1);
                    int i = 0;
                    while (i < list.Count)
                    {
                        sLDocument.SetCellStyle("A" + num, "E" + num, sLStyle);
                        sLDocument.SetCellValue("A" + num, list[i].Description);
                        //sLDocument.SetCellValue("B" + num, list[i].ItemTypeId.HasValue ? list[i].ItemType.Name : list[i].TypeText);
                        sLDocument.SetCellValue("B" + num, list[i].TypeText);//Remarks//list[index].UnitId.HasValue ? list[index].Unit.Name : list[index].UnitText
                        sLDocument.SetCellValue("C" + num, list[i].DeliveredQuantity);
                        sLDocument.SetCellValue("D" + num, list[i].SellingPrice);
                        decimal num6 = list[i].SellingPrice * (decimal)list[i].DeliveredQuantity;
                        sLDocument.SetCellValue("E" + num, num6);
                        d += num6;
                        i++;
                        num++;
                    }
                    sLDocument.MergeWorksheetCells("A" + num, "C" + (num + 4));
                    num2 = d * 0.015m;
                    num3 = (d + num2) * 0.02m;
                    num4 = d + num2 + num3;
                    //num5 = num4 * 0.1m;
                    num5 = num4 * 0.13m;
                    sLDocument.SetCellValue("D" + num++, "G.Exp. %1.5");
                    sLDocument.SetCellValue("E" + (num - 1), num2);
                    sLDocument.SetCellValue("D" + num++, "Profit %2");
                    sLDocument.SetCellValue("E" + (num - 1), num3);
                    sLDocument.SetCellValue("D" + num++, "Sub-Total");
                    sLDocument.SetCellValue("E" + (num - 1), num4);
                    sLDocument.SetCellValue("D" + num++, "VAT");
                    sLDocument.SetCellValue("E" + (num - 1), num5);
                    sLDocument.SetCellValue("D" + num++, "General Total");
                    sLDocument.SetCellValue("E" + (num - 1), num4 + num5);
                    num += 2;
                    sLDocument.SetCellValue("A" + num, "In Written:");
                    num += 2;
                    sLDocument.SetCellStyle("A" + num, "E" + (num + 5), sLStyle);
                    sLDocument.SetCellValue("A" + num, "Bank Name");
                    sLDocument.SetCellValue("B" + num, "Commercial International Bank");
                    sLDocument.MergeWorksheetCells("B" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "Bank Address");
                    sLDocument.SetCellValue("B" + num, "8, Road 257 New Maadi, Cairo, Egypt");
                    sLDocument.MergeWorksheetCells("B" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "Branch Name");
                    sLDocument.SetCellValue("B" + num, "NEW MAADI");
                    sLDocument.MergeWorksheetCells("B" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "Account Holder Name");
                    sLDocument.SetCellValue("B" + num, "RTMS MECHANICAL MAINTENANCE CLEANING LLC");
                    sLDocument.MergeWorksheetCells("B" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "Account Number (EGP) & (USD)");
                    sLDocument.SetCellValue("B" + num, "100027082453 EGP      -      100027082445 USD");
                    sLDocument.MergeWorksheetCells("B" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "SWIFT Number");
                    sLDocument.SetCellValue("B" + num, "CIBEEGCX094");
                    sLDocument.MergeWorksheetCells("B" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "Please make all checks payable to RTMS Mechanical Maintenance Cleaning LLC.");
                    sLDocument.MergeWorksheetCells("A" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "RTMS Mechanical Maintenance & Cleaning LLC. Registration No: 81062");
                    sLDocument.MergeWorksheetCells("A" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "Income Tax File No: 720/6036/5");
                    sLDocument.MergeWorksheetCells("A" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "Sales Tax Registration No: 487-234-618");
                    sLDocument.MergeWorksheetCells("A" + num, "E" + num);
                    sLDocument.SaveAs(filename);
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
        }

        public static void SaveInvoice(Order order, List<int> ids, ApplicationUser customer, string filename)
        {
            try
            {
                using (SLDocument sLDocument = new SLDocument())
                {
                    sLDocument.SetColumnWidth("A", 40.0);
                    sLDocument.SetColumnWidth("B", 15.0);
                    sLDocument.SetColumnWidth("C", 15.0);
                    sLDocument.SetColumnWidth("D", 15.0);
                    sLDocument.SetColumnWidth("E", 15.0);
                    SLStyle sLStyle = new SLStyle();
                    sLStyle.SetPatternFill(PatternValues.Solid, System.Drawing.Color.LightGray, System.Drawing.Color.White);
                    sLStyle.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
                    sLStyle.SetVerticalAlignment(VerticalAlignmentValues.Center);
                    sLStyle.SetRightBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    sLStyle.SetLeftBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    sLStyle.SetTopBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    sLStyle.SetBottomBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    SLStyle sLStyle2 = new SLStyle();
                    sLStyle2.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
                    sLStyle2.SetVerticalAlignment(VerticalAlignmentValues.Center);
                    SLStyle sLStyle3 = new SLStyle();
                    sLStyle3.SetTopBorder(BorderStyleValues.Double, System.Drawing.Color.Black);
                    sLStyle3.SetBottomBorder(BorderStyleValues.Double, System.Drawing.Color.Black);
                    sLDocument.MergeWorksheetCells("A1", "C3");
                    sLDocument.MergeWorksheetCells("B4", "E4");
                    sLDocument.MergeWorksheetCells("B5", "E5");
                    sLDocument.MergeWorksheetCells("A6", "E6");
                    sLDocument.MergeWorksheetCells("B7", "E7");
                    sLDocument.MergeWorksheetCells("B7", "E8");
                    sLDocument.SetCellValue("A1", "TAX INVOICE");
                    sLDocument.SetCellStyle("A1", sLStyle2);
                    sLDocument.SetCellStyle("A4", "E4", sLStyle3);
                    sLDocument.SetCellValue("D1", "Invoice No");
                    sLDocument.SetCellValue("D2", "Reference No");
                    sLDocument.SetCellValue("D3", "Date");
                    sLDocument.SetCellValue("E1", "");
                    sLDocument.SetCellValue("E2", order.SerialNumber);
                    sLDocument.SetCellValue("E3", DateManager.GetLocalDate(new DateTime?(order.CreateDate)).Value.ToString("dd-MMM-yyyy"));
                    sLDocument.SetCellValue("A4", "Company");
                    sLDocument.SetCellValue("B4", ": RTMS MECHANICAL MAINTENANCE & CLEANING LLC");
                    sLDocument.SetCellStyle("A4", "E4", sLStyle3);
                    sLDocument.SetCellValue("A5", "Office Address");
                    sLDocument.SetCellValue("B5", ": 26, Orabi Street, Apt.2, Sarayat Maadi, Cairo, EGYPT");
                    sLDocument.SetCellStyle("A4", "E4", sLStyle3);
                    sLDocument.SetCellValue("A6", "Customer Information");
                    sLDocument.SetCellStyle("A6", "E6", sLStyle);
                    sLDocument.SetCellValue("A7", "Company");
                    sLDocument.SetCellValue("B7", customer.Name);
                    sLDocument.SetCellValue("A8", "Delivery Address");
                    sLDocument.SetCellValue("B8", customer.Address);
                    sLDocument.MergeWorksheetCells("A9", "E9");
                    sLDocument.SetCellValue("A10", "material");
                    sLDocument.SetCellValue("B10", "Category");
                    sLDocument.SetCellValue("C10", "Unit");
                    sLDocument.SetCellValue("D10", "Quantity");
                    sLDocument.SetCellValue("E10", "Cost");
                    sLDocument.SetCellValue("F10", "Total Cost");
                    //IEnumerable<OrderItem> arg_371_0 = order.Items;
                    //Func<OrderItem, bool> arg_371_1;
                    //if ((arg_371_1 = Excel.<>c.<>9__1_0) == null)
                    //{
                    //	arg_371_1 = (Excel.<>c.<>9__1_0 = new Func<OrderItem, bool>(Excel.<>c.<>9.<SaveInvoice>b__1_0));
                    //}
                    //List<OrderItem> list = arg_371_0.Where(arg_371_1).ToList<OrderItem>();

                    List<OrderItem> list = order.Items.Where<OrderItem>((Func<OrderItem, bool>)(item =>
                    {
                        //if (!(item.State == State.Complete))
                        //    return item.State == State.Alternative;
                        if (ids.Contains(item.Id))
                            return true;
                        else
                            return false;
                    })).ToList<OrderItem>();

                    int num = 11;
                    decimal d = new decimal(0, 0, 0, false, 1);
                    decimal num2 = new decimal(0, 0, 0, false, 1);
                    decimal num3 = new decimal(0, 0, 0, false, 1);
                    decimal num4 = new decimal(0, 0, 0, false, 1);
                    decimal num5 = new decimal(0, 0, 0, false, 1);
                    int i = 0;
                    while (i < list.Count)
                    {
                        sLDocument.SetCellStyle("A" + num, "E" + num, sLStyle);
                        sLDocument.SetCellValue("A" + num, list[i].Description);
                        sLDocument.SetCellValue("B" + num, list[i].ItemTypeId.HasValue ? list[i].ItemType.Name : list[i].TypeText);
                        sLDocument.SetCellValue("C" + num, list[i].TypeText);//Remarks
                        sLDocument.SetCellValue("D" + num, list[i].DeliveredQuantity);
                        sLDocument.SetCellValue("E" + num, list[i].SellingPrice);
                        decimal num6 = list[i].SellingPrice * (decimal)list[i].DeliveredQuantity;
                        sLDocument.SetCellValue("F" + num, num6);
                        d += num6;
                        i++;
                        num++;
                    }
                    sLDocument.MergeWorksheetCells("A" + num, "D" + (num + 4));
                    num2 = d * 0.015m;
                    num3 = (d + num2) * 0.02m;
                    num4 = d + num2 + num3;
                    //num5 = num4 * 0.1m;
                    num5 = num4 * 0.13m;
                    sLDocument.SetCellValue("E" + num++, "G.Exp. %1.5");
                    sLDocument.SetCellValue("F" + (num - 1), num2);
                    sLDocument.SetCellValue("E" + num++, "Profit %2");
                    sLDocument.SetCellValue("F" + (num - 1), num3);
                    sLDocument.SetCellValue("E" + num++, "Sub-Total");
                    sLDocument.SetCellValue("F" + (num - 1), num4);
                    sLDocument.SetCellValue("E" + num++, "VAT");
                    sLDocument.SetCellValue("F" + (num - 1), num5);
                    sLDocument.SetCellValue("E" + num++, "General Total");
                    sLDocument.SetCellValue("F" + (num - 1), num4 + num5);
                    num += 2;
                    sLDocument.SetCellValue("A" + num, "In Written:");
                    num += 2;
                    sLDocument.SetCellStyle("A" + num, "E" + (num + 5), sLStyle);
                    sLDocument.SetCellValue("A" + num, "Bank Name");
                    sLDocument.SetCellValue("B" + num, "Commercial International Bank");
                    sLDocument.MergeWorksheetCells("B" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "Bank Address");
                    sLDocument.SetCellValue("B" + num, "8, Road 257 New Maadi, Cairo, Egypt");
                    sLDocument.MergeWorksheetCells("B" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "Branch Name");
                    sLDocument.SetCellValue("B" + num, "NEW MAADI");
                    sLDocument.MergeWorksheetCells("B" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "Account Holder Name");
                    sLDocument.SetCellValue("B" + num, "RTMS MECHANICAL MAINTENANCE CLEANING LLC");
                    sLDocument.MergeWorksheetCells("B" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "Account Number (EGP) & (USD)");
                    sLDocument.SetCellValue("B" + num, "100027082453 EGP      -      100027082445 USD");
                    sLDocument.MergeWorksheetCells("B" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "SWIFT Number");
                    sLDocument.SetCellValue("B" + num, "CIBEEGCX094");
                    sLDocument.MergeWorksheetCells("B" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "Please make all checks payable to RTMS Mechanical Maintenance Cleaning LLC.");
                    sLDocument.MergeWorksheetCells("A" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "RTMS Mechanical Maintenance & Cleaning LLC. Registration No: 81062");
                    sLDocument.MergeWorksheetCells("A" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "Income Tax File No: 720/6036/5");
                    sLDocument.MergeWorksheetCells("A" + num, "E" + num);
                    sLDocument.SetCellValue("A" + ++num, "Sales Tax Registration No: 487-234-618");
                    sLDocument.MergeWorksheetCells("A" + num, "E" + num);
                    sLDocument.SaveAs(filename);
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
        }
        #region old code
        //public static void SaveInvoice(Order order, ApplicationUser customer, string filename)
        //{
        //    using (SLDocument slDocument1 = new SLDocument())
        //    {
        //        slDocument1.SetColumnWidth("A", 40.0);
        //        slDocument1.SetColumnWidth("B", 15.0);
        //        slDocument1.SetColumnWidth("C", 15.0);
        //        slDocument1.SetColumnWidth("D", 15.0);
        //        slDocument1.SetColumnWidth("E", 15.0);
        //        SLStyle Style1 = new SLStyle();
        //        Style1.SetPatternFill(PatternValues.Solid, System.Drawing.Color.LightGray, System.Drawing.Color.White);
        //        Style1.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
        //        Style1.SetVerticalAlignment(VerticalAlignmentValues.Center);
        //        Style1.SetRightBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
        //        Style1.SetLeftBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
        //        Style1.SetTopBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
        //        Style1.SetBottomBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
        //        SLStyle Style2 = new SLStyle();
        //        Style2.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
        //        Style2.SetVerticalAlignment(VerticalAlignmentValues.Center);
        //        SLStyle Style3 = new SLStyle();
        //        Style3.SetTopBorder(BorderStyleValues.Double, System.Drawing.Color.Black);
        //        Style3.SetBottomBorder(BorderStyleValues.Double, System.Drawing.Color.Black);
        //        slDocument1.MergeWorksheetCells("A1", "C3");
        //        slDocument1.MergeWorksheetCells("B4", "E4");
        //        slDocument1.MergeWorksheetCells("B5", "E5");
        //        slDocument1.MergeWorksheetCells("A6", "E6");
        //        slDocument1.MergeWorksheetCells("B7", "E7");
        //        slDocument1.MergeWorksheetCells("B7", "E8");
        //        slDocument1.SetCellValue("A1", "TAX INVOICE");
        //        slDocument1.SetCellStyle("A1", Style2);
        //        slDocument1.SetCellStyle("A4", "E4", Style3);
        //        slDocument1.SetCellValue("D1", "Invoice No");
        //        slDocument1.SetCellValue("D2", "Reference No");
        //        slDocument1.SetCellValue("D3", "Date");
        //        slDocument1.SetCellValue("E1", "");
        //        slDocument1.SetCellValue("E2", order.SerialNumber);
        //        slDocument1.SetCellValue("E3", DateManager.GetLocalDate(new DateTime?(order.CreateDate)).Value.ToString("dd-MMM-yyyy"));
        //        slDocument1.SetCellValue("A4", "Company");
        //        slDocument1.SetCellValue("B4", ": RTMS MECHANICAL MAINTENANCE & CLEANING LLC");
        //        slDocument1.SetCellStyle("A4", "E4", Style3);
        //        slDocument1.SetCellValue("A5", "Office Address");
        //        slDocument1.SetCellValue("B5", ": 26, Orabi Street, Apt.2, Sarayat Maadi, Cairo, EGYPT");
        //        slDocument1.SetCellStyle("A4", "E4", Style3);
        //        slDocument1.SetCellValue("A6", "Customer Information");
        //        slDocument1.SetCellStyle("A6", "E6", Style1);
        //        slDocument1.SetCellValue("A7", "Company");
        //        slDocument1.SetCellValue("B7", customer.Name);
        //        slDocument1.SetCellValue("A8", "Delivery Address");
        //        slDocument1.SetCellValue("B8", customer.Address);
        //        slDocument1.MergeWorksheetCells("A9", "E9");
        //        slDocument1.SetCellValue("A10", "material - Content");
        //        slDocument1.SetCellValue("B10", "Description");
        //        slDocument1.SetCellValue("C10", "Quantity");
        //        slDocument1.SetCellValue("D10", "Cost");
        //        slDocument1.SetCellValue("E10", "Total Cost");
        //        List<OrderItem> list = order.Items.Where<OrderItem>((Func<OrderItem, bool>)(item =>
        //        {
        //            if (!(item.State == State.Complete))
        //                return item.State == State.Alternative;
        //            return true;
        //        })).ToList<OrderItem>();
        //        int num1 = 11;
        //        Decimal num2 = new Decimal(0, 0, 0, false, (byte)1);
        //        Decimal num3 = new Decimal(0, 0, 0, false, (byte)1);
        //        Decimal num4 = new Decimal(0, 0, 0, false, (byte)1);
        //        Decimal Data1 = new Decimal(0, 0, 0, false, (byte)1);
        //        Decimal Data2 = new Decimal(0, 0, 0, false, (byte)1);
        //        int index = 0;
        //        while (index < list.Count)
        //        {
        //            slDocument1.SetCellStyle("A" + (object)num1, "E" + (object)num1, Style1);
        //            slDocument1.SetCellValue("A" + (object)num1, list[index].Description + " - " + (list[index].ItemTypeId.HasValue ? list[index].ItemType.Name : list[index].TypeText));
        //            slDocument1.SetCellValue("B" + (object)num1, list[index].Remarks);
        //            slDocument1.SetCellValue("C" + (object)num1, list[index].DeliveredQuantity);
        //            slDocument1.SetCellValue("D" + (object)num1, list[index].SellingPrice);
        //            Decimal Data3 = list[index].SellingPrice * (Decimal)list[index].DeliveredQuantity;
        //            slDocument1.SetCellValue("E" + (object)num1, Data3);
        //            num2 += Data3;
        //            ++index;
        //            ++num1;
        //        }
        //        slDocument1.MergeWorksheetCells("A" + (object)num1, "c" + (object)(num1 + 4));
        //        Decimal Data4 = num2 * new Decimal(15, 0, 0, false, (byte)3);
        //        Decimal Data5 = (num2 + Data4) * new Decimal(2, 0, 0, false, (byte)2);
        //        Data1 = num2 + Data4 + Data5;
        //        Data2 = Data1 * new Decimal(1, 0, 0, false, (byte)1);
        //        SLDocument slDocument2 = slDocument1;
        //        string str1 = "D";
        //        int num5 = num1;
        //        int num6 = 1;
        //        int num7 = num5 + num6;
        //        // ISSUE: variable of a boxed type
        //        __Boxed<int> local1 = (ValueType)num5;
        //        string CellReference1 = str1 + (object)local1;
        //        string Data6 = "G.Exp. %1.5";
        //        slDocument2.SetCellValue(CellReference1, Data6);
        //        slDocument1.SetCellValue("E" + (object)(num7 - 1), Data4);
        //        SLDocument slDocument3 = slDocument1;
        //        string str2 = "D";
        //        int num8 = num7;
        //        int num9 = 1;
        //        int num10 = num8 + num9;
        //        // ISSUE: variable of a boxed type
        //        __Boxed<int> local2 = (ValueType)num8;
        //        string CellReference2 = str2 + (object)local2;
        //        string Data7 = "Profit %2";
        //        slDocument3.SetCellValue(CellReference2, Data7);
        //        slDocument1.SetCellValue("E" + (object)(num10 - 1), Data5);
        //        SLDocument slDocument4 = slDocument1;
        //        string str3 = "D";
        //        int num11 = num10;
        //        int num12 = 1;
        //        int num13 = num11 + num12;
        //        // ISSUE: variable of a boxed type
        //        __Boxed<int> local3 = (ValueType)num11;
        //        string CellReference3 = str3 + (object)local3;
        //        string Data8 = "Sub-Total";
        //        slDocument4.SetCellValue(CellReference3, Data8);
        //        slDocument1.SetCellValue("E" + (object)(num13 - 1), Data1);
        //        SLDocument slDocument5 = slDocument1;
        //        string str4 = "D";
        //        int num14 = num13;
        //        int num15 = 1;
        //        int num16 = num14 + num15;
        //        // ISSUE: variable of a boxed type
        //        __Boxed<int> local4 = (ValueType)num14;
        //        string CellReference4 = str4 + (object)local4;
        //        string Data9 = "VAT";
        //        slDocument5.SetCellValue(CellReference4, Data9);
        //        slDocument1.SetCellValue("E" + (object)(num16 - 1), Data2);
        //        SLDocument slDocument6 = slDocument1;
        //        string str5 = "D";
        //        int num17 = num16;
        //        int num18 = 1;
        //        int num19 = num17 + num18;
        //        // ISSUE: variable of a boxed type
        //        __Boxed<int> local5 = (ValueType)num17;
        //        string CellReference5 = str5 + (object)local5;
        //        string Data10 = "General Total";
        //        slDocument6.SetCellValue(CellReference5, Data10);
        //        slDocument1.SetCellValue("E" + (object)(num19 - 1), Data1 + Data2);
        //        int num20 = num19 + 2;
        //        slDocument1.SetCellValue("A" + (object)num20, "In Written:");
        //        int num21 = num20 + 2;
        //        slDocument1.SetCellStyle("A" + (object)num21, "E" + (object)(num21 + 5), Style1);
        //        slDocument1.SetCellValue("A" + (object)num21, "Bank Name");
        //        slDocument1.SetCellValue("B" + (object)num21, "Commercial International Bank");
        //        slDocument1.MergeWorksheetCells("B" + (object)num21, "E" + (object)num21);
        //        int num22;
        //        slDocument1.SetCellValue("A" + (object)(num22 = num21 + 1), "Bank Address");
        //        slDocument1.SetCellValue("B" + (object)num22, "8, Road 257 New Maadi, Cairo, Egypt");
        //        slDocument1.MergeWorksheetCells("B" + (object)num22, "E" + (object)num22);
        //        int num23;
        //        slDocument1.SetCellValue("A" + (object)(num23 = num22 + 1), "Branch Name");
        //        slDocument1.SetCellValue("B" + (object)num23, "NEW MAADI");
        //        slDocument1.MergeWorksheetCells("B" + (object)num23, "E" + (object)num23);
        //        int num24;
        //        slDocument1.SetCellValue("A" + (object)(num24 = num23 + 1), "Account Holder Name");
        //        slDocument1.SetCellValue("B" + (object)num24, "RTMS MECHANICAL MAINTENANCE CLEANING LLC");
        //        slDocument1.MergeWorksheetCells("B" + (object)num24, "E" + (object)num24);
        //        int num25;
        //        slDocument1.SetCellValue("A" + (object)(num25 = num24 + 1), "Account Number (EGP) & (USD)");
        //        slDocument1.SetCellValue("B" + (object)num25, "100027082453 EGP      -      100027082445 USD");
        //        slDocument1.MergeWorksheetCells("B" + (object)num25, "E" + (object)num25);
        //        int num26;
        //        slDocument1.SetCellValue("A" + (object)(num26 = num25 + 1), "SWIFT Number");
        //        slDocument1.SetCellValue("B" + (object)num26, "CIBEEGCX094");
        //        slDocument1.MergeWorksheetCells("B" + (object)num26, "E" + (object)num26);
        //        int num27;
        //        slDocument1.SetCellValue("A" + (object)(num27 = num26 + 1), "Please make all checks payable to RTMS Mechanical Maintenance Cleaning LLC.");
        //        slDocument1.MergeWorksheetCells("A" + (object)num27, "E" + (object)num27);
        //        int num28;
        //        slDocument1.SetCellValue("A" + (object)(num28 = num27 + 1), "RTMS Mechanical Maintenance & Cleaning LLC. Registration No: 81062");
        //        slDocument1.MergeWorksheetCells("A" + (object)num28, "E" + (object)num28);
        //        int num29;
        //        slDocument1.SetCellValue("A" + (object)(num29 = num28 + 1), "Income Tax File No: 720/6036/5");
        //        slDocument1.MergeWorksheetCells("A" + (object)num29, "E" + (object)num29);
        //        int num30;
        //        slDocument1.SetCellValue("A" + (object)(num30 = num29 + 1), "Sales Tax Registration No: 487-234-618");
        //        slDocument1.MergeWorksheetCells("A" + (object)num30, "E" + (object)num30);
        //        slDocument1.SaveAs(filename);
        //    }
        //}
        #endregion

        public static void SaveItems(Order order, ApplicationUser customer, string filename)
        {
            try
            {
                using (SLDocument slDocument1 = new SLDocument())
                {
                    SLPageSettings ps = new SLPageSettings();
                    ps.Orientation = OrientationValues.Landscape;
                    slDocument1.SetPageSettings(ps);


                    slDocument1.SetColumnWidth("A", 15.0);
                    slDocument1.SetColumnWidth("B", 30.0);
                    slDocument1.SetColumnWidth("C", 15.0);
                    slDocument1.SetColumnWidth("D", 15.0);
                    slDocument1.SetColumnWidth("E", 15.0);
                    slDocument1.SetColumnWidth("F", 15.0);
                    slDocument1.SetColumnWidth("G", 15.0);
                    SLStyle Style1 = new SLStyle();
                    Style1.SetPatternFill(PatternValues.Solid, System.Drawing.Color.LightGray, System.Drawing.Color.White);
                    Style1.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
                    Style1.SetVerticalAlignment(VerticalAlignmentValues.Center);
                    Style1.SetRightBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    Style1.SetLeftBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    Style1.SetTopBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    Style1.SetBottomBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                    SLStyle Style2 = new SLStyle();
                    Style2.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
                    Style2.SetVerticalAlignment(VerticalAlignmentValues.Center);
                    SLStyle Style3 = new SLStyle();
                    Style3.SetTopBorder(BorderStyleValues.Double, System.Drawing.Color.Black);
                    Style3.SetBottomBorder(BorderStyleValues.Double, System.Drawing.Color.Black);
                    SLStyle slStyle = new SLStyle();
                    int num1 = 0;
                    System.Drawing.Color transparent1 = System.Drawing.Color.Transparent;
                    slStyle.SetTopBorder((BorderStyleValues)num1, transparent1);
                    int num2 = 0;
                    System.Drawing.Color transparent2 = System.Drawing.Color.Transparent;
                    slStyle.SetBottomBorder((BorderStyleValues)num2, transparent2);
                    Style1.SetRightBorder(BorderStyleValues.None, System.Drawing.Color.Transparent);
                    Style1.SetLeftBorder(BorderStyleValues.None, System.Drawing.Color.Transparent);
                    slDocument1.MergeWorksheetCells("A1", "C3");
                    slDocument1.MergeWorksheetCells("B4", "E4");
                    slDocument1.MergeWorksheetCells("B5", "E5");
                    slDocument1.MergeWorksheetCells("A6", "E6");
                    slDocument1.MergeWorksheetCells("B7", "E7");
                    slDocument1.MergeWorksheetCells("B7", "E8");
                    slDocument1.SetCellValue("A1", "Items List");
                    slDocument1.SetCellStyle("A1", Style2);
                    slDocument1.SetCellStyle("A4", "E4", Style3);
                    slDocument1.SetCellValue("D1", "Order No");
                    slDocument1.SetCellValue("D2", "Delivery No");
                    slDocument1.SetCellValue("D3", "Date");
                    slDocument1.SetCellValue("E1", order.SerialNumber);
                    slDocument1.SetCellValue("E2", "");
                    slDocument1.SetCellValue("E3", DateManager.GetLocalDate(new DateTime?(order.CreateDate)).Value.ToString("dd-MMM-yyyy"));
                    slDocument1.SetCellValue("A4", "Company");
                    slDocument1.SetCellValue("B4", ": RTMS MECHANICAL MAINTENANCE & CLEANING LLC");
                    slDocument1.SetCellStyle("A4", "E4", Style3);
                    slDocument1.SetCellValue("A5", "Office Address");
                    slDocument1.SetCellValue("B5", ": 26, Orabi Street, Apt.2, Sarayat Maadi, Cairo, EGYPT");
                    slDocument1.SetCellStyle("A5", "E5", Style3);
                    slDocument1.SetCellValue("A6", "Customer Information");
                    slDocument1.SetCellStyle("A6", "E6", Style1);
                    slDocument1.SetCellValue("A7", "Company");
                    slDocument1.SetCellValue("B7", customer.Name);
                    slDocument1.SetCellValue("A8", "Delivery Address");
                    slDocument1.SetCellValue("B8", customer.Address);
                    slDocument1.MergeWorksheetCells("A9", "E9");
                    slDocument1.SetCellValue("A10", "No");
                    //slDocument1.SetCellValue("B10", "Status");
                    slDocument1.SetCellValue("B10", "Item");
                    slDocument1.SetCellValue("C10", "Unit");
                    slDocument1.SetCellValue("D10", "Qty");
                    slDocument1.SetCellValue("E10", "Description");
                    //slDocument1.SetCellValue("G10", "Unit");
                    List<OrderItem> list = order.Items.ToList<OrderItem>();
                    int num3 = 11;
                    int index = 0;
                    while (index < list.Count)
                    {
                        slDocument1.SetCellStyle("A" + (object)num3, "J" + (object)num3, Style1);
                        slDocument1.SetCellValue("A" + (object)num3, index + 1);
                        //slDocument1.SetCellValue("B" + (object)num3, list[index].State);
                        SLDocument slDocument2 = slDocument1;
                        string CellReference1 = "B" + (object)num3;
                        int? nullable = list[index].ItemTypeId;
                        string Data1 = nullable.HasValue ? list[index].ItemType.Name : list[index].TypeText;
                        slDocument2.SetCellValue(CellReference1, Data1);
                        SLDocument slDocument3 = slDocument1;
                        string CellReference2 = "C" + (object)num3;
                        nullable = list[index].UnitId;
                        string Data2 = nullable.HasValue ? list[index].Unit.Name : list[index].UnitText;
                        slDocument3.SetCellValue(CellReference2, Data2);
                        slDocument1.SetCellValue("D" + (object)num3, list[index].Quantity);
                        slDocument1.SetCellValue("E" + (object)num3, list[index].Description + list[index].Remarks);//Remarks
                        //slDocument1.SetCellValue("G" + (object)num3, list[index].Description);
                        ++index;
                        ++num3;
                    }

                    SLStyle Style11 = slDocument1.CreateStyle();
                    Style11.SetHorizontalAlignment(HorizontalAlignmentValues.Center);
                    Style11.Border.LeftBorder.BorderStyle = BorderStyleValues.Thick;
                    Style11.Border.LeftBorder.Color = System.Drawing.Color.Black;

                    Style11.Border.BottomBorder.BorderStyle = BorderStyleValues.Thick;
                    Style11.Border.BottomBorder.Color = System.Drawing.Color.Black;

                    Style11.Border.RightBorder.BorderStyle = BorderStyleValues.Thick;
                    Style11.Border.RightBorder.Color = System.Drawing.Color.Black;

                    Style11.Border.TopBorder.BorderStyle = BorderStyleValues.Thick;
                    Style11.Border.TopBorder.Color = System.Drawing.Color.Black;

                    //Style11.SetRightBorder(BorderStyleValues.Hair, System.Drawing.Color.Black);
                    //
                    //Style11.SetTopBorder(BorderStyleValues.Double, SLThemeColorIndexValues.Accent6Color);
                    //Style11.SetDiagonalBorder(BorderStyleValues.MediumDashDotDot, SLThemeColorIndexValues.Accent3Color, 0.2);
                    //
                    //Style11.Border.DiagonalUp = true;
                    //Style11.Border.DiagonalDown = true;

                    num3 += 2;
                    slDocument1.SetCellValue("B" + num3, "Authorized Prior Procurment");
                    slDocument1.SetCellStyle("B" + num3, "F" + num3, Style11);
                    slDocument1.MergeWorksheetCells("B" + num3, "F" + num3);
                    num3 += 1;
                    slDocument1.SetCellValue("B" + num3, "Personnel");
                    slDocument1.SetCellValue("C" + num3, "Requester");
                    slDocument1.SetCellValue("D" + num3, "Resp. Manager");
                    slDocument1.SetCellValue("E" + num3, "APM");
                    slDocument1.SetCellValue("F" + num3, "Approval");
                    slDocument1.SetCellStyle("B" + num3, Style11);
                    slDocument1.SetCellStyle("C" + num3, Style11);
                    slDocument1.SetCellStyle("D" + num3, Style11);
                    slDocument1.SetCellStyle("E" + num3, Style11);
                    slDocument1.SetCellStyle("F" + num3, Style11);
                    num3 += 1;
                    slDocument1.SetRowHeight(num3, 20);
                    slDocument1.SetCellValue("B" + num3, "Signture");
                    slDocument1.SetCellStyle("B" + num3, Style11);
                    slDocument1.SetCellStyle("C" + num3, Style11);
                    slDocument1.SetCellStyle("D" + num3, Style11);
                    slDocument1.SetCellStyle("E" + num3, Style11);
                    slDocument1.SetCellStyle("F" + num3, Style11);
                    num3 += 1;
                    slDocument1.SetRowHeight(num3, 20);
                    slDocument1.SetCellValue("B" + num3, "Date");
                    slDocument1.SetCellStyle("B" + num3, Style11);
                    slDocument1.SetCellStyle("C" + num3, Style11);
                    slDocument1.SetCellStyle("D" + num3, Style11);
                    slDocument1.SetCellStyle("E" + num3, Style11);
                    slDocument1.SetCellStyle("F" + num3, Style11);
                    num3 += 2;



                    slDocument1.SetCellValue("B" + num3, "After Procurment");
                    slDocument1.SetCellStyle("B" + num3, "E" + num3, Style11);
                    slDocument1.MergeWorksheetCells("B" + num3, "E" + num3);
                    num3 += 1;
                    slDocument1.SetCellValue("B" + num3, "Recived Date");
                    slDocument1.SetCellValue("C" + num3, "Evidence");
                    slDocument1.SetCellValue("D" + num3, "Resp. Manager");
                    slDocument1.SetCellValue("E" + num3, "Approval Yun Dong Young");
                    slDocument1.SetCellStyle("B" + num3, Style11);
                    slDocument1.SetCellStyle("C" + num3, Style11);
                    slDocument1.SetCellStyle("D" + num3, Style11);
                    slDocument1.SetCellStyle("E" + num3, Style11);
                    num3 += 1;
                    slDocument1.SetRowHeight(num3, 20);
                    slDocument1.SetCellStyle("B" + num3, Style11);
                    slDocument1.SetCellStyle("C" + num3, Style11);
                    slDocument1.SetCellStyle("D" + num3, Style11);
                    slDocument1.SetCellStyle("E" + num3, Style11);
                    num3 += 1;
                    slDocument1.SetRowHeight(num3, 20);
                    slDocument1.SetCellStyle("B" + num3, Style11);
                    slDocument1.SetCellStyle("C" + num3, Style11);
                    slDocument1.SetCellStyle("D" + num3, Style11);
                    slDocument1.SetCellStyle("E" + num3, Style11);
                    num3 += 2;


                    slDocument1.SetCellValue("B" + num3, "Hand Writing Invoice Authorization Table");
                    slDocument1.SetCellStyle("B" + num3, "C" + num3, Style11);
                    slDocument1.MergeWorksheetCells("B" + num3, "C" + num3);
                    num3 += 1;
                    slDocument1.SetCellValue("B" + num3, "Resp. Manager");
                    slDocument1.SetCellValue("C" + num3, "Signture & date");
                    slDocument1.SetCellStyle("B" + num3, Style11);
                    slDocument1.SetCellStyle("C" + num3, Style11);
                    num3 += 1;
                    //slDocument1.SetRowHeight(num3, 20);
                    slDocument1.SetRowHeight(num3, 20);
                    slDocument1.SetCellValue("B" + num3, "Accountatnt Mai Hussien");
                    slDocument1.SetCellStyle("B" + num3, Style11);
                    slDocument1.SetCellStyle("C" + num3, Style11);
                    num3 += 1;
                    slDocument1.SetRowHeight(num3, 20);
                    slDocument1.SetCellValue("B" + num3, "Approval  Yun Dong Young");
                    slDocument1.SetCellStyle("B" + num3, Style11);
                    slDocument1.SetCellStyle("C" + num3, Style11);



                    slDocument1.SaveAs(filename);
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
        }
    }
}
