﻿angular.module('app.api').factory('app.api.customer', ['$httpParamSerializerJQLike', '$http', 'app.data.settings','app.data.shared', function ($httpParamSerializerJQLike, $http, appSettings,sharedData) {
    
 
    function _create(data) {
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.post(appSettings.URI + '/api/customer/create', data, {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return { OK: true, response: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }
    function _get() {
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.get(appSettings.URI + '/api/customer/get', {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return { OK: true, response: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }
    function _remove(data) {
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.delete(appSettings.URI + '/api/customer/' + data.id, {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return { OK: true, response: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }
    function _update(data) {
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.post(appSettings.URI + '/api/customer/update', data, {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return { OK: true, response: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }
    function _getSupervisorUsers() {
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.get(appSettings.URI + '/api/customer/GetSupervisors', {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return { OK: true, response: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }
    return {
        create: _create,
        update: _update,
        remove: _remove,
        get: _get,
        getSupervisorUsers: _getSupervisorUsers
	}

}]);
    
