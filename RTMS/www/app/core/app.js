﻿var dummy = Math.random()
var mainApp = angular.module('mainApp', ['ui.router', 'snap', 'lumx', 'app.data', 'app.api'])
.config(['$httpProvider', '$stateProvider', '$urlRouterProvider', 'snapRemoteProvider', function ($httpProvider, $stateProvider, $urlRouterProvider, snapRemoteProvider) {
    $stateProvider
    .state('login', {
        url: '/login',
        templateUrl: "/www/app/views/login.html?dummy=" + dummy
    })
    .state('customers', {
        url: '/customers',
        templateUrl: "/www/app/views/customers.html?dummy=" + dummy
    })
    .state('profile', {
        url: '/profile',
        templateUrl: "/www/app/views/profile.html?dummy=" + dummy
    })
    .state('orders', {
        url: '/orders',
        templateUrl: "/www/app/views/orders.html?dummy=" + dummy,
        params: { openAddDialog: false }
    })
    .state('items', {
        url: '/items',
        templateUrl: "/www/app/views/items.html?dummy=" + dummy,
        params: { order: null, openAddDialog: false }
    })
    .state('item-types', {
        url: '/item-types',
        templateUrl: "/www/app/views/item-types.html?dummy=" + dummy
    })
        .state('item-units', {
            url: '/item-units',
            templateUrl: "/www/app/views/item-units.html?dummy=" + dummy
        })
    .state('suppliers', {
        url: '/suppliers',
        templateUrl: "/www/app/views/suppliers.html?dummy=" + dummy
    })
    .state('report', {
        url: '/report',
        templateUrl: "/www/app/views/report.html?dummy=" + dummy
    });
    $urlRouterProvider.otherwise('login');

    //
    snapRemoteProvider.globalOptions = {
        disable: 'left',
        tapToClose: true
        // ... others options
    }
    //$httpProvider.defaults.withCredentials = true;
    //$httpProvider.defaults.useXDomain = true;
    
    $httpProvider.interceptors.push(['$q', '$rootScope', '$injector',function ($q, $rootScope, $injector) {
        if($rootScope.showOverlay == undefined)
            $rootScope.showOverlay = 0;
        return {
            'request': function (config) {
                $rootScope.showOverlay++;
                return config;
            },
            'requestError': function (rejection) {
                // do something on error
                //if ($rootScope.showOverlay > 0)
                //    $rootScope.showOverlay--;
                return $q.reject(rejection);
            },

            'response': function (response) {
                // same as above
                if ($rootScope.showOverlay > 0)
                    $rootScope.showOverlay--;
                return response;
            },
            'responseError': function (rejection) {
                // do something on error
                if ($rootScope.showOverlay > 0)
                    $rootScope.showOverlay--;
                var status = rejection.status;
                if (status == 401) {
                    $injector.get('app.api.account').logout().then(function () {
                        $injector.get('$state').go('login');
                    });
                }
                return $q.reject(rejection);
            }
        };
    }]);
}])
.run(['$state', '$rootScope', 'app.api.account', 'app.data.shared', 'app.data.settings', function ($state, $rootScope, accountApi, sharedData, appSettings) {
    $rootScope.user = sharedData.user;
    $rootScope.showOverlay = 0;
    $rootScope.currentState = '';

    if (localStorage) {
        var userData = angular.fromJson(localStorage.getItem(appSettings.dataKey));
        if (userData) {
            accountApi.authorize(userData, true);

        }
        console.log(appSettings.dataKey);
        console.log(userData);
    }

    prepareApp();

    function prepareApp() {
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            if (!sharedData.user.isAuthenticated && toState.name != 'login') {
                event.preventDefault();
                $state.go('login');
            }
            else if (sharedData.user.isAuthenticated) {
                if (toState.name == 'login') {
                    event.preventDefault();
                    $state.go('orders');
                }
                else if (sharedData.user.roles.isSuper || sharedData.user.roles.isAdmin) {
                    if (toState.name == 'profile') {
                        event.preventDefault();
                        $state.go('orders');
                    }
                }
                else if (toState.name == 'customers' || toState.name == 'item-types' || toState.name == 'item-units'
                        || toState.name == 'suppliers') {
                    event.preventDefault();
                    $state.go('orders');
                }
            }
            $rootScope.currentState = toState.name;
        });
    }
}]);
// jQuery needed, uses Bootstrap classes, adjust the path of templateUrl
mainApp.directive('fileDownload', function () {
    return {
        restrict: 'E',
        templateUrl: '/www/app/views/fileDownload.tpl.html',
        scope: true,
        link: function (scope, element, attr) {
            var anchor = element.children()[0];
            $(anchor).text(attr.preparetext);
            // When the download starts, disable the link
            scope.$on('download-start', function () {
                $(anchor).attr('disabled', 'disabled');
            });

            // When the download finishes, attach the data to the link. Enable the link and change its appearance.
            scope.$on('downloaded', function (event, data) {
                $(anchor).attr({
                    href: 'data:application/excel;base64,' + data,
                    download: attr.filename
                }).removeAttr('disabled').text(attr.savetext);
                // Also overwrite the download pdf function to do nothing.
                scope.downloadFile = function () {
                };
            });
        },
        controller: ['$scope', '$attrs', '$http', function ($scope, $attrs, $http) {
            $scope.downloadFile = function () {
                $scope.$emit('download-start');
                var token = localStorage.getItem('token');
                $http.post($attrs.url, {}, {
                    headers: { 'Authorization': 'Bearer ' + token}
                }).then(function (response) {
                    $scope.$emit('downloaded', response.data);
                });
                /*$http.get($attrs.url).then(function (response) {
                    $scope.$emit('downloaded', response.data);
                });*/
            };
        }]
    }
});

mainApp.directive("fileBind", [function () {
    return {
        scope: {
            fileBind: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    scope.fileBind = changeEvent.target.files[0];
                    // or all selected files:
                    // scope.fileread = changeEvent.target.files;
                });
            });
        }
    }
}]);


mainApp.directive("jqdatepicker", function () {

    function link(scope, element, attrs, controller) {
        // CALL THE "datepicker()" METHOD USING THE "element" OBJECT.
        element.datepicker({
            onSelect: function (dt) {
                scope.$apply(function () {
                    // UPDATE THE VIEW VALUE WITH THE SELECTED DATE.
                    controller.$setViewValue(dt);
                });
            },
            dateFormat: "mm/dd/yy"      // SET THE FORMAT.
        });
    }

    return {
        require: 'ngModel',
        link: link
    };
});



