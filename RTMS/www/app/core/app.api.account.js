﻿angular.module('app.api').factory('app.api.account', ['$httpParamSerializer', '$http', 'app.data.settings', 'app.data.shared', function ($httpParamSerializer, $http, appSettings, sharedData) {
    function _authorize(data, isAuthenticated) {
        debugger;
        sharedData.user.isAuthenticated = false;
        sharedData.user.roles = {};
        sharedData.user.claims = {};
        if (isAuthenticated) {
            sharedData.user.userName = data.userName;
            sharedData.user.isAuthenticated = true;
            if (data.roles) {
                sharedData.user.roles.isSupervisor = data.roles.indexOf('supervisor') != -1;
                sharedData.user.roles.isSuper = !sharedData.user.roles.isSupervisor && data.roles.indexOf('super') != -1;
                sharedData.user.roles.isAdmin = data.roles.indexOf('admin') != -1;
                sharedData.user.roles.isCustomer = data.roles.indexOf('customer') != -1;

                if (sharedData.user.roles.isSuper || sharedData.user.roles.isAdmin) {
                    sharedData.user.claims.changePassword = true;
                    sharedData.user.claims.manageCustomers = true;
                    sharedData.user.claims.manageTypes = true;
                    sharedData.user.claims.manageUnits = true;
                    sharedData.user.claims.viewHome = true;
                    sharedData.user.claims.manageProfile = false;
                    sharedData.user.claims.manageOrders = true;
                    sharedData.user.claims.manageItems = true;
                    sharedData.user.claims.manageSuppliers = true;
                }
                else if (sharedData.user.roles.isCustomer || sharedData.user.roles.isSupervisor) {
                    sharedData.user.claims.viewHome = true;
                    sharedData.user.claims.manageProfile = true;
                    sharedData.user.claims.manageOrders = true;
                    sharedData.user.claims.manageItems = true;
                }
            }
        }
    }
    function _authenticate(data) {
        debugger;
        angular.extend(data, { grant_type: 'password' })
        return $http.post(appSettings.URI + '/Token', $httpParamSerializer(data), {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        })
            .then(
                function (response) {
                    localStorage.setItem(appSettings.tokenKey, response.data.access_token);
                    localStorage.setItem(appSettings.userNameKey, response.data.userName);
                    localStorage.setItem(appSettings.dataKey, angular.toJson(response.data));
                    
                    _authorize(response.data, true);
                    return { OK: true, response: response };
                },
                function (err) {
                    _authorize(null, false);
                    return { ERROR: true, response: err };
                }
            );
    }
    function _logout(){
        localStorage.clear();//.removeItem(appSettings.tokenKey)
        _authorize(null, false);
    }
    function _changePassword(data) {
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.post(appSettings.URI + '/api/account/ChangePassword', data, {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return _authenticate({userName:sharedData.user.userName,password:data.newPassword})
                    //return { OK: true, result: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }


    function _get() {
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.get(appSettings.URI + '/api/account/', {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return { OK: true, response: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }
    return {
        authenticate: _authenticate,
        authorize: _authorize,
	    logout:_logout,
	    changePassword: _changePassword,
        get:_get
	}

}]);
    
