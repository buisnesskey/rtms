﻿angular.module('app.api').factory('app.api.orders', ['$httpParamSerializer', '$http', 'app.data.settings', 'app.data.shared', function ($httpParamSerializer, $http, appSettings, sharedData) {
    
 
    function _create(data) {
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.post(appSettings.URI + '/api/orders/', data, {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return { OK: true, response: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }
    function _get(search) {
        if (!search) {
            search = {};
        }
        angular.extend(search, { dummy: 1 });
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.get(appSettings.URI + '/api/orders/?' + $httpParamSerializer(search), {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return { OK: true, response: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }
    function _remove(data) {
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.delete(appSettings.URI + '/api/orders/'+data.id, {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return { OK: true, response: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }
    function _update(data) {
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.post(appSettings.URI + '/api/orders/' + data.id, data, {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return { OK: true, response: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }
    function _sendReceipt(data) {
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.post(appSettings.URI + '/api/orders/sendreceipt/' + data.id, data, {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return { OK: true, response: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }
    function _sendInvoice(data) {
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.post(appSettings.URI + '/api/orders/sendinvoice/' + data.id, data, {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return { OK: true, response: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }
    
    return {
        create: _create,
        update: _update,
        remove: _remove,
        get: _get,
        sendReceipt: _sendReceipt,
        sendInvoice: _sendInvoice
	}

}]);
    
