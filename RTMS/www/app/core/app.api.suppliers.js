﻿angular.module('app.api').factory('app.api.suppliers', ['$httpParamSerializer', '$http', 'app.data.settings', 'app.data.shared', function ($httpParamSerializer, $http, appSettings, sharedData) {
    
 
    function _create(data) {
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.post(appSettings.URI + '/api/suppliers/', data, {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return { OK: true, response: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }
    function _get() {
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.get(appSettings.URI + '/api/suppliers/', {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return { OK: true, response: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }
    function _remove(data) {
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.delete(appSettings.URI + '/api/suppliers/' + data.id, {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return { OK: true, response: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }
    function _update(data) {
        var token = localStorage.getItem(appSettings.tokenKey);
        if (!token) {
            return $q(function (resolve, reject) {
                reject({ notAuthenticated: true });
            });
        }
        else {
            return $http.post(appSettings.URI + '/api/suppliers/' + data.id, data, {
                headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(
                function (response) {
                    return { OK: true, response: response };
                },
                function (err) {
                    return { ERROR: true, response: err };
                }
            );
        }
    }
    return {
        create: _create,
        update: _update,
        remove: _remove,
        get:_get
	}

}]);
    
