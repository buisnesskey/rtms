﻿var appData = angular.module('app.data', [])
.config(function(){
})
.run(function(){
});

appData.factory('app.data.settings', [function () {
    return {
        URI: 'http://localhost:49374',
        tokenKey:'token',
        usernameKey: 'userName',
        dataKey: 'data',
    }
}]);
appData.factory('app.data.shared',[function () {
	return {
	    user: { claims: {}, roles: {}, userName:'',isAuthenticated:false}
	}
}]);
    

