﻿angular.module('mainApp').controller('suppliersCtrl',['$scope', 'LxDialogService', 'LxNotificationService', '$state', 'app.api.suppliers', 'app.data.shared', function ($scope, LxDialogService, LxNotificationService, $state, suppliersApi, sharedData) {
    $scope.dialogId = 'formDialog';
    $scope.model = {};

    $scope.toggleSelection = function (_item,event) {
        if (event)
            event.preventDefault;
        _item.checked = !_item.checked;
    }

    $scope.showDialog = function (id) {
        LxDialogService.open(id);
    }
     $scope.viewMode = false;
    $scope.view = function (item,event) {
        if (event)
            event.preventDefault();
        $scope.model = item;
        $scope.viewMode = true;
        LxDialogService.open($scope.dialogId);
    }
    $scope.add = function () {
        $scope.model = {};
        $scope.viewMode = false;
        LxDialogService.open($scope.dialogId);
    }
    $scope.edit = function () {
        var index = -1;
        for (var i = 0; i < $scope.suppliers.length; i++) {
            if ($scope.suppliers[i].checked) {
                index = i;
                break;
            }
        }
        if (index > -1) {
            $scope.model = $scope.suppliers[i];
            $scope.viewMode = false;
            LxDialogService.open($scope.dialogId);
        }
        else {
            LxNotificationService.alert('Notice', 'Please select one entry to edit', 'Ok', function (answer) {
            });
        }
    }

    $scope.delete = function () {
        var index = -1;
        for (var i = 0; i < $scope.suppliers.length; i++) {
            if ($scope.suppliers[i].checked) {
                index = i;
                break;
            }
        }
        if (index > -1) {
            LxNotificationService.confirm('Confirm', 'Are you sure you want to delete this entry',
            {
                cancel: 'Cancel',
                ok: 'Yes'
            }, function (answer) {
                if (answer) {
                    suppliersApi.remove({ id: $scope.suppliers[index].id }).then(function (result) {
                        if (result.OK) {
                            $scope.suppliers.splice(index, 1);
                        }
                    });
                }
                else {
                }
            });
        }
        else {
            LxNotificationService.alert('Notice', 'Please select one entry to delete', 'Ok', function (answer) {
            });
        }

    }
    $scope.suppliers = [];
    suppliersApi.get().then(
           function (result) {
               console.log(JSON.stringify(result));
               if (result.OK) {
                   $scope.suppliers = result.response.data;
               }
               else {
               }
           });

    $scope.save = function () {
        if (!$scope.model.id) {//add new
            suppliersApi.create($scope.model).then(
              function (result) {
                  console.log(JSON.stringify(result));
                  if (result.OK) {
                      $scope.model.id = 
                      $scope.suppliers.push({
                          id: result.response.data.id,
                          name: $scope.model.name,
                          phone: $scope.model.phone,
                          contactPerson: $scope.model.contactPerson,
                          address: $scope.model.address,
                          isActive: $scope.model.isActive
                      });
                      $scope.model = {};
                      LxDialogService.close($scope.dialogId);
                  }
                  else {
                  }
              });
        }
        else {// update
            suppliersApi.update($scope.model).then(
              function (result) {
                  console.log(JSON.stringify(result));
                  if (result.OK) {
                      $scope.model.checked = false;
                      $scope.model = {};
                      LxDialogService.close($scope.dialogId);
                  }
                  else {
                  }
              });
        }
    }

    $scope.orderby = 'name';
    $scope.orderBy = function (field) {
        $scope.orderby = field;
    }
}]);