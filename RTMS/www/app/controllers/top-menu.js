﻿angular.module('mainApp').controller('topMenuCtrl', ['$scope', '$state', 'LxDialogService', 'app.data.shared', 'app.api.account', function ($scope, $state, LxDialogService, sharedData, accountApi) {

    $scope.addOrder = function (e) {
        if (e) {
            e.preventDefault();
        }
        $state.go('orders', { openAddDialog: true });
    }
                      
}]);