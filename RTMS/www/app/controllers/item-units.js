﻿angular.module('mainApp').controller('itemUnitsCtrl', ['$scope', 'LxDialogService', 'LxNotificationService', '$state', 'app.api.units', 'app.data.shared', function ($scope, LxDialogService, LxNotificationService, $state, unitsApi, sharedData) {
   
    $scope.dialogId = 'formDialog';
    $scope.model = {};

    $scope.toggleSelection = function (_item,event) {
        if (event)
            event.preventDefault;
        _item.checked = !_item.checked;
    }

    $scope.showDialog = function (id) {
        LxDialogService.open(id);
    }

    $scope.viewMode = false;
    $scope.view = function (item,event) {
        if (event)
            event.preventDefault();
        $scope.model = item;
        $scope.viewMode = true;
        LxDialogService.open($scope.dialogId);
    }

    $scope.add = function () {
        $scope.model = {};
        $scope.viewMode = false;
        LxDialogService.open($scope.dialogId);
    }
    $scope.edit = function () {
        var index = -1;
        for (var i = 0; i < $scope.units.length; i++) {
            if ($scope.units[i].checked) {
                index = i;
                break;
            }
        }
        if (index > -1) {
            $scope.model = $scope.units[i];
            $scope.viewMode = false;
            LxDialogService.open($scope.dialogId);
        }
        else {
            LxNotificationService.alert('Notice', 'Please select one entry to edit', 'Ok', function (answer) {
            });
        }
    }

    $scope.delete = function () {
        var index = -1;
        for (var i = 0; i < $scope.units.length; i++) {
            if ($scope.units[i].checked) {
                index = i;
                break;
            }
        }
        if (index > -1) {
            LxNotificationService.confirm('Confirm', 'Are you sure you want to delete this entry',
            {
                cancel: 'Cancel',
                ok: 'Yes'
            }, function (answer) {
                if (answer) {
                    unitsApi.remove({ id: $scope.units[index].id }).then(function (result) {
                        if (result.OK) {
                            $scope.units.splice(index, 1);
                        }
                    });
                }
                else {
                }
            });
        }
        else {
            LxNotificationService.alert('Notice', 'Please select one entry to delete', 'Ok', function (answer) {
            });
        }

    }
    $scope.units = [];
    unitsApi.get().then(
           function (result) {
               console.log(JSON.stringify(result));
               if (result.OK) {
                   $scope.units = result.response.data;
               }
               else {
               }
           });

    $scope.save = function () {
        if (!$scope.model.id) {//add new
            unitsApi.create($scope.model).then(
              function (result) {
                  console.log(JSON.stringify(result));
                  if (result.OK) {
                      $scope.model.id = result.response.data.id;
                      $scope.units.push({
                          id: $scope.model.id,
                          name: $scope.model.name,
                          code: $scope.model.code,
                          isActive: $scope.model.isActive
                      });
                      $scope.model = {};
                      LxDialogService.close($scope.dialogId);
                  }
                  else {
                  }
              });
        }
        else {// update
            unitsApi.update($scope.model).then(
              function (result) {
                  console.log(JSON.stringify(result));
                  if (result.OK) {
                      $scope.model.checked = false;
                      $scope.model = {};
                      LxDialogService.close($scope.dialogId);
                  }
                  else {
                  }
              });
        }
    }

  $scope.orderby = 'name';
    $scope.orderBy = function (field) {
        $scope.orderby = field;
    }
        /*
    $scope.units= [{ ID: 0, Name: 'Bottle', Checked: false },
        { ID: 1, Name: 'Bottle', Checked: false }, { ID: 2, Name: 'Bag', Checked: false },
        { ID: 3, Name: 'Each', Checked: false }, { ID: 4, Name: 'Litre', Checked: false },
        { ID: 5, Name: 'Meter', Checked: false }, { ID: 6, Name: 'Kgm', Checked: false },
        { ID: 7, Name: 'Ton', Checked: false }, { ID: 8, Name: 'Barrel', Checked: false }];
   */
    
}]);