﻿angular.module('mainApp').controller('itemTypesCtrl', ['$scope', 'LxDialogService', 'LxNotificationService', '$state', 'app.api.units','app.api.types', 'app.data.shared', function ($scope, LxDialogService, LxNotificationService, $state, unitsApi,typesApi, sharedData) {

    $scope.dialogId = 'formDialog';
    $scope.model = {};
    $scope.units = {};

    $scope.toggleSelection = function (_item,event) {
        if (event)
            event.preventDefault;
        _item.checked = !_item.checked;
    }

    $scope.showDialog = function (id) {
        LxDialogService.open(id);
    }

    function resetUnits() {
        for (var i = 0; i < $scope.units.length; i++) {
            $scope.units[i].checked = false;
        }
    }
    function setUnits(unitIds) {
        for (var i = 0; i < $scope.units.length; i++) {
                $scope.units[i].checked = unitIds.indexOf($scope.units[i].id) > -1;
        }
    }
    function getUnitsIds() {
        var result= new Array();
        for (var i = 0; i < $scope.units.length; i++) {
            if ($scope.units[i].checked)
                result.push($scope.units[i].id);
        }
        return result;
    }
 $scope.viewMode = false;
    $scope.view = function (item, event) {
     if (event)
         event.preventDefault();
        $scope.model = item;
        $scope.viewMode = true;
        LxDialogService.open($scope.dialogId);
    }
    $scope.add = function () {
        resetUnits();
        $scope.model = {};
        $scope.viewMode = false;
        LxDialogService.open($scope.dialogId);
    }
    $scope.edit = function () {
        var index = -1;
        for (var i = 0; i < $scope.types.length; i++) {
            if ($scope.types[i].checked) {
                index = i;
                break;
            }
        }
        if (index > -1) {
            $scope.model = $scope.types[i];
            setUnits($scope.model.units);
            $scope.viewMode = false;
            LxDialogService.open($scope.dialogId);
        }
        else {
            LxNotificationService.alert('Notice', 'Please select one entry to edit', 'Ok', function (answer) {
            });
        }
    }

    $scope.delete = function () {
        var index = -1;
        for (var i = 0; i < $scope.types.length; i++) {
            if ($scope.types[i].checked) {
                index = i;
                break;
            }
        }
        if (index > -1) {
            LxNotificationService.confirm('Confirm', 'Are you sure you want to delete this entry',
            {
                cancel: 'Cancel',
                ok: 'Yes'
            }, function (answer) {
                if (answer) {
                    typesApi.remove({ id: $scope.types[index].id }).then(function (result) {
                        if (result.OK) {
                            $scope.types.splice(index, 1);
                        }
                    });
                }
                else {
                }
            });
        }
        else {
            LxNotificationService.alert('Notice', 'Please select one entry to delete', 'Ok', function (answer) {
            });
        }

    }
    $scope.types = [];
    unitsApi.get().then(
          function (result) {
              console.log(JSON.stringify(result));
              if (result.OK) {
                  $scope.units = result.response.data;
              }
              else {
              }
          });
    typesApi.get().then(
           function (result) {
               console.log(JSON.stringify(result));
               if (result.OK) {
                   $scope.types = result.response.data;
               }
               else {
               }
           });

    $scope.save = function () {
        if (!$scope.model.id) {//add new
            $scope.model.units = getUnitsIds();
            typesApi.create($scope.model).then(
              function (result) {
                  console.log(JSON.stringify(result));
                  if (result.OK) {
                      $scope.model.id = result.response.data.id;
                      $scope.types.push({
                          id: $scope.model.id,
                          name: $scope.model.name,
                          code: $scope.model.code,
                          isActive: $scope.model.isActive,
                          units: $scope.model.units
                      });
                      $scope.model = {};
                      resetUnits();
                      LxDialogService.close($scope.dialogId);
                  }
                  else {
                  }
              });
        }
        else {// update
            $scope.model.units = getUnitsIds();
            typesApi.update($scope.model).then(
              function (result) {
                  console.log(JSON.stringify(result));
                  if (result.OK) {
                      $scope.model.checked = false;
                      $scope.model = {};
                      resetUnits();
                      LxDialogService.close($scope.dialogId);
                  }
                  else {
                  }
              });
        }
    }
    $scope.orderby = 'name';
    $scope.orderBy = function (field) {
        $scope.orderby = field;
    }
    /*
    $scope.newType = {
        units: [{ ID: 0, Name: 'Bottle', Checked: false },
        { ID: 1, Name: 'Bottle', Checked: false }, { ID: 2, Name: 'Bag', Checked: false },
        { ID: 3, Name: 'Each', Checked: false }, { ID: 4, Name: 'Litre', Checked: false },
        { ID: 5, Name: 'Meter', Checked: false }, { ID: 6, Name: 'Kgm', Checked: false },
        { ID: 7, Name: 'Ton', Checked: false }, { ID: 8, Name: 'Barrel', Checked: false }]
    }
    $scope.newDialogId = 'newDialog';
    $scope.editDialogId = 'editDialog';
    $scope.editType = {
        units: [{ ID: 0, Name: 'Bottle', Checked: false },
            { ID: 1, Name: 'Bottle', Checked: false }, { ID: 2, Name: 'Bag', Checked: false },
            { ID: 3, Name: 'Each', Checked: false }, { ID: 4, Name: 'Litre', Checked: false },
            { ID: 5, Name: 'Meter', Checked: false }, { ID: 6, Name: 'Kgm', Checked: false },
            { ID: 7, Name: 'Ton', Checked: false }, { ID: 8, Name: 'Barrel', Checked: false }]
    };

    $scope.types = [{ Code: 'RTMS-001', ID: 0, Name: 'Water', Checked: false },
        { Code: 'RTMS-001', ID: 1, Name: 'Water', Checked: false },
        { Code: 'RTMS-002', ID: 2, Name: 'A/C Bolt', Checked: false },
        { Code: 'RTMS-003', ID: 3, Name: 'Lorem', Checked: false },
        { Code: 'RTMS-004', ID: 4, Name: 'Lorem', Checked: false },
        { Code: 'RTMS-005', ID: 5, Name: 'Lorem', Checked: false },
        { Code: 'RTMS-006', ID: 6, Name: 'Lorem', Checked: false },
        { Code: 'RTMS-006', ID: 7, Name: 'Lorem', Checked: false },
        { Code: 'RTMS-001', ID: 8, Name: 'Lorem', Checked: false }
    ]
    */
   
    
}]);