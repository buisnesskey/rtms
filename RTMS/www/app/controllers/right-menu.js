﻿angular.module('mainApp').controller('rightMenuCtrl', ['$scope', '$state', 'LxDialogService', 'app.data.shared', 'app.api.account', function ($scope, $state, LxDialogService, sharedData, accountApi) {

    $scope.showDialog = function (id) {
        LxDialogService.open(id);
    }
    //$scope.user = sharedData.user;
    $scope.changePasswordModel = {};

    $scope.changePassword = function () {
        accountApi.changePassword($scope.changePasswordModel).then(
            function (result) {
                console.log(JSON.stringify(result));
                if (result.OK) {
                    $state.go('home');
                }
                else {
                    // $scope.loginFailure = true;
                }
            });
    }
    $scope.logout = function () {
        accountApi.logout();
        $state.go('login');
    }
    $scope.addOrder = function (e) {
        if (e) {
            e.preventDefault();
        }
        $state.go('orders', { openAddDialog: true });
    }

}]);