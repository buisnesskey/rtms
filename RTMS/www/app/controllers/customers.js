﻿angular.module('mainApp').controller('customersCtrl', ['$scope', 'LxDialogService', 'LxNotificationService', '$state', 'app.api.customer', 'app.api.account', 'app.data.shared', function ($scope, LxDialogService, LxNotificationService, $state, customerApi, accountApi, sharedData) {

    $scope.dialogId = 'formDialog';
    $scope.model = {};
    $scope.modelRoleTypes = [];
    $scope.modelSupervisorUsers = [];

    $scope.typeRoleChange = function (typeRoleId) {
        //debugger;
        $scope.modelRoleTypes = [];
        $scope.roles.forEach(function (role, index) {
            var selectedRoleType = $scope.searchArray('value', typeRoleId, $scope.roles);
            //if (selectedType && selectedType.units && selectedType.units.indexOf(role.value) > -1) {
            if (selectedRoleType && role.value > -1) {
                $scope.modelRoleTypes.push(role);
            }
        });
        //if ($scope.typeUnits.length == 0)
        //    $scope.typeUnits.push({ id: null, name: 'Other' });
        $scope.model.typeRoleId = $scope.modelRoleTypes[0].value;
    }

    $scope.typeSupervisorUserChange = function (typeSupervisorUserId) {
        //debugger;
        $scope.modelSupervisorUsers = [];
        $scope.supervisorUsers.forEach(function (supervisorUser, index) {
            var selectedSupervisorUser = $scope.searchArray('value', typeSupervisorUserId, $scope.supervisorUsers);
            //if (selectedType && selectedType.units && selectedType.units.indexOf(role.value) > -1) {
            if (selectedSupervisorUser && supervisorUser.value > -1) {
                $scope.modelSupervisorUsers.push(supervisorUser);
            }
        });
        //if ($scope.typeUnits.length == 0)
        //    $scope.typeUnits.push({ id: null, name: 'Other' });
        $scope.model.typeSupervisorUserId = $scope.modelSupervisorUsers[0].value;
    }

    $scope.toggleSelection = function (_item,event) {
        if (event)
            event.preventDefault;
        _item.checked = !_item.checked;
    }
    $scope.showDialog = function (id) {
        LxDialogService.open(id);
    }
    $scope.viewMode = false;
    $scope.view = function (item,event) {
        if (event)
            event.preventDefault();
        $scope.model = item;
        $scope.viewMode = true;
        LxDialogService.open($scope.dialogId);
    }
    $scope.add = function () {
        debugger;
        $scope.model = {};
        if ($scope.modelRoleTypes && $scope.modelRoleTypes.length > 0) {
            $scope.model.typeRoleId = $scope.modelRoleTypes[0].value;
            $scope.typeRoleChange($scope.model.typeRoleId);
        }
        if ($scope.modelSupervisorUsers && $scope.modelSupervisorUsers.length > 0) {
            $scope.model.typeSupervisorUserId = $scope.modelSupervisorUsers[0].value;
            $scope.typeSupervisorUserChange($scope.model.typeSupervisorUserId);
        }
        $scope.viewMode = false;
        LxDialogService.open($scope.dialogId);
    }
    $scope.edit = function () {
        var index = -1;
        for (var i = 0; i < $scope.customers.length; i++){
            if ($scope.customers[i].checked) {
                index = i;
                break;
            }
        }
        if (index > -1) {
            $scope.model = $scope.customers[i];
            $scope.viewMode = false;
            LxDialogService.open($scope.dialogId);
        }
        else {
            LxNotificationService.alert('Notice', 'Please select one entry to edit', 'Ok', function (answer) {
            });
        }
    }

    $scope.delete = function () {
        var index = -1;
        for (var i = 0; i < $scope.customers.length; i++) {
            if ($scope.customers[i].checked) {
                index = i;
                break;
            }
        }
        if (index > -1) {
            LxNotificationService.confirm('Confirm', 'Are you sure you want to delete this entry',
            {
                cancel: 'Cancel',
                ok: 'Yes'
            }, function (answer) {
                if (answer) {
                    customerApi.remove({ id: $scope.customers[index].id }).then(function (result) {
                        if (result.OK) {
                            $scope.customers.splice(index, 1);
                        }
                    });
                }
                else {
                }
            });
        }
        else {
            LxNotificationService.alert('Notice', 'Please select one entry to delete', 'Ok', function (answer) {
            });
        }
        
    }
    $scope.customers = [];
    customerApi.get().then(
            function (result) {
                console.log(JSON.stringify(result));
                if (result.OK) {
                    $scope.customers = result.response.data;
                }
                else {
                }   
            });

     $scope.save = function () {
         if (!$scope.model.id) {//add new
             customerApi.create($scope.model).then(
               function (result) {
                   console.log(JSON.stringify(result));
                   if (result.OK) {
                       var selectedRoleType = $scope.searchArray('value', $scope.model.typeRoleId, $scope.roles);
                       var roleNameString = selectedRoleType.text;

                       $scope.model.id = result.response.data;
                       $scope.customers.push({
                           id: $scope.model.id,
                           name: $scope.model.name,
                           email: $scope.model.email,
                           phone: $scope.model.phone,
                           contactPerson: $scope.model.contactPerson,
                           address: $scope.model.address,
                           roleName: selectedRoleType.text
                       });
                       $scope.model = {};
                       LxDialogService.close($scope.dialogId);
                   }
                   else {
                   }
               });
         }
         else {// update
             if (!$scope.model.updatePassword) {
                 $scope.model.password = null;
             }
             customerApi.update($scope.model).then(
               function (result) {
                   console.log(JSON.stringify(result));
                   if (result.OK) {
                       $scope.model.checked = false;
                       $scope.model = {};
                       LxDialogService.close($scope.dialogId);
                   }
                   else {
                   }
               });
         }
     }
     $scope.orderby = 'name';
     $scope.orderBy = function (field) {
         $scope.orderby = field;
     }

     $scope.roleObjectToModel = function (object, callback) {
         callback(object.value);
     };
     $scope.roleModelToObject = function (model, callback) {
         var index = -1;
         for (var i = 0; i < $scope.roles.length; i++) {
             if ($scope.roles[i].value == model) {
                 index = i;
                 break;
             }
         }
         callback($scope.roles[index]);
     };

     $scope.supervisorUserObjectToModel = function (object, callback) {
         callback(object.value);
     };
     $scope.supervisorUserModelToObject = function (model, callback) {
         var index = -1;
         for (var i = 0; i < $scope.supervisorUsers.length; i++) {
             if ($scope.supervisorUsers[i].value == model) {
                 index = i;
                 break;
             }
         }
         callback($scope.supervisorUsers[index]);
     };

     $scope.selectLoading = false;

     $scope.selectFilter = function (newFilter) {
         $scope.selectLoading = true;

         if (newFilter) {
             $scope.modelCustomers = [];
             for (var i = 0; i < $scope.customers.length; i++) {
                 if ($scope.customers[i].name.toLowerCase().indexOf(newFilter) > -1) {
                     $scope.modelCustomers.push($scope.customers[i]);
                 }
             }
         }
         else {
             $scope.modelCustomers = $scope.customers;
         }
         if ($scope.modelCustomers.length > 0) {
             $scope.model.typeSupervisorUserId = $scope.modelCustomers[0].typeSupervisorUserId;
             $scope.model.typeRoleId = $scope.modelCustomers[0].typeRoleId;
         }
         $scope.selectLoading = false;
     };


    /*
    $scope.customers = [
        {
            ID: 123, Name: 'John Smith', Email: 'johnsmith@gmail.com', Phone: '002 09378 2345', ContactPerson: 'Lorem ipsum dolor sit amet, consectetur',
            Address: '26, ipsum st, Lorem consectetur', Checked: false
        },
         {
             ID: 122, Name: 'John Smith', Email: 'johnsmith@gmail.com', Phone: '002 09378 2345', ContactPerson: 'Lorem ipsum dolor sit amet, consectetur',
             Address: '26, ipsum st, Lorem consectetur', Checked: false
         },
          {
              ID: 124, Name: 'John Smith', Email: 'johnsmith@gmail.com', Phone: '002 09378 2345', ContactPerson: 'Lorem ipsum dolor sit amet, consectetur',
              Address: '26, ipsum st, Lorem consectetur', Checked: false
          },
           {
               ID: 125, Name: 'John Smith', Email: 'johnsmith@gmail.com', Phone: '002 09378 2345', ContactPerson: 'Lorem ipsum dolor sit amet, consectetur',
               Address: '26, ipsum st, Lorem consectetur', Checked: false
           },
            {
                ID: 126, Name: 'John Smith', Email: 'johnsmith@gmail.com', Phone: '002 09378 2345', ContactPerson: 'Lorem ipsum dolor sit amet, consectetur',
                Address: '26, ipsum st, Lorem consectetur', Checked: false
            },
             {
                 ID: 127, Name: 'John Smith', Email: 'johnsmith@gmail.com', Phone: '002 09378 2345', ContactPerson: 'Lorem ipsum dolor sit amet, consectetur',
                 Address: '26, ipsum st, Lorem consectetur', Checked: false
             }
    ];
    */
     $scope.roles = [];
     accountApi.get().then(
            function (result) {
                if (result.OK) {
                    $scope.roles = result.response.data;
                    $scope.modelRoleTypes = result.response.data;
                    //$scope.roles.push({ id: null, name: '[Role]:Other', units: [0] });
                }
                else {
                }
            });

     $scope.supervisorUsers = [];
     customerApi.getSupervisorUsers().then(
            function (result) {
                if (result.OK) {
                    $scope.supervisorUsers = result.response.data;
                    $scope.modelSupervisorUsers = result.response.data;
                    //$scope.roles.push({ id: null, name: '[Role]:Other', units: [0] });
                }
                else {
                }
            });


     $scope.searchArray = function (key, value, array) {
         for (var i = 0; i < array.length; i++) {
             if (array[i][key] === value) {
                 return array[i];
             }
         }
         return null;
     }

}]);

