﻿angular.module('mainApp').controller('ordersCtrl', ['$http','$scope', '$state', 'LxDialogService', 'LxNotificationService', 'app.api.orders', 'app.data.shared', 'app.api.customer', function ($http,$scope, $state, LxDialogService, LxNotificationService, ordersApi, sharedData, customerApi) {

    $scope.dialogId = 'orderFormDialog';
    $scope.model = {};
    $scope.viewMode = false;

    $scope.states = [
        { id: 'waiting', name: 'Waiting' },
        { id: 'processing', name: 'Processing' },
        { id: 'complete', name: 'Complete' },
        { id: 'canceled', name: 'Canceled' },
        { id: 'alternative', name: 'Alternative' },
        { id: 'pending', name: 'Pending' }];
    $scope.toggleSelection = function (_item, event) {
        if (event)
            event.preventDefault;
        _item.checked = !_item.checked;
    }

    $scope.showDialog = function (id) {
        LxDialogService.open(id);
    }

    $scope.view = function (item, event) {
        if (event)
            event.preventDefault();
        $scope.model = item;
        $scope.viewMode = true;
        LxDialogService.open($scope.dialogId);
    }
    $scope.add = function () {
        $scope.model = {};
        $scope.viewMode = false;
        LxDialogService.open($scope.dialogId);
    }
    $scope.edit = function () {
        var index = -1;
        for (var i = 0; i < $scope.orders.length; i++) {
            if ($scope.orders[i].checked) {
                index = i;
                break;
            }
        }
        if (index > -1) {
            $scope.model = $scope.orders[i];
            if ($scope.model.state !== null && typeof $scope.model.state === 'object')
                $scope.model.state = $scope.model.state.finalStatus;
            else
                $scope.model.state = $scope.model.state;
            $scope.viewMode = false;
            LxDialogService.open($scope.dialogId);
        }
        else {
            LxNotificationService.alert('Notice', 'Please select one entry to edit', 'Ok', function (answer) {
            });
        }
    }

    $scope.delete = function () {
        var index = -1;
        for (var i = 0; i < $scope.orders.length; i++) {
            if ($scope.orders[i].checked) {
                index = i;
                break;
            }
        }
        if (index > -1) {
            LxNotificationService.confirm('Confirm', 'Are you sure you want to delete this entry',
            {
                cancel: 'Cancel',
                ok: 'Yes'
            }, function (answer) {
                if (answer) {
                    ordersApi.remove({ id: $scope.orders[index].id }).then(function (result) {
                        if (result.OK) {
                            $scope.orders.splice(index, 1);
                        }
                    });
                }
                else {
                }
            });
        }
        else {
            LxNotificationService.alert('Notice', 'Please select one entry to delete', 'Ok', function (answer) {
            });
        }

    }
    $scope.customers = [];
    customerApi.get().then(
           function (result) {
               if (result.OK) {
                   $scope.customers = result.response.data;
                   $scope.modelCustomers = result.response.data;
               }
               else {
               }
           });

    $scope.orders = [];
    ordersApi.get().then(
           function (result) {
               //console.log(JSON.stringify(result));
               if (result.OK) {
                   $scope.orders = result.response.data;
               }
               else {
               }
               if ($state.params.openAddDialog) {
                   $scope.add();
               }
           });



    $scope.save = function () {
        if (!$scope.model.id) {//add new
            ordersApi.create($scope.model).then(
              function (result) {
                  console.log(JSON.stringify(result));
                  if (result.OK) {
                      $scope.model.id = result.response.data.id;
                      $scope.model.createDate = result.response.data.createDate;
                      $scope.model.customerName = result.response.data.customerName;
                      $scope.model.customerId = result.response.data.customerId;
                      $scope.orders.push({
                          id: $scope.model.id,
                          serialNumber: $scope.model.serialNumber,
                          description: $scope.model.description,
                          createDate: $scope.model.createDate,
                          customerName: $scope.model.customerName,
                          customerId: $scope.model.customerId,
                          state: result.response.data.state
                      });
                      viewId = $scope.model.id;
                      $scope.model = {};
                      showViewAfterClose = true;
                      LxDialogService.close($scope.dialogId);
                  }
                  else {
                  }
              });
        }
        else {// update
            ordersApi.update($scope.model).then(
              function (result) {
                  console.log(JSON.stringify(result));
                  if (result.OK) {
                      $scope.model.checked = false;
                      $scope.model.customerName = result.response.data.customerName;
                      $scope.model.customerId = result.response.data.customerId;
                      $scope.model.state = result.response.data.state;
                      $scope.model.createDate = result.response.data.createDate;
                      viewId = $scope.model.id;
                      $scope.model = {};
                      showViewAfterClose = true;
                      LxDialogService.close($scope.dialogId);
                  }
                  else {
                  }
              });
        }
    }
    var showViewAfterClose = false;
    var viewId = null;
    $scope.closeDialog = function () {
        viewId = null;
        showViewAfterClose = false;
        LxDialogService.close($scope.dialogId);
    }
    $scope.$on('lx-dialog__close-end', function (_event, _dialogId) {
        if (showViewAfterClose && viewId != null) {
            showViewAfterClose = false;
            var index = -1;
            for (var i = 0; i < $scope.orders.length; i++) {
                if ($scope.orders[i].id == viewId) {
                    index = i;
                    break;
                }
            }
            viewId = null;
            if (index > -1)
                $scope.view($scope.orders[index]);
        }
        else if (viewItemsAfterClose && orderToView != null) {
            $state.go('items', { order: orderToView, openAddDialog: _openAddDialog });
        }
    });
    var viewItemsAfterClose = false;
    var orderToView = null;
    var _openAddDialog = false;
    $scope.viewItems = function (order) {
        $state.go('items', { order: order, openAddDialog: false });
    }
    $scope.closeDialogAndviewItems = function (order, openAddDialog) {
        viewItemsAfterClose = true;
        orderToView = order;
        _openAddDialog = openAddDialog;
        LxDialogService.close($scope.dialogId);
        //$state.go('items', { order: order, openAddDialog: openAddDialog });
    }

    $scope.objectToModel = function (object, callback) {
        if (object && object.id) {
            callback(object.id);
        }
        else {
            callback();
        }
    };
    $scope.modelToObject = function (model, callback) {
        //alert(model);
        var index = -1;
        for (var i = 0; i < $scope.customers.length; i++) {
            if ($scope.modelCustomers[i].id == model) {
                index = i;
                break;
            }
        }
        callback($scope.modelCustomers[index]);
    };

    $scope.stateObjectToModel = function (object, callback) {
        callback(object.id);
    };
    $scope.stateModelToObject = function (model, callback) {
        var index = -1;
        for (var i = 0; i < $scope.states.length; i++) {
            if ($scope.states[i].id == model) {
                index = i;
                break;
            }
        }
        callback($scope.states[index]);
    };
    $scope.selectLoading = false;

    $scope.selectFilter = function (newFilter) {
        $scope.selectLoading = true;

        if (newFilter) {
            $scope.modelCustomers = [];
            for (var i = 0; i < $scope.customers.length; i++) {
                if ($scope.customers[i].name.toLowerCase().indexOf(newFilter) > -1) {
                    $scope.modelCustomers.push($scope.customers[i]);
                }
            }
        }
        else {
            $scope.modelCustomers = $scope.customers;
        }
        if ($scope.modelCustomers.length > 0) {
            $scope.model.customerId = $scope.modelCustomers[0].id;
        }
        $scope.selectLoading = false;
    };

    //$scope.downloadReceipt = function (order) {
    //    ordersApi.receipt().then(function () {
    //    });
    //}
    $scope.orderby = 'state.finalStatusRank';
    $scope.orderBy = function (field) {
        $scope.orderby = field;
    }

    $scope.sendReceipt = function (id) {

        ordersApi.sendReceipt({ id: id }).then(function (result) {
            if (result.OK) {
                LxNotificationService.success('Sent successfully');
            } else {
                LxNotificationService.error('Failed to send');
            }
        });
    }
    $scope.sendInvoice = function (id) {
        ordersApi.sendInvoice({ id: id }).then(function (result) {
            if (result.OK) {
                LxNotificationService.success('Sent successfully');
            } else {
                LxNotificationService.error('Failed to send');
            }
        });
    }

    $scope.download = function (url,fileName) {
        var token = localStorage.getItem('token');
        $http.post(url, {}, {
            headers: { 'Authorization': 'Bearer ' + token }
        }).then(function (response) {
            download('data:application/excel;base64,' + response.data, fileName, 'application/excel');

        });
    }
    /*
    $scope.orders = [{ ID: 1, Code: 'trx-3420', Title: 'Lorem ipsum dolor', Description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', Status: 'Waiting', Date: '1/1/2016', Checked: false },
    { ID: 2, Code: 'trx-3420', Title: 'Lorem ipsum dolor', Description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', Status: 'Waiting', Date: '1/1/2016', Checked: false },
    { ID: 3, Code: 'trx-3420', Title: 'Lorem ipsum dolor', Description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', Status: 'Waiting', Date: '1/1/2016', Checked: false },
    { ID: 4, Code: 'trx-3420', Title: 'Lorem ipsum dolor', Description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', Status: 'Waiting', Date: '1/1/2016', Checked: false },
    { ID: 5, Code: 'trx-3420', Title: 'Lorem ipsum dolor', Description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', Status: 'Waiting', Date: '1/1/2016', Checked: false },
    { ID: 6, Code: 'trx-3420', Title: 'Lorem ipsum dolor', Description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', Status: 'Waiting', Date: '1/1/2016', Checked: false },
    { ID: 7, Code: 'trx-3420', Title: 'Lorem ipsum dolor', Description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', Status: 'Waiting', Date: '1/1/2016', Checked: false },
    { ID: 8, Code: 'trx-3420', Title: 'Lorem ipsum dolor', Description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', Status: 'Waiting', Date: '1/1/2016', Checked: false },
    { ID: 9, Code: 'trx-3420', Title: 'Lorem ipsum dolor', Description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', Status: 'Waiting', Date: '1/1/2016', Checked: false }]
    */
}]);