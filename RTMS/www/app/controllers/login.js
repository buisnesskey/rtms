﻿angular.module('mainApp').controller('loginCtrl', ['$rootScope','$scope','$state', 'app.api.account','app.data.shared', function ($rootScope,$scope,$state, accountApi,sharedData) {
    $scope.loginModel = {};
    $scope.loginFailure = false;

    $scope.login = function () {
        accountApi.authenticate($scope.loginModel).then(
            function (result) {
                console.log(JSON.stringify(result));
                if (result.OK) {
                    $rootScope.user = sharedData.user;
                    $state.go('orders');
                }
                else {
                    $scope.loginFailure = true;
                }
            });
    }
}]);