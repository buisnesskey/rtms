﻿angular.module('mainApp').controller('itemsCtrl', ['$filter', '$scope', '$state', 'LxDialogService', 'LxNotificationService', 'app.api.items', 'app.api.units', 'app.api.types', 'app.api.suppliers', 'app.data.shared', function ($filter, $scope, $state, LxDialogService, LxNotificationService, itemsApi, unitsApi, typesApi, suppliersApi, sharedData) {
    $scope.dialogId = 'itemsFormDialog';
    $scope.model = {};
    $scope.order = $state.params.order;
    $scope.typeUnits = [];
    $scope.modelTypes = [];
    $scope.modelcurrencies = [];

    $scope.currencyChange = function (currencyId) {
        //debugger;
        $scope.modelcurrencies = [];
        $scope.roles.forEach(function (currency, index) {
            var selectedRoleType = $scope.searchArray('value', currencyId, $scope.currencies);
            if (selectedRoleType && currency.value > -1) {
                $scope.modelcurrencies.push(currency);
            }
        });
        $scope.model.currencyId = $scope.modelcurrencies[0].value;
    }
    $scope.typeChange = function (typeId) {
        $scope.typeUnits = [];
        $scope.units.forEach(function (unit, index) {
            var selectedType = $scope.searchArray('id', typeId, $scope.modelTypes);
            if (selectedType && selectedType.units && selectedType.units.indexOf(unit.id) > -1) {
                $scope.typeUnits.push(unit);
            }
        });
        if ($scope.typeUnits.length == 0)
            $scope.typeUnits.push({ id: null, name: 'Other' });
        $scope.model.unitId = $scope.typeUnits[0].id;
    }
    $scope.toggleSelection = function (_item, event) {
        if (event)
            event.preventDefault;
        _item.checked = !_item.checked;
    }

    $scope.showDialog = function (id) {
        LxDialogService.open(id);
    }
    $scope.closeDialog = function () {
        LxDialogService.close($scope.dialogId);
    }
    $scope.viewMode = false;
    $scope.view = function (item, event) {
        if (event)
            event.preventDefault();
        $scope.model = item;
        $scope.viewMode = true;
        LxDialogService.open($scope.dialogId);
    }
    $scope.add = function () {
        $scope.model = {};
        if ($scope.modelTypes && $scope.modelTypes.length > 0) {
            $scope.model.typeId = $scope.modelTypes[0].id;
            $scope.typeChange($scope.model.typeId);
        }
        $scope.viewMode = false;
        LxDialogService.open($scope.dialogId);
    }

    $scope.edit = function () {
        var index = -1;
        for (var i = 0; i < $scope.items.length; i++) {
            if ($scope.items[i].checked) {
                index = i;
                break;
            }
        }
        if (index > -1) {
            $scope.model = $scope.items[i];
            $scope.viewMode = false;
            LxDialogService.open($scope.dialogId);
        }
        else {
            LxNotificationService.alert('Notice', 'Please select one entry to edit', 'Ok', function (answer) {
            });
        }
    }

    $scope.delete = function () {
        var index = -1;
        for (var i = 0; i < $scope.items.length; i++) {
            if ($scope.items[i].checked) {
                index = i;
                break;
            }
        }
        if (index > -1) {
            LxNotificationService.confirm('Confirm', 'Are you sure you want to delete this entry',
            {
                cancel: 'Cancel',
                ok: 'Yes'
            }, function (answer) {
                if (answer) {
                    itemsApi.remove({ id: $scope.items[index].id }).then(function (result) {
                        if (result.OK) {
                            $scope.items.splice(index, 1);
                        }
                    });
                }
                else {
                }
            });
        }
        else {
            LxNotificationService.alert('Notice', 'Please select one entry to delete', 'Ok', function (answer) {
            });
        }

    }
    $scope.exportReceipt = function () {
        debugger;
        var index = -1;
        var ids = new Array();
        for (var i = 0; i < $scope.items.length; i++) {
            if ($scope.items[i].checked) {
                index = i;
                ids.push($scope.items[i].id);
                //break;
            }
        }
        if (index > -1) {
            LxNotificationService.confirm('Confirm', 'Are you sure you want to export these entry',
            {
                cancel: 'Cancel',
                ok: 'Yes'
            }, function (answer) {
                if (answer) {
                    //itemsApi.remove({ id: $scope.items[index].id }).then(function (result) {
                    //    if (result.OK) {
                    //        $scope.items.splice(index, 1);
                    //    }
                    //});

                    //download('data:application/excel;base64,' + response.data, fileName, 'application/excel');
                    var currentOrderId = $scope.order == null ? 0 : $scope.order.id;
                    //console.log($scope.order.id);
                    itemsApi.exportReceipt({ id: currentOrderId, ids: ids }).then(function (result) {
                        if (result.OK) {
                            //window.location.href = appSettings.URI + '/api/items/downloadexcel?file=' + result.response.data.FileName;
                            window.location.href = result.response.data.FileName;
                            //result.fileName;
                            //$scope.items.splice(index, 1);
                        }
                    });
                }
                else {
                }
            });
        }
        else {
            LxNotificationService.alert('Notice', 'Please select one entry to delete', 'Ok', function (answer) {
            });
        }

    }

    $scope.deleteAttachment = function (model) {
        itemsApi.removeAttachment($scope.model.id).then(function (result) {
            if (result.OK) {
                model.attachment = null;
            }
            else {

            }
        });
    }
    $scope.save = function () {
        $scope.model.orderId = $scope.order.id;
        if ($scope.model.typeId == 0)
            $scope.model.typeId == null;
        if ($scope.model.unitId == 0)
            $scope.model.unitId == null;
        if ($scope.model.file && $scope.model.file.name) {
            var reader = new FileReader();
            reader.addEventListener("load", function () {
                $scope.model.attachment = {};
                $scope.model.attachment.content = reader.result.substring(reader.result.indexOf(',')+1);
                $scope.model.attachment.name = $scope.model.file.name;
                $scope.model.file = null;
                sendRequest();
            }, false);
            reader.readAsDataURL($scope.model.file);
        }
        else {
            $scope.model.attachment = null;
            sendRequest();
        }
        

    }
    function sendRequest() {

        if (!$scope.model.id) {//add new                
            itemsApi.create($scope.model).then(
              function (result) {
                  console.log(JSON.stringify(result));
                  if (result.OK) {
                      $scope.items.push({
                          id: result.response.data.id,
                          code: result.response.data.code,
                          customerName: result.response.data.customerName,
                          customerId: result.response.data.customerId,
                          typeId: result.response.data.typeId,
                          unitId: result.response.data.unitId,
                          typeText: result.response.data.typeText,
                          unitText: result.response.data.unitText,
                          description: result.response.data.description,
                          remarks: result.response.data.remarks,
                          state: result.response.data.state,
                          stateRank: parseInt(result.response.data.stateRank),
                          quantity: result.response.data.quantity,
                          deliveredQuantity: result.response.data.deliveredQuantity,
                          createDate: result.response.data.createDate,
                          completeDate: result.response.data.completeDate,
                          buyingPrice: result.response.data.buyingPrice,
                          sellingPrice: result.response.data.sellingPrice,
                          supplierName: result.response.data.supplierName,
                          supplierId: result.response.data.supplierId,
                          typeName: result.response.data.typeName,
                          attachment: result.response.data.attachment,
                          currencyName: result.response.data.currencyName
                      });
                      $scope.model = {};
                      LxDialogService.close($scope.dialogId);
                  }
                  else {
                  }
              });
        }
        else {// update
            itemsApi.update($scope.model).then(
              function (result) {
                  console.log(JSON.stringify(result));
                  if (result.OK) {
                      $scope.model.checked = false;
                      $scope.model.typeName = result.response.data.typeName;
                      $scope.model.stateRank = parseInt(result.response.data.stateRank);
                      $scope.model.attachment = result.response.data.attachment;
                      $scope.model = {};
                      LxDialogService.close($scope.dialogId);
                  }
                  else {
                  }
              });
        }
    }


    
    $scope.states = [{ id: 'waiting', name: 'Waiting' },
        { id: 'processing', name: 'Processing' },
        { id: 'complete', name: 'Complete' },
        { id: 'canceled', name: 'Canceled' },
        { id: 'alternative', name: 'Alternative' },
        { id: 'pending', name: 'Pending' }];
    suppliersApi.get().then(
     function (result) {
         if (result.OK) {
             $scope.suppliers = result.response.data;
             $filter('orderBy')($scope.suppliers, 'name');
             $scope.suppliers.push({ id: null, name: '[Supplier]:Other' });
         }
         else {
         }
     });
    $scope.units = [];
    $scope.types = [];

    $scope.currencies = [];
    itemsApi.getCurrencies().then(
           function (result) {
               if (result.OK) {
                   $scope.roles = result.response.data;
                   $scope.currencies = result.response.data;
                   //$scope.roles.push({ id: null, name: '[Role]:Other', units: [0] });
               }
               else {
               }
           });

    unitsApi.get().then(
      function (result) {
          if (result.OK) {
              $scope.units = result.response.data;
              $scope.units.push({ id: null, name: '[Unit]:Other' });
              $scope.typeUnits = $scope.units;
          }
          else {
          }
      });
    typesApi.get().then(
           function (result) {
               if (result.OK) {
                   $scope.types = result.response.data;
                   $scope.modelTypes = result.response.data;
                   $scope.types.push({ id: null, name: '[Type]:Other', units: [0] });
               }
               else {
               }
           });
    itemsApi.get(($scope.order && $scope.order.id) ? { orderId: $scope.order.id } : {}).then(
           function (result) {
               if (result.OK) {
                   $scope.items = result.response.data;
                   angular.forEach($scope.items, function (item) {
                       item.stateRank = parseInt(item.stateRank);
                   });
               }
               else {
               }
               if ($state.params.openAddDialog) {
                   $scope.add();
               }
           });

    var file = null;
    $scope.attachFile = function (_newFile) {
        /*console.log(_newFile);
        var reader = new FileReader();
        reader.addEventListener("load", function () {
            console.log(reader.result);
        }, false);

        if (_newFile) {
            reader.readAsDataURL(_newFile);
        }*/
    }
    $scope.resetFile = function (inputElem) {
        angular.element('#' + inputElem).val(null);
    }

    $scope.typeObjectToModel = function (object, callback) {
        if (object && object.id) {
            callback(object.id);
        }
        else {
            callback();
        }
    };
    $scope.unitObjectToModel = function (object, callback) {
        callback(object.id);
    };
    $scope.stateObjectToModel = function (object, callback) {
        callback(object.id);
    };
    $scope.supplierObjectToModel = function (object, callback) {
        callback(object.id);
    };
    $scope.typeModelToObject = function (model, callback) {
        //alert(model);
        var index = -1;
        for (var i = 0; i < $scope.types.length; i++) {
            if ($scope.modelTypes[i].id == model) {
                index = i;
                break;
            }
        }
        callback($scope.modelTypes[index]);
    };
    $scope.unitModelToObject = function (model, callback) {
        var index = -1;
        for (var i = 0; i < $scope.typeUnits.length; i++) {
            if ($scope.typeUnits[i].id == model) {
                index = i;
                break;
            }
        }
        callback($scope.typeUnits[index]);
    };

    $scope.supplierModelToObject = function (model, callback) {
        var index = -1;
        for (var i = 0; i < $scope.suppliers.length; i++) {
            if ($scope.suppliers[i].id == model) {
                index = i;
                break;
            }
        }
        callback($scope.suppliers[index]);
    };
    $scope.stateModelToObject = function (model, callback) {
        var index = -1;
        for (var i = 0; i < $scope.states.length; i++) {
            if ($scope.states[i].id == model) {
                index = i;
                break;
            }
        }
        callback($scope.states[index]);
    };

    $scope.currencyObjectToModel = function (object, callback) {
        callback(object.value);
    };
    $scope.currencyModelToObject = function (model, callback) {
        var index = -1;
        for (var i = 0; i < $scope.currencies.length; i++) {
            if ($scope.currencies[i].value == model) {
                index = i;
                break;
            }
        }
        callback($scope.currencies[index]);
    };

    $scope.searchArray = function (key, value, array) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][key] === value) {
                return array[i];
            }
        }
        return null;
    }

    $scope.orderby = 'stateRank';
    $scope.orderBy = function (field) {
        $scope.orderby = field;
    }

    $scope.selectLoading = false;

    $scope.selectFilter = function (newFilter) {
        $scope.selectLoading = true;

        if (newFilter) {
            $scope.modelTypes = [];
            $scope.typeUnits = [];
            for (var i = 0; i < $scope.types.length; i++) {
                if ($scope.types[i].name.toLowerCase().indexOf(newFilter) > -1) {
                    $scope.modelTypes.push($scope.types[i]);
                }
            }
        }
        else {
            $scope.modelTypes = $scope.types;
        }
        if ($scope.modelTypes.length > 0) {
            $scope.model.typeId = $scope.modelTypes[0].id;
            $scope.typeChange($scope.model.typeId);
        }
        $scope.selectLoading = false;
    };

    $scope.viewAttachment = function (item) {
        window.open(item.attachment, '_blank');
    }

    $scope.download = function (url, fileName) {
        var token = localStorage.getItem('token');
        $http.post(url, {}, {
            headers: { 'Authorization': 'Bearer ' + token }
        }).then(function (response) {
            download('data:application/excel;base64,' + response.data, fileName, 'application/excel');

        });
    }
}]);