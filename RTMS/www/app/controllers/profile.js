﻿angular.module('mainApp').controller('profileCtrl',['$scope', 'LxDialogService', function ($scope, LxDialogService) {
    $scope.profile = { name: 'Michael Lotfy', email: 'miclotfy@gmail.com', phone: '+2 012 2341 9876', address: '18 Shubra st, Cairo, Egypt.' };
    $scope.account = {};
    $scope.editProfileDlgId = 'editProfileDlg';
    $scope.changePasswordDlgId = 'changePasswordDlg';

    $scope.showDialog = function (dlgId) {
        LxDialogService.open(dlgId);
    }
}]);