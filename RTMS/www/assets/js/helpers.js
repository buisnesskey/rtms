﻿function trimLeft (input, charlist) {
    if (charlist === undefined)
        charlist = "\s";

    return input.replace(new RegExp("^[" + charlist + "]+"), "");
};
function trimRight(input, charlist) {
  if (charlist === undefined)
    charlist = "\s";

  return input.replace(new RegExp("[" + charlist + "]+$"), "");
};
function trim(input, charlist) {
    return input.trimLeft(charlist).trimRight(charlist);
};