﻿var mdl = angular.module('mdl', [])
.config(function () {
})
.run(function () {
});
mdl.directive('mdl', function() {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			componentHandler.upgradeElement(element[0]);
		}
	};
});
mdl.component('mdl-button', {
	templateUrl: 'components/mdl/views/button.html',
	controller: function () {
	},
	transclude :true,
	bindings: {name: '@'}
});