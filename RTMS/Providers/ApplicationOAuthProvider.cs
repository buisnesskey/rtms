// Decompiled with JetBrains decompiler
// Type: RTMS.Providers.ApplicationOAuthProvider
// Assembly: RTMS, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0A16F994-A518-4FEB-A56D-42E16C403459
// Assembly location: D:\rtms\bin\RTMS.dll

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using RTMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RTMS.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;

        public ApplicationOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
                throw new ArgumentNullException("publicClientId");
            this._publicClientId = publicClientId;
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            ApplicationUserManager userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
            ApplicationUser user = await userManager.FindAsync(context.UserName, context.Password);
            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
            }
            else
            {
                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync((UserManager<ApplicationUser>)userManager, "Bearer");
                ClaimsIdentity userIdentityAsync = await user.GenerateUserIdentityAsync((UserManager<ApplicationUser>)userManager, "Cookies");
                context.Validated(new AuthenticationTicket(oAuthIdentity, ApplicationOAuthProvider.CreateProperties(user.UserName, JsonConvert.SerializeObject((object)oAuthIdentity.Claims.Where<Claim>((Func<Claim, bool>)(c => c.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role")).ToList<Claim>().Select<Claim, string>((Func<Claim, string>)(x => x.Value))))));
                context.Request.Context.Authentication.SignIn(userIdentityAsync);
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> keyValuePair in (IEnumerable<KeyValuePair<string, string>>)context.Properties.Dictionary)
                context.AdditionalResponseParameters.Add(keyValuePair.Key, (object)keyValuePair.Value);
            return (Task)Task.FromResult<object>((object)null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            if (context.ClientId == null)
                context.Validated();
            return (Task)Task.FromResult<object>((object)null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == this._publicClientId && new Uri(context.Request.Uri, "/").AbsoluteUri == context.RedirectUri)
                context.Validated();
            return (Task)Task.FromResult<object>((object)null);
        }

        public static AuthenticationProperties CreateProperties(string userName, string roles = "[]")
        {
            return new AuthenticationProperties((IDictionary<string, string>)new Dictionary<string, string>()
      {
        {
          "userName",
          userName
        },
        {
          "roles",
          roles
        }
      });
        }
    }
}
