using System;
using System.ComponentModel.DataAnnotations;

namespace RTMS.Models
{
	public class OrderItem
	{
		public int Id
		{
			get;
			set;
		}

		public string Remarks
		{
			get;
			set;
		}

		public string Description
		{
			get;
			set;
		}

		public double Quantity
		{
			get;
			set;
		}

		public double DeliveredQuantity
		{
			get;
			set;
		}

		public string Attachment
		{
			get;
			set;
		}

		public string State
		{
			get;
			set;
		}

		public string TypeText
		{
			get;
			set;
		}

		public string UnitText
		{
			get;
			set;
		}

		public decimal BuyingPrice
		{
			get;
			set;
		}

		public decimal SellingPrice
		{
			get;
			set;
		}

		public DateTime CreateDate
		{
			get;
			set;
		}

		public DateTime? CompleteDate
		{
			get;
			set;
		}

		public int OrderId
		{
			get;
			set;
		}

		[Required]
		public virtual Order Order
		{
			get;
			set;
		}

		public int? SupplierId
		{
			get;
			set;
		}

		public virtual Supplier Supplier
		{
			get;
			set;
		}

		public int? ItemTypeId
		{
			get;
			set;
		}

		public virtual ItemType ItemType
		{
			get;
			set;
		}

		public int? UnitId
		{
			get;
			set;
		}

		public virtual TypeUnit Unit
		{
			get;
			set;
		}

        public byte CurrencyId
        {
            get;
            set;
        }
	}
}
