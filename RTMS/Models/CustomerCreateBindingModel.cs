using System;
using System.ComponentModel.DataAnnotations;

namespace RTMS.Models
{
	public class CustomerCreateBindingModel
	{
		[Display(Name = "Name"), Required]
		public string Name
		{
			get;
			set;
		}

		[Display(Name = "Email"), Required]
		public string Email
		{
			get;
			set;
		}

		[Display(Name = "Phone"), Required]
		public string Phone
		{
			get;
			set;
		}

		[Display(Name = "Contact Person")]
		public string ContactPerson
		{
			get;
			set;
		}

		[Display(Name = "Address")]
		public string Address
		{
			get;
			set;
		}

		[DataType(DataType.Password), Display(Name = "Password"), Required, StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 5)]
		public string Password
		{
			get;
			set;
		}

        [Display(Name = "Role")]
        public byte TypeRoleId
        {
            get;
            set;
        }

        [Display(Name = "Supervisor User")]
        public string TypeSupervisorUserId
        {
            get;
            set;
        }
	}
}
