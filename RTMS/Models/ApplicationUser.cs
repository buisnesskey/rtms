using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RTMS.Models
{
	public class ApplicationUser : IdentityUser
	{
		public string Name
		{
			get;
			set;
		}

		public string Phone
		{
			get;
			set;
		}

		public string ContactPerson
		{
			get;
			set;
		}

		public string Address
		{
			get;
			set;
		}

		public ICollection<Order> Orders
		{
			get;
			set;
		}



		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
		{
			return await manager.CreateIdentityAsync(this, authenticationType);
		}
	}
}
