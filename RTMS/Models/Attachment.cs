using System;
using System.ComponentModel.DataAnnotations;

namespace RTMS.Models
{
	public class Attachment
	{
		[Required]
		public string Content
		{
			get;
			set;
		}

		[Required]
		public string Name
		{
			get;
			set;
		}
	}
}
