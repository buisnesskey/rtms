using System;
using System.ComponentModel.DataAnnotations;

namespace RTMS.Models
{
	public class RemoveLoginBindingModel
	{
		[Display(Name = "Login provider"), Required]
		public string LoginProvider
		{
			get;
			set;
		}

		[Display(Name = "Provider key"), Required]
		public string ProviderKey
		{
			get;
			set;
		}
	}
}
