using System;

namespace RTMS.Models
{
	public class UserLoginInfoViewModel
	{
		public string LoginProvider
		{
			get;
			set;
		}

		public string ProviderKey
		{
			get;
			set;
		}
	}
}
