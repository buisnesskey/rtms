﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RTMS.Models
{
    public class SupervisorCustomer
    {
        public int Id
        {
            get;
            set;
        }

        public string CustomerUserId
        {
            get;
            set;
        }


        public virtual ApplicationUser CustomerUser
        {
            get;
            set;
        }

        public string SupervisorUserId
        {
            get;
            set;
        }

        public virtual ApplicationUser SupervisorUser
        {
            get;
            set;
        }
    }
}