using System;
using System.ComponentModel.DataAnnotations;

namespace RTMS.Models
{
	public class OrderBindingModel
	{
		[Required]
		public string SerialNumber
		{
			get;
			set;
		}

		public string Description
		{
			get;
			set;
		}

		public string CustomerId
		{
			get;
			set;
		}

        public DateTime? CreateDate
        {
            get;
            set;
        }
        public string State
        {
            get;
            set;
        }
	}
}
