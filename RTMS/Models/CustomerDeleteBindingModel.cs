using System;
using System.ComponentModel.DataAnnotations;

namespace RTMS.Models
{
	public class CustomerDeleteBindingModel
	{
		[Display(Name = "Id"), Required]
		public string Id
		{
			get;
			set;
		}
	}
}
