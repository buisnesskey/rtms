﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RTMS.Models.Reports
{
    public class ReportViewModel
    {
        public string item { set; get; }
        public double quantity { set; get; }
        public double deliverdQuantity { set; get; }
        public decimal priceBuy { set; get; }
        public decimal priceSell { set; get; }
        public DateTime creationDate { set; get; }
        public string itemDescription { set; get; }
        public string serial { set; get; }
        public byte currencyId { set; get; }
    }
}