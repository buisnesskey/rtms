﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RTMS.Models.Reports
{
    public class ReportGroupByViewModel
    {
        public string item {set;get;}
        public string itemDescription { set; get; }
        public double quantity { set; get; }
        public double deliverdQuantity { set; get; }
        public decimal priceBuy {set;get;}
        public decimal priceSell {set;get;}
        public decimal priceBuyTotal {set;get;}
        public decimal priceSellTotal { set; get; }
        public decimal priceDeliverdSellTotal { set; get; }
        public string creationDate { set; get; }
        public string serial { set; get; }
        public string currencyName { set; get; }
    }
}