﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RTMS.Models
{
    public class SelectListItemViewModel
    {
        public byte value { set; get; }
        public string text { set; get; }
    }
}