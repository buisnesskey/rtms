using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RTMS.Models
{
	public class Order
	{
		public int Id
		{
			get;
			set;
		}

		public string SerialNumber
		{
			get;
			set;
		}

		public string Description
		{
			get;
			set;
		}

		public DateTime CreateDate
		{
			get;
			set;
		}

		public virtual ICollection<OrderItem> Items
		{
			get;
			set;
		}

		public string UserId
		{
			get;
			set;
		}

		[Required]
		public virtual ApplicationUser User
		{
			get;
			set;
		}
	}
}
