﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RTMS.Models
{
    public class CustomerViewModel
    {
        public string Id {set;get;}
        public string Name {set;get;}
        public string Email {set;get;}
        public string Phone {set;get;}
        public string ContactPerson {set;get;}
        public string Address {set;get;}
        public string roleName {set;get;}
        public byte typeRoleId {set;get;}
        public string typeSupervisorUserId { set; get; }
    }
}