using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RTMS.Models
{
	public class ItemType
	{
		public int Id
		{
			get;
			set;
		}

		public string Code
		{
			get;
			set;
		}

		[Required]
		public string Name
		{
			get;
			set;
		}

		public bool IsActive
		{
			get;
			set;
		}

		public virtual ICollection<TypeUnit> TypeUnits
		{
			get;
			set;
		}
	}
}
