using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RTMS.Models
{
	public class ItemTypeBindingModel
	{
		[Display(Name = "Name"), Required]
		public string Name
		{
			get;
			set;
		}

		[Display(Name = "Code"), Required]
		public string Code
		{
			get;
			set;
		}

		[Display(Name = "Active"), Required]
		public bool IsActive
		{
			get;
			set;
		}

		[Display(Name = "Units")]
		public List<int> Units
		{
			get;
			set;
		}
	}
}
