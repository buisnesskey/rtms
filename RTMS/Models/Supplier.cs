using System;
using System.ComponentModel.DataAnnotations;

namespace RTMS.Models
{
	public class Supplier
	{
		public int Id
		{
			get;
			set;
		}

		[Required]
		public string Name
		{
			get;
			set;
		}

		public string ContactPerson
		{
			get;
			set;
		}

		public string Phone
		{
			get;
			set;
		}

		public string Address
		{
			get;
			set;
		}

		public bool IsActive
		{
			get;
			set;
		}
	}
}
