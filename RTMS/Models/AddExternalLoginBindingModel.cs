using System;
using System.ComponentModel.DataAnnotations;

namespace RTMS.Models
{
	public class AddExternalLoginBindingModel
	{
		[Display(Name = "External access token"), Required]
		public string ExternalAccessToken
		{
			get;
			set;
		}
	}
}
