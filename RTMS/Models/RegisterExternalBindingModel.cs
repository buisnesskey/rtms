using System;
using System.ComponentModel.DataAnnotations;

namespace RTMS.Models
{
	public class RegisterExternalBindingModel
	{
		[Display(Name = "Email"), Required]
		public string Email
		{
			get;
			set;
		}
	}
}
