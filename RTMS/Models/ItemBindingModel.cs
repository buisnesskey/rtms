using System;
using System.ComponentModel.DataAnnotations;

namespace RTMS.Models
{
	public class ItemBindingModel
	{
		[Required]
		public int OrderId
		{
			get;
			set;
		}

		public string Description
		{
			get;
			set;
		}

		public string Remarks
		{
			get;
			set;
		}

		public double Quantity
		{
			get;
			set;
		}

		public double DeliveredQuantity
		{
			get;
			set;
		}

		public Attachment Attachment
		{
			get;
			set;
		}

		public string State
		{
			get;
			set;
		}

		public string TypeText
		{
			get;
			set;
		}

		public string UnitText
		{
			get;
			set;
		}

		public decimal BuyingPrice
		{
			get;
			set;
		}

		public decimal SellingPrice
		{
			get;
			set;
		}

		public int? TypeId
		{
			get;
			set;
		}

		public int? UnitId
		{
			get;
			set;
		}

		public int? SupplierId
		{
			get;
			set;
		}
        public byte CurrencyId
        {
            get;
            set;
        }
	}
}
