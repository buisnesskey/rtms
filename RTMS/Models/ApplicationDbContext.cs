using Microsoft.AspNet.Identity.EntityFramework;
using RTMS.Migrations;
using System;
using System.Data.Entity;

namespace RTMS.Models
{
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
	{
		public DbSet<TypeUnit> TypeUnits
		{
			get;
			set;
		}

		public DbSet<ItemType> ItemTypes
		{
			get;
			set;
		}

		public DbSet<Order> Orders
		{
			get;
			set;
		}

		public DbSet<OrderItem> OrderItems
		{
			get;
			set;
		}

		public DbSet<Supplier> Suppliers
		{
			get;
			set;
		}

        public DbSet<SupervisorCustomer> SupervisorCustomers
        {
            get;
            set;
        }

		public ApplicationDbContext() : base("DefaultConnection", false)
		{
			Database.SetInitializer<ApplicationDbContext>(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
		}

		public static ApplicationDbContext Create()
		{
			return new ApplicationDbContext();
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}
	}
}
