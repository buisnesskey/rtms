using RTMS.Common;
using RTMS.Core;
using RTMS.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace RTMS.Controllers
{
    [Authorize, EnableCors("*", "*", "GET, POST, PUT, DELETE, OPTIONS", SupportsCredentials = true), RoutePrefix("api/Units")]
    public class UnitsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [HttpGet]
        public IHttpActionResult Get()
        {
            if (base.User.IsInRole(SystemRoles.Admin) || base.User.IsInRole(SystemRoles.Super))
            {
                return this.Ok(from u in this.db.TypeUnits
                               select new
                               {
                                   u.Id,
                                   u.Name,
                                   u.Code,
                                   u.IsActive
                               });
            }
            return this.Ok(from u in this.db.TypeUnits
                           where u.IsActive == true
                           select new
                           {
                               u.Id,
                               u.Name,
                               u.Code,
                               u.IsActive
                           });
        }

        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Post(int id, TypeUnit typeUnit)
        {
            IHttpActionResult result;
            try
            {
                if (!this.ModelState.IsValid)
                {
                    result = this.BadRequest(this.ModelState);
                }
                else if (id != typeUnit.Id)
                {
                    result = this.BadRequest();
                }
                else
                {
                    this.db.Entry<TypeUnit>(typeUnit).State = EntityState.Modified;
                    try
                    {
                        await this.db.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!this.TypeUnitExists(id))
                        {
                            result = this.NotFound();
                            return result;
                        }
                        throw;
                    }
                    result = this.StatusCode(HttpStatusCode.NoContent);
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return result;
        }

        public async Task<IHttpActionResult> Post(TypeUnit typeUnit)
        {
            IHttpActionResult result;
            try
            {
                if (!this.ModelState.IsValid)
                {
                    result = this.BadRequest(this.ModelState);
                }
                else
                {
                    this.db.TypeUnits.Add(typeUnit);
                    await this.db.SaveChangesAsync();
                    string location = this.Url.Route("DefaultApi", new
                    {
                        controller = "Units",
                        id = typeUnit.Id
                    });
                    result = this.Created<TypeUnit>(location, typeUnit);
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return result;
        }

        public async Task<IHttpActionResult> Delete(int id)
        {
            IHttpActionResult result;
            try
            {
                TypeUnit typeUnit = await this.db.TypeUnits.FindAsync(new object[]
			{
				id
			});
                if (typeUnit == null)
                {
                    result = this.NotFound();
                }
                else
                {
                    List<ItemType>.Enumerator var_4 = typeUnit.ItemTypes.ToList<ItemType>().GetEnumerator();
                    try
                    {
                        while (var_4.MoveNext())
                        {
                            ItemType var_5_C7 = var_4.Current;
                            typeUnit.ItemTypes.Remove(var_5_C7);
                        }
                    }
                    finally
                    {
                        int num = 0;
                        if (num < 0)
                        {
                            var_4.Dispose();
                        }
                    }
                    this.db.TypeUnits.Remove(typeUnit);
                    await this.db.SaveChangesAsync();
                    result = this.Ok(new
                    {
                        Id = id
                    });
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TypeUnitExists(int id)
        {
            return this.db.TypeUnits.Count((TypeUnit e) => e.Id == id) > 0;
        }
    }
}
