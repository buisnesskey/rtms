using RTMS.Common;
using RTMS.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace RTMS.Controllers
{
    [Authorize, EnableCors("*", "*", "GET, POST, PUT, DELETE, OPTIONS", SupportsCredentials = true)]
    public class SuppliersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public IQueryable Get()
        {
            return this.db.Suppliers;
        }

        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Post(int id, Supplier supplier)
        {
            IHttpActionResult result;
            try
            {
                if (!this.ModelState.IsValid)
                {
                    result = this.BadRequest(this.ModelState);
                }
                else if (id != supplier.Id)
                {
                    result = this.BadRequest();
                }
                else
                {
                    this.db.Entry<Supplier>(supplier).State = EntityState.Modified;
                    try
                    {
                        await this.db.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!this.SupplierExists(id))
                        {
                            result = this.NotFound();
                            return result;
                        }
                        throw;
                    }
                    result = this.StatusCode(HttpStatusCode.NoContent);
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return result;
        }

        public async Task<IHttpActionResult> Post(Supplier supplier)
        {
            IHttpActionResult result;
            try
            {
                if (!this.ModelState.IsValid)
                {
                    result = this.BadRequest(this.ModelState);
                }
                else
                {
                    this.db.Suppliers.Add(supplier);
                    await this.db.SaveChangesAsync();
                    result = this.CreatedAtRoute<Supplier>("DefaultApi", new
                    {
                        id = supplier.Id
                    }, supplier);
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return result;
        }

        public async Task<IHttpActionResult> Delete(int id)
        {
            Supplier supplier = await this.db.Suppliers.FindAsync(new object[]
			{
				id
			});
            Supplier supplier2 = supplier;
            IHttpActionResult result;
            try
            {
                if (supplier2 == null)
                {
                    result = this.NotFound();
                }
                else
                {
                    this.db.Suppliers.Remove(supplier2);
                    await this.db.SaveChangesAsync();
                    result = this.Ok<Supplier>(supplier2);
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SupplierExists(int id)
        {
            return this.db.Suppliers.Count((Supplier e) => e.Id == id) > 0;
        }
    }
}
