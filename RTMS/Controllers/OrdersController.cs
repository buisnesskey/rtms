using log4net;
using Microsoft.AspNet.Identity;
using RTMS.Common;
using RTMS.Core;
using RTMS.Models;
using RTMS.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace RTMS.Controllers
{
    [Authorize, EnableCors("*", "*", "GET, POST, PUT, DELETE, OPTIONS", SupportsCredentials = true)]
    public class OrdersController : ApiController
    {
        private static log4net.ILog Log { get; set; }
        ILog log = log4net.LogManager.GetLogger(typeof(OrdersController));
        private ApplicationDbContext db = new ApplicationDbContext();

        private Status GetState(int orderId)
        {
            
            Status status = new Status();
            try
            {
                Order order = this.db.Orders.Find(new object[]
				{
					orderId
				});

                string userId = order.UserId;
                var appUser = this.db.SupervisorCustomers.Where(x => x.CustomerUserId.Equals(userId)).FirstOrDefault();
                status.AllCount = this.db.OrderItems.Count((OrderItem item) => item.OrderId == orderId);
                status.WaitingCount = this.db.OrderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Waiting);
                status.ProcessingCount = this.db.OrderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Processing);
                status.CompleteCount = this.db.OrderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Complete);
                status.AlternativeCount = this.db.OrderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Alternative);
                status.CanceledCount = this.db.OrderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Canceled);
                status.PendingCount = this.db.OrderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Pending);
                if (status.ProcessingCount > 0 || (status.WaitingCount > 0 && status.CompleteCount > 0))
                {
                    status.FinalStatus = State.Processing;
                }
                else if (status.WaitingCount == 0 && status.ProcessingCount == 0 && (status.CompleteCount > 0 || status.CanceledCount > 0 || status.AlternativeCount > 0))
                {
                    status.FinalStatus = State.Complete;
                }
                else if (status.AllCount > 0 && status.AllCount == status.CanceledCount)
                {
                    status.FinalStatus = State.Canceled;
                }
                else if (status.AllCount > 0 && status.AllCount == status.AlternativeCount)
                {
                    status.FinalStatus = State.Complete;
                }
                else if (status.PendingCount > 0 || (status.AllCount == 0 && appUser != null))
                {
                    status.FinalStatus = State.Pending;
                }
                else
                {
                    status.FinalStatus = State.Waiting;
                }
                status.FinalStatusRank = ((status.FinalStatus == State.Waiting) ? 10 : ((status.FinalStatus == State.Processing) ? 20 : ((status.FinalStatus == State.Complete) ? 30 : 40)));

            }
            catch (Exception exception)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(exception, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            } 
            return status;
        }
        private Status GetState2(Order order, List<OrderItem> db_orderItems, List<SupervisorCustomer> db_SupervisorCustomers)
        {

            Status status = new Status();
            try
            {
                //Order order = this.db.Orders.Find(new object[]
                //{
                //	orderId
                //});

                string userId = order.UserId;
                var orderId = order.Id;
                var appUser = db_SupervisorCustomers.Where(x => x.CustomerUserId.Equals(userId)).FirstOrDefault();
                status.AllCount = db_orderItems.Count((OrderItem item) => item.OrderId == orderId);
                status.WaitingCount = db_orderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Waiting);
                status.ProcessingCount = db_orderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Processing);
                status.CompleteCount = db_orderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Complete);
                status.AlternativeCount = db_orderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Alternative);
                status.CanceledCount = db_orderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Canceled);
                status.PendingCount = db_orderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Pending);
                if (status.ProcessingCount > 0 || (status.WaitingCount > 0 && status.CompleteCount > 0))
                {
                    status.FinalStatus = State.Processing;
                }
                else if (status.WaitingCount == 0 && status.ProcessingCount == 0 && (status.CompleteCount > 0 || status.CanceledCount > 0 || status.AlternativeCount > 0))
                {
                    status.FinalStatus = State.Complete;
                }
                else if (status.AllCount > 0 && status.AllCount == status.CanceledCount)
                {
                    status.FinalStatus = State.Canceled;
                }
                else if (status.AllCount > 0 && status.AllCount == status.AlternativeCount)
                {
                    status.FinalStatus = State.Complete;
                }
                else if (status.PendingCount > 0 || (status.AllCount == 0 && appUser != null))
                {
                    status.FinalStatus = State.Pending;
                }
                else
                {
                    status.FinalStatus = State.Waiting;
                }
                status.FinalStatusRank = ((status.FinalStatus == State.Waiting) ? 10 : ((status.FinalStatus == State.Processing) ? 20 : ((status.FinalStatus == State.Complete) ? 30 : 40)));

            }
            catch (Exception exception)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(exception, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return status;
        }
        [HttpGet]
        public IEnumerable Get(string customerId = "", string state = "")
        {
            var OrderItemsList = db.OrderItems.ToList();
            var SupervisorCustomersList = this.db.SupervisorCustomers.ToList();
            string currentUserId = base.User.Identity.GetUserId();
            if (base.User.IsInRole(SystemRoles.Customer))
            {
                return from order in
                           (from order in this.db.Orders
                            where order.UserId == currentUserId
                            select order).ToList<Order>()
                       select new
                       {
                           Id = order.Id,
                           SerialNumber = order.SerialNumber,
                           Description = order.Description,
                           CreateDate = DateManager.GetLocalDate(new DateTime?(order.CreateDate)),
                           CustomerName = order.User.Name,
                           CustomerId = order.UserId,
                           State = this.GetState2(order, OrderItemsList, SupervisorCustomersList)
                       } into order
                       where string.IsNullOrEmpty(state) || order.State.FinalStatus == state
                       select order;
            }
            if (base.User.IsInRole(SystemRoles.Supervisor))
            {
                string userId = this.User.Identity.GetUserId();
                var appUsers = this.db.SupervisorCustomers.Where(x => x.SupervisorUserId.Equals(userId)).Select(x => x.CustomerUserId);
                return from order in
                           (from order in this.db.Orders
                            where order.UserId == currentUserId || appUsers.Contains(order.UserId)
                            select order).ToList<Order>()
                       select new
                       {
                           Id = order.Id,
                           SerialNumber = order.SerialNumber,
                           Description = order.Description,
                           CreateDate = DateManager.GetLocalDate(new DateTime?(order.CreateDate)),
                           CustomerName = order.User.Name,
                           CustomerId = order.UserId,
                           State = this.GetState2(order, OrderItemsList, SupervisorCustomersList)
                       } into order
                       where string.IsNullOrEmpty(state) || order.State.FinalStatus == state
                       select order;
            }
            return from order in
                       (from order in this.db.Orders
                        where string.IsNullOrEmpty(customerId) || order.UserId == customerId
                        select order).ToList<Order>()
                   select new
                   {
                       Id = order.Id,
                       SerialNumber = order.SerialNumber,
                       Description = order.Description,
                       CreateDate = DateManager.GetLocalDate(new DateTime?(order.CreateDate)),
                       CustomerName = order.User.Name,
                       CustomerId = order.UserId,
                       State = this.GetState2(order, OrderItemsList, SupervisorCustomersList)
                   } into order
                   where (string.IsNullOrEmpty(state) || order.State.FinalStatus == state)
                   && (state == State.Pending || order.State.FinalStatus != State.Pending)
                   select order;
        }

        public async Task<IHttpActionResult> Post(int id, OrderBindingModel model)
        {
            IHttpActionResult result = this.BadRequest();
            try
            {
                if (!this.ModelState.IsValid)
                {
                    result = this.BadRequest(this.ModelState);
                }
                else
                {
                    Order order = this.db.Orders.Find(new object[]
				{
					id
				});
                    if (order == null)
                    {
                        result = this.NotFound();
                    }
                    else
                    {
                        if (this.User.IsInRole(SystemRoles.Customer))
                        {
                            if (this.GetState(id).FinalStatus != State.Waiting)
                            {
                                result = this.BadRequest("Order processing has started phase, call administrator for modification");
                                return result;
                            }
                            order.User = this.db.Users.Find(new object[]
						{
							this.User.Identity.GetUserId()
						});
                            order.UserId = order.User.Id;
                        }
                        else
                        {
                            ApplicationUser applicationUser = this.db.Users.Find(new object[]
						{
							model.CustomerId
						});
                            if (applicationUser == null)
                            {
                                applicationUser = this.db.Users.Find(new object[]
							{
								this.User.Identity.GetUserId()
							});
                            }
                            order.User = applicationUser;
                            order.UserId = applicationUser.Id;
                        }
                        order.SerialNumber = model.SerialNumber;
                        order.Description = model.Description;
                        order.CreateDate = model.CreateDate.HasValue ? model.CreateDate.Value : DateTime.UtcNow;
                        this.db.Entry<Order>(order).State = EntityState.Modified;
                        if (State.Pending == this.GetState(order.Id).FinalStatus.ToString())
                        {
                            if (model.State == State.Waiting)
                            {
                                order.Items.ToList().ForEach(x =>
                                {
                                    x.State = State.Waiting;
                                });
                            }
                        }
                        try
                        {
                            await this.db.SaveChangesAsync();
                        }
                        catch (Exception ex)
                        {
                            var method = MethodBase.GetCurrentMethod();
                            ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                            result = this.InternalServerError();
                            return result;
                        }
                        result = this.CreatedAtRoute("DefaultApi", new
                        {
                            id = order.Id
                        }, new
                        {
                            Id = order.Id,
                            SerialNumber = order.SerialNumber,
                            Description = order.Description,
                            CreateDate = DateManager.GetLocalDate(new DateTime?(order.CreateDate)),
                            CustomerName = order.User.Name,
                            CustomerId = order.UserId,
                            State = this.GetState(order.Id)
                        });
                    }
                }
            }
            catch (Exception exception)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(exception, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return result;
        }

        public async Task<IHttpActionResult> Post(OrderBindingModel model)
        {
            IHttpActionResult result = this.BadRequest();
            try
            {
                if (!this.ModelState.IsValid)
                {
                    result = this.BadRequest(this.ModelState);
                }
                else
                {
                    Order order = this.db.Orders.Create();
                    if (this.User.IsInRole(SystemRoles.Customer))
                    {
                        order.User = this.db.Users.Find(new object[]
					    {
						    this.User.Identity.GetUserId()
					    });

                        //string userId = this.User.Identity.GetUserId();
                        var appUser = this.db.SupervisorCustomers.Where(x => x.CustomerUserId.Equals(order.User.Id)).FirstOrDefault();
                        var appSupervisorUser = this.db.Users.Find(new object[]
					    {
						    appUser.SupervisorUserId
					    });
                        if (appUser != null)
                        {
                            //orderItem.State = State.Pending;
                            MailHelper.SendMail(appSupervisorUser.Email, "RTMS Invoice", "Dear Supervisor, Action needed, please check pending order at procurement.rtms.com.eg for order#" + order.SerialNumber);
                        }

                        order.UserId = order.User.Id;
                    }
                    else
                    {
                        ApplicationUser applicationUser = this.db.Users.Find(new object[]
					    {
						    model.CustomerId
					    });
                        if (applicationUser == null)
                        {
                            applicationUser = this.db.Users.Find(new object[]
						    {
							    this.User.Identity.GetUserId()
						    });
                        }
                        order.User = applicationUser;
                        order.UserId = applicationUser.Id;
                    }
                    order.SerialNumber = model.SerialNumber;
                    order.Description = model.Description;
                    order.CreateDate = model.CreateDate.HasValue ? model.CreateDate.Value : DateTime.UtcNow;
                    this.db.Orders.Add(order);
                    try
                    {
                        await this.db.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        var method = MethodBase.GetCurrentMethod();
                        ExceptionHandler.WriteLog(ex.GetBaseException(), string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                        ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                        result = this.InternalServerError();
                        return result;
                    }
                    result = this.CreatedAtRoute("DefaultApi", new
                    {
                        id = order.Id
                    }, new
                    {
                        Id = order.Id,
                        SerialNumber = order.SerialNumber,
                        Description = order.Description,
                        CreateDate = DateManager.GetLocalDate(new DateTime?(order.CreateDate)),
                        CustomerName = order.User.Name,
                        CustomerId = order.UserId,
                        State = this.GetState(order.Id)
                    });
                }
            }
            catch (Exception exception)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(exception, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return result;
        }

        public async Task<IHttpActionResult> Delete(int id)
        {
            log.Info("----------------1------------------");
            Order order = await this.db.Orders.FindAsync(new object[]
			{
				id
			});
            IHttpActionResult result;
            try
            {
                log.Info("----------------2------------------");

                if (order == null)
                {
                    result = this.NotFound();
                }
                else
                {
                    log.Info("----------------3------------------");

                    if (this.User.IsInRole(SystemRoles.Customer))
                    {
                        if (this.GetState(id).FinalStatus != State.Waiting)
                        {
                            result = this.BadRequest("Order processing has started phase, call administrator for modification");
                            return result;
                        }
                        if (order.UserId != this.User.Identity.GetUserId())
                        {
                            result = this.NotFound();
                            return result;
                        }
                    }
                    log.Info("----------------4------------------");

                    this.db.Orders.Remove(order);
                    log.Info("----------------5------------------");

                    await this.db.SaveChangesAsync();
                    log.Info("----------------6------------------");

                    result = this.Ok(new
                    {
                        Id = id
                    });
                }
            }
            catch (Exception exception)
            {
                log.Info("----------------7------------------");

                Log.Error(exception);
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(exception, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            log.Info("----------------8------------------");

            return result;
        }

        [ResponseType(typeof(void)), Route("api/orders/sendreceipt/{id}")]
        public IHttpActionResult SendReceipt(int id)
        {
            Order order = this.db.Orders.Find(new object[]
			{
				id
			});
            if (order == null)
            {
                return this.NotFound();
            }
            if (base.User.IsInRole(SystemRoles.Customer))
            {
                return this.BadRequest("Only Administrators Allowed");
            }
            if (this.GetState(id).FinalStatus != State.Complete)
            {
                return this.BadRequest("Order not complete yet");
            }
            ApplicationUser applicationUser = this.db.Users.Find(new object[]
			{
				order.UserId
			});
            string text = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss-ffffff") + ".xlsx";
            text = HttpContext.Current.Server.MapPath("~/temp/" + text);
            Excel.SaveReceipt(order, applicationUser, text);
            try
            {
                //MailHelper.SendMail("amr.alsayd.w88@gmail.com", "RTMS Invoice", "Dear Customer, attached is the deliver receipt for order#" + order.SerialNumber, text);
                MailHelper.SendMail(applicationUser.Email, "RTMS Invoice", "Dear Customer, attached is the deliver receipt for order#" + order.SerialNumber, text);
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                log.Error(ex);
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                return this.StatusCode(HttpStatusCode.BadRequest);
            }
            return this.StatusCode(HttpStatusCode.NoContent);
        }

        [ResponseType(typeof(void)), Route("api/orders/sendinvoice/{id}")]
        public IHttpActionResult SendInvoice(int id)
        {
            Order order = this.db.Orders.Find(new object[]
			{
				id
			});
            if (order == null)
            {
                return this.NotFound();
            }
            if (base.User.IsInRole(SystemRoles.Customer))
            {
                return this.BadRequest("Only Administrators Allowed");
            }
            if (this.GetState(id).FinalStatus != State.Complete)
            {
                return this.BadRequest("Order not complete yet");
            }
            ApplicationUser applicationUser = this.db.Users.Find(new object[]
			{
				order.UserId
			});
            string text = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss-ffffff") + ".xlsx";
            text = HttpContext.Current.Server.MapPath("~/temp/" + text);
            Excel.SaveInvoice(order, applicationUser, text);
            try
            {
                MailHelper.SendMail(applicationUser.Email, "RTMS Invoice", "Dear Customer, attached is the invoice for order#" + order.SerialNumber, text);
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                return this.StatusCode(HttpStatusCode.BadRequest);
            }
            File.Delete(text);
            return this.StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(int id)
        {
            return this.db.Orders.Count((Order e) => e.Id == id) > 0;
        }
    }
}
