// Decompiled with JetBrains decompiler
// Type: RTMS.Controllers.TypesController
// Assembly: RTMS, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0A16F994-A518-4FEB-A56D-42E16C403459
// Assembly location: D:\rtms\bin\RTMS.dll

using RTMS.Common;
using RTMS.Core;
using RTMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace RTMS.Controllers
{
    [Authorize]
    [RoutePrefix("api/types")]
    [EnableCors("*", "*", "GET, POST, PUT, DELETE, OPTIONS", SupportsCredentials = true)]
    public class TypesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public IQueryable Get()
        {
            if (base.User.IsInRole(SystemRoles.Admin) || base.User.IsInRole(SystemRoles.Super))
            {
                return from t in this.db.ItemTypes
                       select new
                       {
                           Id = t.Id,
                           Name = t.Name,
                           Code = t.Code,
                           IsActive = t.IsActive,
                           Units = (from u in t.TypeUnits
                                    select u.Id).ToList<int>()
                       };
            }
            return from i in this.db.ItemTypes
                   where i.IsActive == true
                   select i into t
                   select new
                   {
                       Id = t.Id,
                       Name = t.Name,
                       Code = t.Code,
                       IsActive = t.IsActive,
                       Units = (from u in t.TypeUnits
                                select u.Id).ToList<int>()
                   };
        }

        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Post(int id, ItemTypeBindingModel model)
        {
            if (!this.ModelState.IsValid)
                return (IHttpActionResult)this.BadRequest(this.ModelState);
            ItemType itemType = this.db.ItemTypes.Find((object)id);
            if (itemType == null)
                return (IHttpActionResult)this.NotFound();
            itemType.Name = model.Name;
            itemType.Code = model.Code;
            itemType.IsActive = model.IsActive;
            itemType.TypeUnits.ToList<TypeUnit>();
            foreach (TypeUnit typeUnit in itemType.TypeUnits.Where<TypeUnit>((Func<TypeUnit, bool>)(u => !model.Units.Contains(u.Id))).ToList<TypeUnit>())
                itemType.TypeUnits.Remove(typeUnit);
            foreach (int unit in model.Units)
            {
                int unitId = unit;
                if (itemType.TypeUnits.Count<TypeUnit>((Func<TypeUnit, bool>)(t => t.Id == unitId)) == 0)
                {
                    TypeUnit typeUnit = this.db.TypeUnits.Find((object)unitId);
                    if (typeUnit != null)
                        itemType.TypeUnits.Add(typeUnit);
                }
            }
            try
            {
                int num = await this.db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                return (IHttpActionResult)this.InternalServerError();
            }
            return (IHttpActionResult)this.StatusCode(HttpStatusCode.NoContent);
        }

        public async Task<IHttpActionResult> Post(ItemTypeBindingModel model)
        {
            if (!this.ModelState.IsValid)
                return (IHttpActionResult)this.BadRequest(this.ModelState);
            ItemType itemType = this.db.ItemTypes.Create();
            itemType.Name = model.Name;
            itemType.Code = model.Code;
            itemType.IsActive = model.IsActive;
            itemType.TypeUnits = (ICollection<TypeUnit>)new HashSet<TypeUnit>();
            foreach (int unit in model.Units)
            {
                TypeUnit typeUnit = this.db.TypeUnits.Find((object)unit);
                if (typeUnit != null)
                    itemType.TypeUnits.Add(typeUnit);
            }
            this.db.ItemTypes.Add(itemType);
            try
            {
                int num = await this.db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                return (IHttpActionResult)this.InternalServerError();
            }
            return (IHttpActionResult)this.Created(this.Url.Route("DefaultApi", (object)new
            {
                controller = "Types",
                id = itemType.Id
            }), new
            {
                Id = itemType.Id,
                Name = itemType.Name,
                Code = itemType.Code,
                IsActive = itemType.IsActive,
                Units = itemType.TypeUnits.Select<TypeUnit, int>((Func<TypeUnit, int>)(u => u.Id))
            });
        }

        public async Task<IHttpActionResult> Delete(int id)
        {
            ItemType itemType = null;
            try
            {
                itemType = await this.db.ItemTypes.FindAsync((object)id);
                if (itemType == null)
                    return (IHttpActionResult)this.NotFound();
                foreach (TypeUnit typeUnit in itemType.TypeUnits.ToList<TypeUnit>())
                    itemType.TypeUnits.Remove(typeUnit);
                this.db.ItemTypes.Remove(itemType);
                int num = await this.db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return (IHttpActionResult)this.Ok(new
            {
                Id = itemType.Id
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                this.db.Dispose();
            base.Dispose(disposing);
        }

        private bool ItemTypeExists(int id)
        {
            return this.db.ItemTypes.Count<ItemType>((Expression<Func<ItemType, bool>>)(e => e.Id == id)) > 0;
        }
    }
}
