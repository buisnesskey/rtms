using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using RTMS.Common;
using RTMS.Core;
using RTMS.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace RTMS.Controllers
{
    [Authorize, EnableCors("*", "*", "GET, POST, PUT, DELETE, OPTIONS", SupportsCredentials = true), RoutePrefix("api/Customer")]
    public class CustomerController : ApiController
    {
        private static log4net.ILog Log { get; set; }
        ILog log = log4net.LogManager.GetLogger(typeof(CustomerController));      

        private ApplicationDbContext db = new ApplicationDbContext();
        private class ExternalLoginData
        {
            public string LoginProvider
            {
                get;
                set;
            }

            public string ProviderKey
            {
                get;
                set;
            }

            public string UserName
            {
                get;
                set;
            }

            public IList<Claim> GetClaims()
            {
                IList<Claim> list = new List<Claim>();
                list.Add(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", this.ProviderKey, null, this.LoginProvider));
                if (this.UserName != null)
                {
                    list.Add(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", this.UserName, null, this.LoginProvider));
                }
                return list;
            }

            public static CustomerController.ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }
                Claim claim = identity.FindFirst("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier");
                if (claim == null || string.IsNullOrEmpty(claim.Issuer) || string.IsNullOrEmpty(claim.Value))
                {
                    return null;
                }
                if (claim.Issuer == "LOCAL AUTHORITY")
                {
                    return null;
                }
                return new CustomerController.ExternalLoginData
                {
                    LoginProvider = claim.Issuer,
                    ProviderKey = claim.Value,
                    UserName = identity.FindFirstValue("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name")
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                if (strengthInBits % 8 != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }
                byte[] array = new byte[strengthInBits / 8];
                CustomerController.RandomOAuthStateGenerator._random.GetBytes(array);
                return HttpServerUtility.UrlTokenEncode(array);
            }
        }

        private const string LocalLoginProvider = "Local";

        private ApplicationUserManager _userManager;

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat
        {
            get;
            private set;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return this._userManager ?? base.Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                this._userManager = value;
            }
        }

        private IAuthenticationManager Authentication
        {
            get
            {
                return base.Request.GetOwinContext().Authentication;
            }
        }

        [HttpGet, Route("Get")]
        public IHttpActionResult Get()
        {
            IHttpActionResult result;
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                RoleManager<IdentityRole> manager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(applicationDbContext));
                IdentityRole role = manager.FindByName(SystemRoles.Customer);
                IdentityRole roleSupervisor = manager.FindByName(SystemRoles.Supervisor);
                IdentityRole roleAdmin = manager.FindByName(SystemRoles.Admin);
                IdentityRole roleSuper = manager.FindByName(SystemRoles.Super);
                bool isAdmin = base.User.IsInRole(SystemRoles.Admin);
                bool isSuper = base.User.IsInRole(SystemRoles.Super);
                if (role == null)
                {
                    result = this.Ok();
                }
                else
                {
                    var supervisorCustomersList = db.SupervisorCustomers.ToList();
                    var resultList = (from u in this.UserManager.Users
                                      where u.Roles.Any((IdentityUserRole r) => r.RoleId == role.Id || r.RoleId == roleSupervisor.Id || ((isAdmin || isSuper) && (r.RoleId == roleAdmin.Id || r.RoleId == roleSuper.Id)))
                                      select new CustomerViewModel()
                                      {
                                          Id = u.Id,
                                          Name = u.Name,
                                          Email = u.Email,
                                          Phone = u.Phone,
                                          ContactPerson = u.ContactPerson,
                                          Address = u.Address,
                                          roleName = u.Roles.FirstOrDefault().RoleId == role.Id ? Enums.SystemRoleEnum.Customer.ToString()
                                              : u.Roles.FirstOrDefault().RoleId == roleSupervisor.Id ? Enums.SystemRoleEnum.Supervisor.ToString() : u.Roles.FirstOrDefault().RoleId == roleAdmin.Id ? Enums.SystemRoleEnum.Admin.ToString() : u.Roles.FirstOrDefault().RoleId == roleSuper.Id ? Enums.SystemRoleEnum.Super.ToString() : Enums.SystemRoleEnum.Customer.ToString(),
                                          typeRoleId = u.Roles.FirstOrDefault().RoleId == role.Id ? (byte)Enums.SystemRoleEnum.Customer
                                              : u.Roles.FirstOrDefault().RoleId == roleSupervisor.Id ? (byte)Enums.SystemRoleEnum.Supervisor : u.Roles.FirstOrDefault().RoleId == roleAdmin.Id ? (byte)Enums.SystemRoleEnum.Admin : u.Roles.FirstOrDefault().RoleId == roleSuper.Id ? (byte)Enums.SystemRoleEnum.Super : (byte)Enums.SystemRoleEnum.Customer,
                                          //typeSupervisorUserId = supervisorCustomersList.Where(x => x.CustomerUserId == u.Id).FirstOrDefault().SupervisorUserId
                                      }).ToList();
                    foreach (var item in resultList)
                    {
                        var sc = supervisorCustomersList.Where(x => x.CustomerUserId == item.Id).FirstOrDefault();
                        item.typeSupervisorUserId = sc != null ? sc.SupervisorUserId : null;
                    }
                    result = this.Ok(resultList);

                    
                }
            }
            
            return result;
        }

        [Route("Create")]
        public async Task<IHttpActionResult> Create(CustomerCreateBindingModel model)
        {
            IHttpActionResult result;
            if (!this.ModelState.IsValid)
            {
                result = this.BadRequest(this.ModelState);
            }
            else
            {
                ApplicationUser applicationUser = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    Name = model.Name,
                    Phone = model.Phone,
                    ContactPerson = model.ContactPerson,
                    Address = model.Address
                };
                IdentityResult identityResult = await this.UserManager.CreateAsync(applicationUser, model.Password);
                if (!identityResult.Succeeded)
                {
                    result = this.GetErrorResult(identityResult);
                }
                else
                {
                    if (model.TypeRoleId == (byte)Enums.SystemRoleEnum.Supervisor)
                        identityResult = await this.UserManager.AddToRoleAsync(applicationUser.Id, SystemRoles.Supervisor);
                    else if (model.TypeRoleId == (byte)Enums.SystemRoleEnum.Admin)
                        identityResult = await this.UserManager.AddToRoleAsync(applicationUser.Id, SystemRoles.Admin);
                    else
                        identityResult = await this.UserManager.AddToRoleAsync(applicationUser.Id, SystemRoles.Customer);

                    if (!identityResult.Succeeded)
                    {
                        result = this.GetErrorResult(identityResult);
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(model.TypeSupervisorUserId))
                        {
                            if (model.TypeRoleId != (byte)Enums.SystemRoleEnum.Admin)
                            {
                                ApplicationUser SupervisorApplicationUser = this.db.Users.Find(new object[]
					            {
						            model.TypeSupervisorUserId
					            });
                                SupervisorCustomer cus = new SupervisorCustomer()
                                {
                                    //CustomerUser = applicationUser,
                                    CustomerUserId = applicationUser.Id,
                                    SupervisorUser = SupervisorApplicationUser,
                                    SupervisorUserId = SupervisorApplicationUser.Id,
                                };
                                this.db.SupervisorCustomers.Add(cus);
                            }
				            try
				            {
					            await this.db.SaveChangesAsync();
				            }
				            catch (Exception ex)
				            {
                                var method = MethodBase.GetCurrentMethod();
                                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
					            result = this.InternalServerError();
					            return result;
				            }
                            
                        }

                        result = this.Ok<string>(applicationUser.Id);
                    }
                }
            }
            return result;
        }

        [Route("Update")]
        public async Task<IHttpActionResult> Update(CustomerUpdateBindingModel model)
        {
            IHttpActionResult result;
            if (!this.ModelState.IsValid)
            {
                result = this.BadRequest(this.ModelState);
            }
            else
            {
                ApplicationUser applicationUser = await this.UserManager.FindByIdAsync(model.Id);
                if (applicationUser == null)
                {
                    result = this.BadRequest("customer doesn't exist");
                }
                else
                {

                    applicationUser.UserName = model.Email;
                    applicationUser.Email = model.Email;
                    applicationUser.Name = model.Name;
                    applicationUser.Phone = model.Phone;
                    applicationUser.ContactPerson = model.ContactPerson;
                    applicationUser.Address = model.Address;
                    IdentityResult identityResult = await this.UserManager.UpdateAsync(applicationUser);
                    if (!identityResult.Succeeded)
                    {
                        result = this.GetErrorResult(identityResult);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(model.Password))
                        {
                            string var_6_272 = await this.UserManager.GeneratePasswordResetTokenAsync(model.Id);
                            await this.UserManager.ResetPasswordAsync(model.Id, var_6_272, model.Password);
                            if (!identityResult.Succeeded)
                            {
                                result = this.GetErrorResult(identityResult);
                                return result;
                            }
                        }

                        ApplicationDbContext applicationDbContext = new ApplicationDbContext();
                        RoleManager<IdentityRole> manager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(applicationDbContext));
                        IdentityRole roleCustomer = manager.FindByName(SystemRoles.Customer);
                        IdentityRole roleSupervisor = manager.FindByName(SystemRoles.Supervisor);
                        IdentityRole roleAdmin = manager.FindByName(SystemRoles.Admin);
                        bool isCustomer = applicationUser.Roles.Any(x => x.RoleId == roleCustomer.Id);
                        bool isSupervisor = applicationUser.Roles.Any(x => x.RoleId == roleSupervisor.Id);
                        bool isAdmin = applicationUser.Roles.Any(x => x.RoleId == roleAdmin.Id);

                        SupervisorCustomer cusOld = this.db.SupervisorCustomers.Where(x => x.CustomerUserId == applicationUser.Id).FirstOrDefault();
                        bool relationIsExist = db.SupervisorCustomers.Any(x => x.CustomerUserId == applicationUser.Id && x.SupervisorUserId == model.TypeSupervisorUserId);

                        //create supervisor
                        if (isCustomer && model.TypeRoleId == (byte)Enums.SystemRoleEnum.Supervisor)
                        {
                            identityResult = await this.UserManager.RemoveFromRoleAsync(applicationUser.Id, SystemRoles.Customer);
                            identityResult = await this.UserManager.AddToRoleAsync(applicationUser.Id, SystemRoles.Supervisor);

                            //remove prev relation
                            if (cusOld != null)
                                this.db.SupervisorCustomers.Remove(cusOld);
                        }
                        //create customer
                        else if (isSupervisor && model.TypeRoleId == (byte)Enums.SystemRoleEnum.Customer)
                        {
                            identityResult = await this.UserManager.RemoveFromRoleAsync(applicationUser.Id, SystemRoles.Supervisor);
                            identityResult = await this.UserManager.AddToRoleAsync(applicationUser.Id, SystemRoles.Customer);

                            if (!relationIsExist)
                            {
                                //remove prev relation
                                if (cusOld != null)
                                    this.db.SupervisorCustomers.Remove(cusOld);

                                if (!string.IsNullOrWhiteSpace(model.TypeSupervisorUserId))
                                {
                                    //add new relation
                                    SupervisorCustomer cus = new SupervisorCustomer()
                                    {
                                        //CustomerUser = applicationUser,
                                        CustomerUserId = applicationUser.Id,
                                        SupervisorUserId = model.TypeSupervisorUserId,
                                    };
                                    this.db.SupervisorCustomers.Add(cus);
                                }
                            }
                        }
                        else if(model.TypeRoleId != (byte)Enums.SystemRoleEnum.Admin)
                        {
                            //the role is the same
                            if (cusOld == null || model.TypeSupervisorUserId != cusOld.SupervisorUserId)
                            {
                                if (cusOld != null)
                                    this.db.SupervisorCustomers.Remove(cusOld);

                                if (!string.IsNullOrWhiteSpace(model.TypeSupervisorUserId))
                                {
                                    //add new relation
                                    SupervisorCustomer cus = new SupervisorCustomer()
                                    {
                                        //CustomerUser = applicationUser,
                                        CustomerUserId = applicationUser.Id,
                                        SupervisorUserId = model.TypeSupervisorUserId,
                                    };
                                    this.db.SupervisorCustomers.Add(cus);
                                }
                            }
                        }
                        else if (!isAdmin && model.TypeRoleId == (byte)Enums.SystemRoleEnum.Admin)
                        {
                            identityResult = await this.UserManager.RemoveFromRolesAsync(applicationUser.Id, new string[] { SystemRoles.Customer.ToString(), SystemRoles.Supervisor.ToString(), SystemRoles.Super.ToString(), SystemRoles.Admin.ToString() });
                            identityResult = await this.UserManager.AddToRoleAsync(applicationUser.Id, SystemRoles.Admin);
                        }
                        try
                        {
                            await this.db.SaveChangesAsync();
                        }
                        catch (Exception ex)
                        {
                            var method = MethodBase.GetCurrentMethod();
                            ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                            result = this.InternalServerError();
                            return result;
                        }
                        //update customer relations
                        //add if not exsit and delete others
                        result = this.Ok();
                    }
                }
            }
            return result;
        }

        //[AllowAnonymous]
        [Route("Delete")]
        //public async Task<IHttpActionResult> Delete(CustomerDeleteBindingModel model)
        public async Task<IHttpActionResult> Delete(string id)
        {
            IHttpActionResult result;
            try
            {
                //Log.Error("------------------L1------------------");
                //log.Error("------------------l1------------------");
                if (!this.ModelState.IsValid)
                {
                    //log.Error("------------------l2------------------");
                    //Log.Error("------------------L2------------------");
                    result = this.BadRequest(this.ModelState);
                }
                else
                {
                    //log.Error("------------------l3------------------");
                    //Log.Error("------------------L3------------------");
                    ApplicationUser applicationUser = await this.UserManager.FindByIdAsync(id);
                    if (applicationUser == null)
                    {
                        //log.Error("------------------l4------------------");
                        //Log.Error("------------------L4------------------");
                        result = this.BadRequest("customer doesn't exist");
                    }
                    else
                    {
                        //log.Error("------------------l5------------------");
                        //Log.Error("------------------L5------------------");
                        IdentityResult identityResult = await this.UserManager.DeleteAsync(applicationUser);
                        if (!identityResult.Succeeded)
                        {
                            //log.Error("------------------l6------------------");
                            //Log.Error("------------------L6------------------");
                            result = this.GetErrorResult(identityResult);
                        }
                        else
                        {
                            //log.Error("------------------l7------------------");
                            //Log.Error("------------------L7------------------");
                            //customer relation
                            //delete the old relations
                            result = this.Ok();
                        }
                        //log.Error("------------------l8------------------");
                        //Log.Error("------------------L8------------------");
                    }
                    //log.Error("------------------l9------------------");
                    //Log.Error("------------------L9------------------");
                }
                //log.Error("------------------l10------------------");
                //Log.Error("------------------L10------------------");
            }
            catch (Exception ex)
            {
                log.Error(ex);
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            //log.Error("------------------l11------------------");
            //Log.Error("------------------L11------------------");
            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this._userManager != null)
            {
                this._userManager.Dispose();
                this._userManager = null;
            }
            base.Dispose(disposing);
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return this.InternalServerError();
            }
            if (result.Succeeded)
            {
                return null;
            }
            if (result.Errors != null)
            {
                foreach (string current in result.Errors)
                {
                    base.ModelState.AddModelError("", current);
                }
            }
            if (base.ModelState.IsValid)
            {
                return this.BadRequest();
            }
            return this.BadRequest(base.ModelState);
        }


        [HttpGet]
        public IHttpActionResult GetSupervisors()
        {
            IHttpActionResult result;
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    RoleManager<IdentityRole> manager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(applicationDbContext));
                    IdentityRole role = manager.FindByName(SystemRoles.Customer);
                    IdentityRole roleSupervisor = manager.FindByName(SystemRoles.Supervisor);
                    if (role == null)
                    {
                        result = this.Ok();
                    }
                    else
                    {
                        result = this.Ok((from u in this.UserManager.Users
                                          where u.Roles.Any((IdentityUserRole r) => r.RoleId == roleSupervisor.Id)
                                          select new
                                          {
                                              value = u.Id,
                                              text = u.Name,
                                              //u.Email,
                                              //u.Phone,
                                              //u.ContactPerson,
                                              //u.Address,
                                              //roleName = u.Roles.FirstOrDefault().RoleId == role.Id ? Enums.SystemRoleEnum.Customer.ToString()
                                              //    : u.Roles.FirstOrDefault().RoleId == roleSupervisor.Id ? Enums.SystemRoleEnum.Supervisor.ToString() : Enums.SystemRoleEnum.Customer.ToString()
                                          }).ToList());
                    }
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return result;
        }
    }
}
