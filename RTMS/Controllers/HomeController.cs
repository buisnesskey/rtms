using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using RTMS.Common;
using RTMS.Core;
using RTMS.Models;
using RTMS.Models.Reports;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace RTMS.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Route("/")]
        public ActionResult Index()
        {
            return base.View();
        }
        [HttpPost]
        [Route("/report")]
        public JsonResult Report(int category = new int(),
            int groupBy = new int(),
            DateTime? fromDate = null,
            DateTime? toDate = null,
            string item = null,
            string serial = "",
            bool itemCheck = false,
            bool serialCheck = false,
            bool deliverdQCheck = false,
            byte currencyId = new byte(),
            int jtStartIndex = new int(),
            int jtPageSize = new int(),
            string jtSorting = null)
        {
            var result = db.OrderItems.Include("ItemTypes,Orders").
                Where(x => (x.State != State.Canceled) &&
                    (category == 0 || x.ItemTypeId == category) &&
                    (!fromDate.HasValue || x.Order.CreateDate >= fromDate) &&
                    (!toDate.HasValue || x.Order.CreateDate <= toDate) &&
                    (item == "" || item == null || x.Description.Contains(item)) &&
                    (serial == "" || serial == null || x.Order.SerialNumber.Contains(serial)) &&
                    (currencyId == 0 || x.CurrencyId == currencyId)).
                Select(x => new ReportViewModel()
            {
                item = x.ItemType.Name,
                quantity = x.Quantity,
                deliverdQuantity = x.DeliveredQuantity,
                priceBuy = x.BuyingPrice,
                priceSell = x.SellingPrice,
                creationDate = x.CreateDate,
                itemDescription = x.Description,
                serial = x.Order.SerialNumber,
                currencyId = x.CurrencyId
            }).OrderBy(x => x.creationDate).ToList();
            int totalCount = new int();
            var dateResult = RunReportGroupBy(result, out totalCount, groupBy, item, itemCheck, serial, serialCheck, currencyId, jtSorting, jtStartIndex, jtPageSize);


            if (RTMS.Common.Enums.GroupByEnum.Year.GetHashCode() == groupBy)
                return Json(new { Result = "OK", Records = dateResult, TotalRecordCount = totalCount }, JsonRequestBehavior.AllowGet);
            if (RTMS.Common.Enums.GroupByEnum.Month.GetHashCode() == groupBy)
                return Json(new { Result = "OK", Records = dateResult, TotalRecordCount = totalCount }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { Result = "OK", Records = dateResult, TotalRecordCount = totalCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("/dllCategory")]
        public JsonResult DllCategory()
        {
            return Json(db.ItemTypes.Select(x => new { id = x.Id, name = x.Name }).ToList(), JsonRequestBehavior.AllowGet);
        }

        private string GetState(int orderId)
        {
            int num = this.db.OrderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Waiting);
            int num2 = this.db.OrderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Processing);
            int num3 = this.db.OrderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Complete);
            if (num2 > 0 || (num > 0 && num3 > 0))
            {
                return State.Processing;
            }
            if (num == 0 && num2 == 0 && num3 > 0)
            {
                return State.Complete;
            }
            return State.Waiting;
        }

        [Authorize, HttpPost]
        public void Receipt(int id)
        {
            try
            {
                if (base.User.IsInRole(SystemRoles.Customer))
                {
                    return;
                }
                Order order = this.db.Orders.Find(new object[]
			{
				id
			});
                if (order == null)
                {
                    return;
                }
                ApplicationUser customer = this.db.Users.Find(new object[]
			{
				order.UserId
			});
                string text = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss-ffffff") + ".xlsx";
                text = base.Server.MapPath("~/temp/" + text);
                Excel.SaveReceipt(order, customer, text);
                base.Response.Clear();
                base.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                base.Response.AddHeader("Content-Disposition", "attachment; filename=WebStreamDownload.xlsx");
                string s = Convert.ToBase64String(System.IO.File.ReadAllBytes(text));
                System.IO.File.Delete(text);
                base.Response.Write(s);
                base.Response.Flush();
                base.Response.End();
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
        }

        [Authorize, HttpPost]
        public void Invoice(int id)
        {
            try
            {
                if (base.User.IsInRole(SystemRoles.Customer))
                {
                    return;
                }
                Order order = this.db.Orders.Find(new object[]
			{
				id
			});
                if (order == null)
                {
                    return;
                }
                ApplicationUser customer = this.db.Users.Find(new object[]
			{
				order.UserId
			});
                string text = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss-ffffff") + ".xlsx";
                text = base.Server.MapPath("~/temp/" + text);
                Excel.SaveInvoice(order, customer, text);
                base.Response.Clear();
                base.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                base.Response.AddHeader("Content-Disposition", "attachment; filename=WebStreamDownload.xlsx");
                string s = Convert.ToBase64String(System.IO.File.ReadAllBytes(text));
                System.IO.File.Delete(text);
                base.Response.Write(s);
                base.Response.Flush();
                base.Response.End();
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
        }

        [Authorize, HttpPost]
        public void Items(int id)
        {
            //if (base.User.IsInRole(SystemRoles.Customer))
            //{
            //    return;
            //}
            Order order = this.db.Orders.Find(new object[]
			{
				id
			});
            if (order == null)
            {
                return;
            }
            ApplicationUser customer = this.db.Users.Find(new object[]
			{
				order.UserId
			});
            string text = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss-ffffff") + ".xlsx";
            text = base.Server.MapPath("~/temp/" + text);
            Excel.SaveItems(order, customer, text);
            base.Response.Clear();
            base.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            base.Response.AddHeader("Content-Disposition", "attachment; filename=WebStreamDownload.xlsx");
            string s = Convert.ToBase64String(System.IO.File.ReadAllBytes(text));
            System.IO.File.Delete(text);
            base.Response.Write(s);
            base.Response.Flush();
            base.Response.End();
        }

        #region export RTMS
        [HttpPost]
        [Route("/exporReport")]
        public JsonResult ExporReport(string jtSorting = null, int category = new int(), int groupBy = new int(), DateTime? fromDate = null, DateTime? toDate = null, string item = null, bool itemCheck = false, string serial = null, bool serialCheck = false, byte currencyId = new byte())
        {
            try
            {
                string text = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss-ffffff") + ".xlsx";
                text = base.Server.MapPath("~/temp/" + text);

                var result = db.OrderItems.Include("ItemTypes,Orders").
                    Where(x => (x.State != State.Canceled) && 
                        (category == 0 || x.ItemTypeId == category) &&
                        (!fromDate.HasValue || x.Order.CreateDate >= fromDate) &&
                        (!toDate.HasValue || x.Order.CreateDate <= toDate) &&
                        (item == "" || item == null || x.Description.Contains(item)) &&
                    (serial == "" || serial == null || x.Order.SerialNumber.Contains(serial)) &&
                    (currencyId == 0 || x.CurrencyId == currencyId)
                        ).
                    Select(x => new ReportViewModel()
                    {
                        item = x.ItemType.Name,
                        quantity = x.Quantity,
                        deliverdQuantity = x.DeliveredQuantity,
                        priceBuy = x.BuyingPrice,
                        priceSell = x.SellingPrice,
                        creationDate = x.CreateDate,
                        itemDescription = x.Description,
                        serial = x.Order.SerialNumber,
                        currencyId = x.CurrencyId
                    }).OrderBy(x => x.creationDate).ToList();
                int totalCount = new int();
                var dateResult = RunReportGroupBy(result, out totalCount, groupBy, item, itemCheck, serial, serialCheck, currencyId, jtSorting, -1, 0);

                dateResult.Add(new ReportGroupByViewModel());
                dateResult.Add(new ReportGroupByViewModel() {
                    quantity = dateResult.Sum(x => x.quantity),
                    priceBuyTotal = dateResult.Sum(x => x.priceBuyTotal),
                    priceSellTotal = dateResult.Sum(x => x.priceSellTotal),
                    priceSell = dateResult.Sum(x => x.priceSell),
                    priceBuy = dateResult.Sum(x => x.priceBuy),
                    deliverdQuantity = dateResult.Sum(x => x.deliverdQuantity),
                    priceDeliverdSellTotal = dateResult.Sum(x => x.priceDeliverdSellTotal),
                });

                #region Export to Excel
                Dictionary<string, string> dictionaryHeaders = new Dictionary<string, string>();
                List<string> excludedColumns = new List<string>();
                dictionaryHeaders.Add("serial", "serial");
                dictionaryHeaders.Add("item", "item");
                dictionaryHeaders.Add("itemDescription", "itemDescription");
                dictionaryHeaders.Add("quantity", "quantity");
                dictionaryHeaders.Add("deliverdQuantity", "deliverdQuantity");
                dictionaryHeaders.Add("priceBuy", "priceBuy");
                dictionaryHeaders.Add("priceSell", "Price");
                dictionaryHeaders.Add("priceBuyTotal", "priceBuyTotal");
                dictionaryHeaders.Add("priceSellTotal", "priceSellTotal");
                dictionaryHeaders.Add("currencyName", "currencyName");
                dictionaryHeaders.Add("creationDate", "creationDate");
                dictionaryHeaders.Add("priceDeliverdSellTotal", "Total");


                excludedColumns.Add("priceBuy");
                excludedColumns.Add("priceBuyTotal");
                excludedColumns.Add("priceSellTotal");
                excludedColumns.Add("creationDate");

                //dictionaryHeaders.Add("SMSContent", "SMS Content");
                //dictionaryHeaders.Add("CreationDate", "Creation Date");
                //dictionaryHeaders.Add("SendingStatusName", "SMS Status ");
                //
                //excludedColumns.Add("SendingDate");
                //excludedColumns.Add("SMSChannelId");


                Helper helper = new Helper();
                string fileName = "";
                string pathExcel = base.Server.MapPath("~/temp/");
                if (dateResult != null && dateResult.Count > new int())
                {
                    var dataTableResult = helper.ToDataTableWithHeaders(dateResult, excludedColumns, dictionaryHeaders);
                    bool exportToExcelResult = helper.GenerateExcelSheetWithoutDownload(dataTableResult, pathExcel, out fileName);
                }
                #endregion


                //text = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss-ffffff") + ".xlsx";
                //string fileName = text;

                string url = helper.ResolveServerUrl(VirtualPathUtility.ToAbsolute("~/temp/" + fileName), false);
                string siteUrl = ConfigurationManager.AppSettings["ServerSiteUrl"];
                string fileUrl = ConfigurationManager.AppSettings["ServerSiteUrl"] + fileName;
                return Json(new { Result = "OK", FileName = url });
                return Json(new { Result = "OK", FileName = fileUrl });


                string completeFileName = base.Server.MapPath("~/temp/" + fileName);
                //Excel.SaveItems(order, customer, text);
                base.Response.Clear();
                base.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                base.Response.AddHeader("Content-Disposition", "attachment; filename=WebStreamDownload.xlsx");
                //string s = Convert.ToBase64String(System.IO.File.ReadAllBytes(text));
                //string s = Convert.ToBase64String(System.IO.File.ReadAllBytes(base.Server.MapPath("~/temp/" + text)));
                string s = Convert.ToBase64String(System.IO.File.ReadAllBytes(completeFileName));
                System.IO.File.Delete(text);
                base.Response.Write(s);
                base.Response.Flush();
                base.Response.End();
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
        }

        #endregion

        #region private methods
        private List<ReportGroupByViewModel> RunReportGroupBy(List<ReportViewModel> result, out int totalCount, int groupBy = new int(), string item = null, bool itemCheck = false, string serial = null, bool serialCheck = false, byte currencyId = new byte(), string jtSorting = null, int jtStartIndex = new int(), int jtPageSize = new int())
        {
            totalCount = new int();
            var dateGroupByResult = (from s in result
                                     group s by new
                                     {
                                         item = new { s.item },
                                         currecny = s.currencyId,
                                         itemDescription = (string.IsNullOrWhiteSpace(item) && !itemCheck ? null : s.itemDescription),
                                         orderSerial = (string.IsNullOrWhiteSpace(serial) && !serialCheck ? null : s.serial),
                                         date = new DateTime(s.creationDate.Year, s.creationDate.Month, s.creationDate.Day)
                                     } into g
                                     select new { g });
            if (RTMS.Common.Enums.GroupByEnum.Year.GetHashCode() == groupBy)
                dateGroupByResult = (from s in result
                                     group s by new
                                     {
                                         item = new { s.item },
                                         currecny = s.currencyId,
                                         itemDescription = (string.IsNullOrWhiteSpace(item) && !itemCheck ? null : s.itemDescription),
                                         orderSerial = (string.IsNullOrWhiteSpace(serial) && !serialCheck ? null : s.serial),
                                         date = new DateTime(s.creationDate.Year, 1, 1)
                                     } into g
                                     select new { g });
            if (RTMS.Common.Enums.GroupByEnum.Month.GetHashCode() == groupBy)
                dateGroupByResult = (from s in result
                                     group s by new
                                     {
                                         item = new { s.item },
                                         currecny = s.currencyId,
                                         itemDescription = (string.IsNullOrWhiteSpace(item) && !itemCheck ? null : s.itemDescription),
                                         orderSerial = (string.IsNullOrWhiteSpace(serial) && !serialCheck ? null : s.serial),
                                         date = new DateTime(s.creationDate.Year, s.creationDate.Month, 1)
                                     } into g
                                     select new { g });




            var finalResult = from s in dateGroupByResult
                              select new ReportGroupByViewModel()
                              {
                                  item = s.g.Key.item.item,
                                  quantity = s.g.Sum(x => x.quantity),
                                  deliverdQuantity = s.g.Sum(x => x.deliverdQuantity),
                                  priceBuy = Math.Round(s.g.Average(x => x.priceBuy), 2),//AverageAverageAverageAverageAverageAverage
                                  priceSell = Math.Round(s.g.Average(x => x.priceSell), 2),//AverageAverageAverageAverageAverageAverage
                                  priceBuyTotal = Math.Round(s.g.Sum(x => (x.priceBuy * (decimal)x.quantity)), 2),
                                  priceSellTotal = s.g.Sum(x => (x.priceSell * (decimal)x.quantity)),
                                  //priceSellTotal = Math.Round(s.g.Average(x => x.priceSell) * (decimal)s.g.Sum(x => x.quantity), 2),
                                  priceDeliverdSellTotal = s.g.Sum(x => (x.priceSell * (decimal)x.deliverdQuantity)),
                                  //priceDeliverdSellTotal = Math.Round(s.g.Average(x => x.priceSell) * (decimal)s.g.Sum(x => x.deliverdQuantity), 2),
                                  creationDate = RTMS.Common.Enums.GroupByEnum.Month.GetHashCode() == groupBy ? s.g.Key.date.ToString("MMM-yyyy") : RTMS.Common.Enums.GroupByEnum.Year.GetHashCode() == groupBy ? s.g.Key.date.Year.ToString() : s.g.Key.date.ToString("dd-MM-yyyy"),
                                  itemDescription = s.g.Key.itemDescription,
                                  serial = s.g.Key.orderSerial,
                                  currencyName = s.g.Key.currecny > new byte() ? ((Enums.CurrencyEnum)s.g.Key.currecny).ToString() : ""
                              };



            #region sorting
            string[] sortingExpression = jtSorting.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            RTMS.Common.Enums.SortDirection sortingDir = sortingExpression[1] == "DESC" ? RTMS.Common.Enums.SortDirection.Descending : RTMS.Common.Enums.SortDirection.Ascending;
            switch (sortingExpression[0])
            {
                case "quantity":
                    if (sortingDir == Common.Enums.SortDirection.Ascending)
                        finalResult = finalResult.OrderBy(x => x.quantity);
                    else
                        finalResult = finalResult.OrderByDescending(x => x.quantity);
                    break;
                case "deliverdQuantity":
                    if (sortingDir == Common.Enums.SortDirection.Ascending)
                        finalResult = finalResult.OrderBy(x => x.deliverdQuantity);
                    else
                        finalResult = finalResult.OrderByDescending(x => x.deliverdQuantity);
                    break;
                case "creationDate":
                    if (sortingDir == Common.Enums.SortDirection.Ascending)
                        finalResult = finalResult.OrderBy(x => x.creationDate);
                    else
                        finalResult = finalResult.OrderByDescending(x => x.creationDate);
                    break;
                case "priceSellTotal":
                    if (sortingDir == Common.Enums.SortDirection.Ascending)
                        finalResult = finalResult.OrderBy(x => x.priceSellTotal);
                    else
                        finalResult = finalResult.OrderByDescending(x => x.priceSellTotal);
                    break;
                case "priceBuyTotal":
                    if (sortingDir == Common.Enums.SortDirection.Ascending)
                        finalResult = finalResult.OrderBy(x => x.priceBuyTotal);
                    else
                        finalResult = finalResult.OrderByDescending(x => x.priceBuyTotal);
                    break;
                case "priceSell":
                    if (sortingDir == Common.Enums.SortDirection.Ascending)
                        finalResult = finalResult.OrderBy(x => x.priceSell);
                    else
                        finalResult = finalResult.OrderByDescending(x => x.priceSell);
                    break;
                case "priceBuy":
                    if (sortingDir == Common.Enums.SortDirection.Ascending)
                        finalResult = finalResult.OrderBy(x => x.priceBuy);
                    else
                        finalResult = finalResult.OrderByDescending(x => x.priceBuy);
                    break;
                case "item":
                    if (sortingDir == Common.Enums.SortDirection.Ascending)
                        finalResult = finalResult.OrderBy(x => x.item);
                    else
                        finalResult = finalResult.OrderByDescending(x => x.item);
                    break;
                case "itemDescription":
                    if (sortingDir == Common.Enums.SortDirection.Ascending)
                        finalResult = finalResult.OrderBy(x => x.itemDescription);
                    else
                        finalResult = finalResult.OrderByDescending(x => x.itemDescription);
                    break;
                case "serial":
                    if (sortingDir == Common.Enums.SortDirection.Ascending)
                        finalResult = finalResult.OrderBy(x => x.serial);
                    else
                        finalResult = finalResult.OrderByDescending(x => x.serial);
                    break;
                case "priceDeliverdSellTotal":
                    if (sortingDir == Common.Enums.SortDirection.Ascending)
                        finalResult = finalResult.OrderBy(x => x.priceDeliverdSellTotal);
                    else
                        finalResult = finalResult.OrderByDescending(x => x.priceDeliverdSellTotal);
                    break;
                case "currencyName":
                    if (sortingDir == Common.Enums.SortDirection.Ascending)
                        finalResult = finalResult.OrderBy(x => x.currencyName);
                    else
                        finalResult = finalResult.OrderByDescending(x => x.currencyName);
                    break;
                default:
                    finalResult = finalResult.OrderBy(x => x.item);
                    break;
            }
            #endregion

            totalCount = finalResult.Count();
            if (jtStartIndex == 0)
                finalResult = finalResult.Take(jtPageSize);
            else if (jtStartIndex != -1)
                finalResult = finalResult.Skip(jtStartIndex).Take(jtPageSize);

            return finalResult.ToList();
        }


        #endregion
    }
}
