using Microsoft.AspNet.Identity;
using RTMS.Common;
using RTMS.Core;
using RTMS.Models;
using RTMS.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace RTMS.Controllers
{
    [Authorize, EnableCors("*", "*", "GET, POST, PUT, DELETE, OPTIONS", SupportsCredentials = true)]
    public class ItemsController : ApiController
    {
        private string ServerUrl = WebConfigurationManager.AppSettings["ServerUrl"];

        private ApplicationDbContext db = new ApplicationDbContext();

        private Status GetState(int orderId)
        {
            Status status = new Status();
            try
            {
                status.AllCount = this.db.OrderItems.Count((OrderItem item) => item.OrderId == orderId);
                status.WaitingCount = this.db.OrderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Waiting);
                status.ProcessingCount = this.db.OrderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Processing);
                status.CompleteCount = this.db.OrderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Complete);
                status.AlternativeCount = this.db.OrderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Alternative);
                status.CanceledCount = this.db.OrderItems.Count((OrderItem item) => item.OrderId == orderId && item.State == State.Canceled);
                if (status.ProcessingCount > 0 || (status.WaitingCount > 0 && status.CompleteCount > 0))
                {
                    status.FinalStatus = State.Processing;
                }
                else if (status.WaitingCount == 0 && status.ProcessingCount == 0 && (status.CompleteCount > 0 || status.CanceledCount > 0 || status.AlternativeCount > 0))
                {
                    status.FinalStatus = State.Complete;
                }
                else if (status.AllCount > 0 && status.AllCount == status.CanceledCount)
                {
                    status.FinalStatus = State.Canceled;
                }
                else if (status.AllCount > 0 && status.AllCount == status.AlternativeCount)
                {
                    status.FinalStatus = State.Complete;
                }
                else
                {
                    status.FinalStatus = State.Waiting;
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return status;
        }

        [HttpGet]
        public IEnumerable Get(string customerId = "", int? orderId = null, string state = "")
        {
            string currentUserId = base.User.Identity.GetUserId();
            if (base.User.IsInRole(SystemRoles.Customer))
            {
                return from item in
                           (from item in this.db.OrderItems
                            where item.Order.UserId == currentUserId && (!orderId.HasValue || item.OrderId == orderId.Value) && (string.IsNullOrEmpty(state) || item.State == state.ToLower())
                            select item).ToList<OrderItem>()
                       select new
                       {
                           Id = item.Id,
                           OrderId = item.OrderId,
                           TypeId = item.ItemTypeId,
                           UnitId = item.UnitId,
                           TypeText = item.TypeText,
                           UnitText = item.UnitText,
                           Remarks = item.Remarks,
                           Description = item.Description,
                           State = item.State,
                           StateRank = ((item.State == State.Waiting) ? 10 : ((item.State == State.Processing) ? 20 : ((item.State == State.Complete) ? 30 : 40))),
                           Quantity = item.Quantity,
                           DeliveredQuantity = item.DeliveredQuantity,
                           Attachment = (string.IsNullOrEmpty(item.Attachment) ? null : (this.ServerUrl + "/Uploads/" + item.Attachment)),
                           CreateDate = DateManager.GetLocalDate(new DateTime?(item.CreateDate)),
                           CompleteDate = DateManager.GetLocalDate(item.CompleteDate),
                           CustomerName = item.Order.User.Name,
                           CustomerId = item.Order.User.Id,
                           TypeName = (item.ItemTypeId.HasValue ? item.ItemType.Name : item.TypeText),
                           CurrencyId = item.CurrencyId,
                           Order = new
                           {
                               item.Order.Id,
                               item.Order.SerialNumber
                           }
                       };
            }
            return from item in
                       (from item in this.db.OrderItems
                        where (!orderId.HasValue || item.OrderId == orderId.Value) && (string.IsNullOrEmpty(customerId) || item.Order.UserId == customerId) && (string.IsNullOrEmpty(state) || item.State == state.ToLower())
                        select item).ToList<OrderItem>()
                   select new
                   {
                       Id = item.Id,
                       OrderId = item.OrderId,
                       TypeId = item.ItemTypeId,
                       UnitId = item.UnitId,
                       TypeText = item.TypeText,
                       UnitText = item.UnitText,
                       Remarks = item.Remarks,
                       Description = item.Description,
                       State = item.State,
                       StateRank = ((item.State == State.Waiting) ? 10 : ((item.State == State.Processing) ? 20 : ((item.State == State.Complete) ? 30 : 40))),
                       Quantity = item.Quantity,
                       DeliveredQuantity = item.DeliveredQuantity,
                       Attachment = (string.IsNullOrEmpty(item.Attachment) ? null : (this.ServerUrl + "/Uploads/" + item.Attachment)),
                       CreateDate = DateManager.GetLocalDate(new DateTime?(item.CreateDate)),
                       CompleteDate = DateManager.GetLocalDate(item.CompleteDate),
                       CustomerName = item.Order.User.Name,
                       CustomerId = item.Order.User.Id,
                       BuyingPrice = item.BuyingPrice,
                       SellingPrice = item.SellingPrice,
                       SupplierName = ((item.Supplier != null) ? item.Supplier.Name : ""),
                       SupplierId = ((item.Supplier != null) ? item.Supplier.Id : 0),
                       TypeName = (item.ItemTypeId.HasValue ? item.ItemType.Name : item.TypeText),
                       CurrencyId = item.CurrencyId,
                       CurrencyName = item.CurrencyId > new byte() ? ((Enums.CurrencyEnum)item.CurrencyId).ToString() : "",
                       Order = new
                       {
                           item.Order.Id,
                           item.Order.SerialNumber
                       }
                   };
        }

        public async Task<IHttpActionResult> Post(int id, ItemBindingModel model)
        {
            IHttpActionResult result;
            if (!this.ModelState.IsValid)
            {
                result = this.BadRequest(this.ModelState);
            }
            else
            {
                OrderItem orderItem = this.db.OrderItems.Find(new object[]
				{
					id
				});
                if (orderItem == null)
                {
                    result = this.NotFound();
                }
                else if (this.User.IsInRole(SystemRoles.Customer) && this.GetState(orderItem.OrderId).FinalStatus != State.Waiting)
                {
                    result = this.BadRequest("Order processing has started phase, call administrator for modification");
                }
                else
                {
                    orderItem.Order = this.db.Orders.Find(new object[]
					{
						model.OrderId
					});
                    orderItem.Remarks = model.Remarks;
                    orderItem.Description = model.Description;
                    orderItem.Quantity = model.Quantity;
                    orderItem.TypeText = model.TypeText;
                    orderItem.UnitText = model.UnitText;
                    if (model.TypeId.HasValue && model.TypeId > 0)
                    {
                        orderItem.ItemType = this.db.ItemTypes.Find(new object[]
						{
							model.TypeId
						});
                    }
                    if (model.UnitId.HasValue && model.UnitId > 0)
                    {
                        orderItem.Unit = this.db.TypeUnits.Find(new object[]
						{
							model.UnitId
						});
                    }
                    if (model.Attachment != null)
                    {
                        string text = HostingEnvironment.MapPath("~/Uploads/");
                        if (!string.IsNullOrEmpty(orderItem.Attachment))
                        {
                            try
                            {
                                File.Delete(text + orderItem.Attachment);
                            }
                            catch (Exception ex)
                            {
                                File.AppendAllText(HostingEnvironment.MapPath("~/temp/errors.txt"), "\n" + ex.Message);
                            }
                        }
                        string text2 = string.Concat(new object[]
						{
							model.OrderId,
							"-",
							DateTime.UtcNow.ToString("dd-MM-yyyy-hh-mm-ffffff"),
							Path.GetExtension(model.Attachment.Name)
						});
                        try
                        {
                            if (!Directory.Exists(text))
                            {
                                Directory.CreateDirectory(text);
                            }
                            File.WriteAllBytes(text + text2, Convert.FromBase64String(model.Attachment.Content));
                            orderItem.Attachment = text2;
                        }
                        catch (Exception ex2)
                        {
                            File.AppendAllText(HostingEnvironment.MapPath("~/temp/errors.txt"), "\n" + ex2.Message);
                        }
                    }
                    if (!this.User.IsInRole(SystemRoles.Customer))
                    {
                        orderItem.DeliveredQuantity = model.DeliveredQuantity;
                        if (model.SellingPrice > 0.0m)
                        {
                            orderItem.SellingPrice = model.SellingPrice;
                        }
                        if (model.BuyingPrice > 0.0m)
                        {
                            orderItem.BuyingPrice = model.BuyingPrice;
                        }
                        if (model.SupplierId.HasValue && model.SupplierId > 0)
                        {
                            orderItem.Supplier = this.db.Suppliers.Find(new object[]
							{
								model.SupplierId
							});
                        }
                        if (string.IsNullOrEmpty(model.State))
                        {
                            orderItem.State = State.Waiting;
                        }
                        else
                        {
                            orderItem.CompleteDate = null;
                            string a = model.State.ToLower();
                            if (!(a == "waiting"))
                            {
                                if (!(a == "processing"))
                                {
                                    if (!(a == "canceled"))
                                    {
                                        if (!(a == "alternative"))
                                        {
                                            if (!(a == "complete"))
                                            {
                                                orderItem.State = State.Waiting;
                                            }
                                            else
                                            {
                                                orderItem.State = State.Complete;
                                                orderItem.CompleteDate = new DateTime?(DateTime.UtcNow);
                                            }
                                        }
                                        else
                                        {
                                            orderItem.State = State.Alternative;
                                        }
                                    }
                                    else
                                    {
                                        orderItem.State = State.Canceled;
                                    }
                                }
                                else
                                {
                                    orderItem.State = State.Processing;
                                }
                            }
                            else
                            {
                                orderItem.State = State.Waiting;
                            }
                        }
                    }
                    orderItem.CurrencyId = model.CurrencyId;
                    this.db.Entry<OrderItem>(orderItem).State = EntityState.Modified;
                    try
                    {
                        await this.db.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        var method = MethodBase.GetCurrentMethod();
                        ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                        result = this.InternalServerError();
                        return result;
                    }
                    if (this.User.IsInRole(SystemRoles.Customer))
                    {
                        result = this.CreatedAtRoute("DefaultApi", new
                        {
                            id = orderItem.Id
                        }, new
                        {
                            Id = orderItem.Id,
                            OrderId = orderItem.OrderId,
                            TypeId = ((orderItem.ItemType == null) ? 0 : orderItem.ItemType.Id),
                            UnitId = ((orderItem.Unit == null) ? 0 : orderItem.Unit.Id),
                            TypeText = orderItem.TypeText,
                            UnitText = orderItem.UnitText,
                            Remarks = orderItem.Remarks,
                            Description = orderItem.Description,
                            State = orderItem.State,
                            StateRank = ((orderItem.State == State.Waiting) ? 10 : ((orderItem.State == State.Processing) ? 20 : ((orderItem.State == State.Complete) ? 30 : 40))),
                            Quantity = orderItem.Quantity,
                            DeliveredQuantity = orderItem.DeliveredQuantity,
                            Attachment = (string.IsNullOrEmpty(orderItem.Attachment) ? null : (this.ServerUrl + "/Uploads/" + orderItem.Attachment)),
                            CreateDate = DateManager.GetLocalDate(new DateTime?(orderItem.CreateDate)),
                            CompleteDate = DateManager.GetLocalDate(orderItem.CompleteDate),
                            CustomerName = orderItem.Order.User.Name,
                            CustomerId = orderItem.Order.User.Id,
                            TypeName = (orderItem.ItemTypeId.HasValue ? orderItem.ItemType.Name : orderItem.TypeText),
                            CurrencyId = orderItem.CurrencyId,
                            CurrencyName = orderItem.CurrencyId > new byte() ? ((Enums.CurrencyEnum)orderItem.CurrencyId).ToString() : "",
                            Order = new
                            {
                                orderItem.Order.Id,
                                orderItem.Order.SerialNumber
                            }
                        });
                    }
                    else
                    {
                        result = this.CreatedAtRoute("DefaultApi", new
                        {
                            id = orderItem.Id
                        }, new
                        {
                            Id = orderItem.Id,
                            OrderId = orderItem.OrderId,
                            TypeId = ((orderItem.ItemType == null) ? 0 : orderItem.ItemType.Id),
                            UnitId = ((orderItem.Unit == null) ? 0 : orderItem.Unit.Id),
                            TypeText = orderItem.TypeText,
                            UnitText = orderItem.UnitText,
                            Remarks = orderItem.Remarks,
                            Description = orderItem.Description,
                            State = orderItem.State,
                            StateRank = ((orderItem.State == State.Waiting) ? 10 : ((orderItem.State == State.Processing) ? 20 : ((orderItem.State == State.Complete) ? 30 : 40))),
                            Quantity = orderItem.Quantity,
                            DeliveredQuantity = orderItem.DeliveredQuantity,
                            Attachment = (string.IsNullOrEmpty(orderItem.Attachment) ? null : (this.ServerUrl + "/Uploads/" + orderItem.Attachment)),
                            CreateDate = DateManager.GetLocalDate(new DateTime?(orderItem.CreateDate)),
                            CompleteDate = DateManager.GetLocalDate(orderItem.CompleteDate),
                            CustomerName = orderItem.Order.User.Name,
                            CustomerId = orderItem.Order.User.Id,
                            BuyingPrice = orderItem.BuyingPrice,
                            SellingPrice = orderItem.SellingPrice,
                            SupplierName = ((orderItem.Supplier != null) ? orderItem.Supplier.Name : ""),
                            SupplierId = ((orderItem.Supplier != null) ? orderItem.Supplier.Id : 0),
                            CurrencyId = orderItem.CurrencyId,
                            CurrencyName = orderItem.CurrencyId > new byte() ? ((Enums.CurrencyEnum)orderItem.CurrencyId).ToString() : "",
                            Order = new
                            {
                                orderItem.Order.Id,
                                orderItem.Order.SerialNumber
                            }
                        });
                    }
                }
            }
            return result;
        }

        public async Task<IHttpActionResult> Post(ItemBindingModel model)
        {
            IHttpActionResult result;
            if (!this.ModelState.IsValid)
            {
                result = this.BadRequest(this.ModelState);
            }
            else
            {
                OrderItem orderItem = this.db.OrderItems.Create();
                orderItem.CreateDate = DateTime.UtcNow;
                orderItem.Order = this.db.Orders.Find(new object[]
				{
					model.OrderId
				});
                orderItem.Remarks = model.Remarks;
                orderItem.Description = model.Description;
                orderItem.Quantity = model.Quantity;
                orderItem.TypeText = model.TypeText;
                orderItem.UnitText = model.UnitText;
                if (model.TypeId.HasValue && model.TypeId > 0)
                {
                    orderItem.ItemType = this.db.ItemTypes.Find(new object[]
					{
						model.TypeId
					});
                }
                if (model.UnitId.HasValue && model.UnitId > 0)
                {
                    orderItem.Unit = this.db.TypeUnits.Find(new object[]
					{
						model.UnitId
					});
                }
                string userId = this.User.Identity.GetUserId();
                var appUser = this.db.SupervisorCustomers.Where(x => x.CustomerUserId.Equals(userId)).FirstOrDefault();
                if (appUser != null)
                    orderItem.State = State.Pending;
                else
                    orderItem.State = State.Waiting;
                if (model.Attachment != null)
                {
                    string text = HostingEnvironment.MapPath("~/Uploads/");
                    string text2 = string.Concat(new object[]
					{
						model.OrderId,
						"-",
						DateTime.UtcNow.ToString("dd-MM-yyyy-hh-mm-ffffff"),
						Path.GetExtension(model.Attachment.Name)
					});
                    try
                    {
                        if (!Directory.Exists(text))
                        {
                            Directory.CreateDirectory(text);
                        }
                        File.WriteAllBytes(text + text2, Convert.FromBase64String(model.Attachment.Content));
                        orderItem.Attachment = text2;
                    }
                    catch (Exception ex)
                    {
                        File.AppendAllText(HostingEnvironment.MapPath("~/temp/errors.txt"), "\n" + ex.Message);
                    }
                }
                if (!this.User.IsInRole(SystemRoles.Customer))
                {
                    orderItem.DeliveredQuantity = model.DeliveredQuantity;
                    if (model.SellingPrice > 0.0m)
                    {
                        orderItem.SellingPrice = model.SellingPrice;
                    }
                    if (model.BuyingPrice > 0.0m)
                    {
                        orderItem.BuyingPrice = model.BuyingPrice;
                    }
                    if (model.SupplierId.HasValue && model.SupplierId > 0)
                    {
                        orderItem.Supplier = this.db.Suppliers.Find(new object[]
						{
							model.SupplierId
						});
                    }
                    if (string.IsNullOrEmpty(model.State))
                    {
                        orderItem.State = State.Waiting;
                    }
                    else
                    {
                        orderItem.CompleteDate = null;
                        string a = model.State.ToLower();
                        if (!(a == "waiting"))
                        {
                            if (!(a == "processing"))
                            {
                                if (!(a == "canceled"))
                                {
                                    if (!(a == "alternative"))
                                    {
                                        if (!(a == "complete"))
                                        {
                                            orderItem.State = State.Waiting;
                                        }
                                        else
                                        {
                                            orderItem.State = State.Complete;
                                            orderItem.CompleteDate = new DateTime?(DateTime.UtcNow);
                                        }
                                    }
                                    else
                                    {
                                        orderItem.State = State.Alternative;
                                    }
                                }
                                else
                                {
                                    orderItem.State = State.Canceled;
                                }
                            }
                            else
                            {
                                orderItem.State = State.Processing;
                            }
                        }
                        else
                        {
                            orderItem.State = State.Waiting;
                        }
                    }
                }
                orderItem.CurrencyId = model.CurrencyId;
                this.db.OrderItems.Add(orderItem);
                try
                {
                    await this.db.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    var method = MethodBase.GetCurrentMethod();
                    ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                    result = this.InternalServerError();
                    return result;
                }
                if (this.User.IsInRole(SystemRoles.Customer))
                {
                    result = this.CreatedAtRoute("DefaultApi", new
                    {
                        id = orderItem.Id
                    }, new
                    {
                        Id = orderItem.Id,
                        OrderId = orderItem.OrderId,
                        TypeId = ((orderItem.ItemType == null) ? 0 : orderItem.ItemType.Id),
                        UnitId = ((orderItem.Unit == null) ? 0 : orderItem.Unit.Id),
                        TypeText = orderItem.TypeText,
                        UnitText = orderItem.UnitText,
                        Remarks = orderItem.Remarks,
                        Description = orderItem.Description,
                        State = orderItem.State,
                        StateRank = ((orderItem.State == State.Waiting) ? 10 : ((orderItem.State == State.Processing) ? 20 : ((orderItem.State == State.Complete) ? 30 : 40))),
                        Quantity = orderItem.Quantity,
                        DeliveredQuantity = orderItem.DeliveredQuantity,
                        Attachment = (string.IsNullOrEmpty(orderItem.Attachment) ? null : (this.ServerUrl + "/Uploads/" + orderItem.Attachment)),
                        CreateDate = DateManager.GetLocalDate(new DateTime?(orderItem.CreateDate)),
                        CompleteDate = DateManager.GetLocalDate(orderItem.CompleteDate),
                        CustomerName = orderItem.Order.User.Name,
                        CustomerId = orderItem.Order.User.Id,
                        TypeName = (orderItem.ItemTypeId.HasValue ? orderItem.ItemType.Name : orderItem.TypeText),
                        CurrencyId = orderItem.CurrencyId,
                        CurrencyName = orderItem.CurrencyId > new byte() ? ((Enums.CurrencyEnum)orderItem.CurrencyId).ToString() : "",
                        Order = new
                        {
                            orderItem.Order.Id,
                            orderItem.Order.SerialNumber
                        }
                    });
                }
                else
                {
                    result = this.CreatedAtRoute("DefaultApi", new
                    {
                        id = orderItem.Id
                    }, new
                    {
                        Id = orderItem.Id,
                        OrderId = orderItem.OrderId,
                        TypeId = ((orderItem.ItemType == null) ? 0 : orderItem.ItemType.Id),
                        UnitId = ((orderItem.Unit == null) ? 0 : orderItem.Unit.Id),
                        TypeText = orderItem.TypeText,
                        UnitText = orderItem.UnitText,
                        Remarks = orderItem.Remarks,
                        Description = orderItem.Description,
                        State = orderItem.State,
                        StateRank = ((orderItem.State == State.Waiting) ? 10 : ((orderItem.State == State.Processing) ? 20 : ((orderItem.State == State.Complete) ? 30 : 40))),
                        Quantity = orderItem.Quantity,
                        DeliveredQuantity = orderItem.DeliveredQuantity,
                        Attachment = (string.IsNullOrEmpty(orderItem.Attachment) ? null : (this.ServerUrl + "/Uploads/" + orderItem.Attachment)),
                        CreateDate = DateManager.GetLocalDate(new DateTime?(orderItem.CreateDate)),
                        CompleteDate = DateManager.GetLocalDate(orderItem.CompleteDate),
                        CustomerName = orderItem.Order.User.Name,
                        CustomerId = orderItem.Order.User.Id,
                        BuyingPrice = orderItem.BuyingPrice,
                        SellingPrice = orderItem.SellingPrice,
                        SupplierName = ((orderItem.Supplier != null) ? orderItem.Supplier.Name : ""),
                        SupplierId = ((orderItem.Supplier != null) ? orderItem.Supplier.Id : 0),
                        CurrencyId = orderItem.CurrencyId,
                        CurrencyName = orderItem.CurrencyId > new byte() ? ((Enums.CurrencyEnum)orderItem.CurrencyId).ToString() : "",
                        Order = new
                        {
                            orderItem.Order.Id,
                            orderItem.Order.SerialNumber
                        }
                    });
                }
            }
            return result;
        }

        [HttpDelete, Route("api/items/removeattachment/{id}")]
        public async Task<IHttpActionResult> RemoveAttachment(int id)
        {
            OrderItem orderItem = this.db.OrderItems.Find(new object[]
			{
				id
			});
            IHttpActionResult result;
            if (orderItem == null)
            {
                result = this.NotFound();
            }
            else
            {
                orderItem.Order = this.db.Orders.Find(new object[]
				{
					orderItem.OrderId
				});
                if (this.User.IsInRole(SystemRoles.Customer) && this.GetState(orderItem.OrderId).FinalStatus != State.Waiting)
                {
                    result = this.BadRequest("Order processing has started phase, call administrator for modification");
                }
                else
                {
                    string str = HostingEnvironment.MapPath("~/Uploads/");
                    if (!string.IsNullOrEmpty(orderItem.Attachment))
                    {
                        try
                        {
                            File.Delete(str + orderItem.Attachment);
                        }
                        catch
                        {
                        }
                    }
                    orderItem.Attachment = null;
                    this.db.Entry<OrderItem>(orderItem).State = EntityState.Modified;
                    try
                    {
                        await this.db.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        var method = MethodBase.GetCurrentMethod();
                        ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                        result = this.InternalServerError();
                        return result;
                    }
                    result = this.Ok();
                }
            }
            return result;
        }

        public async Task<IHttpActionResult> Delete(int id)
        {
            OrderItem orderItem = await this.db.OrderItems.FindAsync(new object[]
			{
				id
			});
            OrderItem orderItem2 = orderItem;
            IHttpActionResult result;
            try
            {
                if (orderItem2 == null)
                {
                    result = this.NotFound();
                }
                else
                {
                    if (this.User.IsInRole(SystemRoles.Customer))
                    {
                        if (this.GetState(orderItem2.OrderId).FinalStatus != State.Waiting)
                        {
                            result = this.BadRequest("Order processing has started phase, call administrator for modification");
                            return result;
                        }
                        if (orderItem2.Order.UserId != this.User.Identity.GetUserId())
                        {
                            result = this.NotFound();
                            return result;
                        }
                    }
                    this.db.OrderItems.Remove(orderItem2);
                    await this.db.SaveChangesAsync();
                    result = this.Ok(new
                    {
                        orderItem2.Id
                    });
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return result;
        }


        [ResponseType(typeof(void)), Route("api/items/exportreceipt/{id}")]
        public IHttpActionResult ExportReceipt(int id, List<int> ids)
        {
            var ordersList = this.db.Orders.Where(x => x.Items.Any(y => ids.Contains(y.Id)));
            List<int> ordersIdsList = ordersList.Select(x=>x.Id).ToList();
            var customersList = this.db.Users.Where(x => x.Orders.Any(y => ordersIdsList.Contains(y.Id)));
            //System.IO.File.AppendAllText(@"H:\hshome\rtmsadmin\procurement.rtms.com.eg\temp\logs\error.txt", "\n" + "77");
            //if (base.User.IsInRole(SystemRoles.Customer))
            //{
            //    //return this.NotFound();
            //}
            //Order order = this.db.Orders.Find(new object[]
			//{
			//	id
			//});
            //if (order == null)
            //{
            //    //return this.NotFound();
            //}
            //ApplicationUser customer = this.db.Users.Find(new object[]
			//{
			//	order.UserId
			//});

            var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            //If you have Physical file Read the fileStream and use it.
            string text = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss-ffffff") + ".xlsx";
            //text = HttpContext.Current.Server.MapPath("~/temp/" + text);
            text = @"H:\hshome\rtmsadmin\procurement.rtms.com.eg\temp\" + text;

            text = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss-ffffff") + ".xlsx";
            string fileName = text;
            text = HttpContext.Current.Server.MapPath("~/temp/" + text);

            Excel.SaveReceipt(ordersList.ToList(), ids, customersList.ToList(), text);

            //response.Content = new StreamContent(fileStream);
            //OR
            //Create a file on the fly and get file data as a byte array and send back to client
            //response.Content = new ByteArrayContent(System.IO.File.ReadAllBytes(text));//Use your byte array
            //response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            //response.Content.Headers.ContentDisposition.FileName = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss-ffffff") + ".xlsx";//your file Name- text.xls
            //response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/ms-excel");
            ////response.Content.Headers.ContentType  = new MediaTypeHeaderValue("application/octet-stream");
            //response.Content.Headers.ContentLength = System.IO.File.ReadAllBytes(text).Length;
            //response.StatusCode = System.Net.HttpStatusCode.OK;
            //string url = System.Web.HttpContext.Current.Request.UrlReferrer.ToString()+"api/items/downloadexcel?file="+ fileName;
            string url = ResolveServerUrl(VirtualPathUtility.ToAbsolute("~/temp/" + fileName), false);
            string siteUrl = ConfigurationManager.AppSettings["ServerSiteUrl"];
            string fileUrl = ConfigurationManager.AppSettings["ServerSiteUrl"] + fileName;
            return Json(new { Result = "OK", FileName = fileUrl });
            //return response;


            //string text = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss-ffffff") + ".xlsx";
            //text = HttpContext.Current.Server.MapPath("~/temp/" + text);
            //Excel.SaveReceipt(order,ids, customer, text);
            //HttpContext.Current.Response.Clear();
            //HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=WebStreamDownload.xlsx");
            //string s = Convert.ToBase64String(System.IO.File.ReadAllBytes(text));
            //System.IO.File.Delete(text);
            //HttpContext.Current.Response.Write(s);
            //HttpContext.Current.Response.Flush();
            //HttpContext.Current.Response.End();
            //return this.StatusCode(HttpStatusCode.NoContent);

            //int x = new int();
            //Order order = this.db.Orders.Find(new object[]
            //{
            //	id
            //});
            //if (order == null)
            //{
            //    return this.NotFound();
            //}
            //if (base.User.IsInRole(SystemRoles.Customer))
            //{
            //    return this.BadRequest("Only Administrators Allowed");
            //}
            ////if (this.GetState(id).FinalStatus != State.Complete)
            ////{
            ////    return this.BadRequest("Order not complete yet");
            ////}
            //ApplicationUser applicationUser = this.db.Users.Find(new object[]
            //{
            //	order.UserId
            //});
            //string text = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss-ffffff") + ".xlsx";
            //text = HttpContext.Current.Server.MapPath("~/temp/" + text);
            ////Excel.SaveInvoice(order, applicationUser, text);
            //Excel.SaveInvoice(order,ids,applicationUser, text);
            //try
            //{
            //    MailHelper.SendMail(applicationUser.Email, "RTMS Invoice", "Dear Customer, attached is the invoice for order#" + order.SerialNumber, text);
            //}
            //catch (Exception)
            //{
            //    return this.StatusCode(HttpStatusCode.BadRequest);
            //}
            //File.Delete(text);
            //return this.StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(int id)
        {
            return this.db.Orders.Count((Order e) => e.Id == id) > 0;
        }


        [HttpGet, AllowAnonymous]
        public void DownloadExcel(string file)
        {
            try
            {
                //string fullPath = Path.Combine("~/temp/", file);
                string fullPath = HttpContext.Current.Server.MapPath("~/temp/" + file);
                string fullPath1 = ResolveServerUrl(VirtualPathUtility.ToAbsolute("~/temp/" + file), false);
                DownloadExcelFile(fullPath);
            }
            catch (Exception exception)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(exception, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
            }

        }

        public bool DownloadExcelFile(string filePath)
        {
            bool downloadStatus = false;
            try
            {
                FileInfo fileInfo = new FileInfo(filePath);
                HttpResponse Response = System.Web.HttpContext.Current.Response;
                Response.Clear();
                Response.ClearHeaders();
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileInfo.Name);
                Response.ContentType = "application/ms-excel";
                Response.TransmitFile(filePath);
                downloadStatus = true;
            }
            catch (Exception exception)
            {
                #region Log The Exception
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(exception, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                #endregion
            }
            return downloadStatus;
        }

        public string ResolveServerUrl(string serverUrl, bool forceHttps)
        {
            string newUrl = string.Empty;
            try
            {
                if (serverUrl.IndexOf("://") > -1)
                    return serverUrl;

                newUrl = serverUrl;
                Uri originalUri = System.Web.HttpContext.Current.Request.Url;
                newUrl = (forceHttps ? "https" : originalUri.Scheme) +
                    "://" + originalUri.Authority + newUrl;
                return newUrl;
            }
            catch (Exception exception)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(exception, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
            }
            return newUrl;
        }

        [HttpGet, Route("api/items/currencies"), AllowAnonymous]
        public IHttpActionResult Get()
        {
            return this.Ok(GetSelectListItems());
        }

        private IEnumerable<SelectListItemViewModel> GetSelectListItems()
        {
            var selectList = new List<SelectListItemViewModel>();

            // Get all values of the Industry enum
            var enumValues = Enum.GetValues(typeof(RTMS.Common.Enums.CurrencyEnum)) as RTMS.Common.Enums.CurrencyEnum[];
            if (enumValues == null)
                return null;

            foreach (var enumValue in enumValues)
            {
                // Create a new SelectListItem element and set its 
                // Value and Text to the enum value and description.
                //if (enumValue == Common.Enums.CurrencyEnum.Supervisor || enumValue == Common.Enums.CurrencyEnum.Customer)
                //{
                selectList.Add(new SelectListItemViewModel
                {
                    value = (byte)enumValue,
                    // GetIndustryName just returns the Display.Name value
                    // of the enum - check out the next chapter for the code of this function.
                    text = enumValue.ToString()
                });
                //}
            }

            return selectList;
        }
    }
}
