using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using RTMS.Common;
using RTMS.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Reflection;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using RTMS.Core;

namespace RTMS.Controllers
{
    [Authorize, EnableCors("*", "*", "GET, POST, PUT, DELETE, OPTIONS", SupportsCredentials = true), RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private class ExternalLoginData
        {
            public string LoginProvider
            {
                get;
                set;
            }

            public string ProviderKey
            {
                get;
                set;
            }

            public string UserName
            {
                get;
                set;
            }

            public IList<Claim> GetClaims()
            {
                IList<Claim> list = new List<Claim>();
                list.Add(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", this.ProviderKey, null, this.LoginProvider));
                if (this.UserName != null)
                {
                    list.Add(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", this.UserName, null, this.LoginProvider));
                }
                return list;
            }

            public static AccountController.ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }
                Claim claim = identity.FindFirst("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier");
                if (claim == null || string.IsNullOrEmpty(claim.Issuer) || string.IsNullOrEmpty(claim.Value))
                {
                    return null;
                }
                if (claim.Issuer == "LOCAL AUTHORITY")
                {
                    return null;
                }
                return new AccountController.ExternalLoginData
                {
                    LoginProvider = claim.Issuer,
                    ProviderKey = claim.Value,
                    UserName = identity.FindFirstValue("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name")
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                if (strengthInBits % 8 != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }
                byte[] array = new byte[strengthInBits / 8];
                AccountController.RandomOAuthStateGenerator._random.GetBytes(array);
                return HttpServerUtility.UrlTokenEncode(array);
            }
        }

        private const string LocalLoginProvider = "Local";

        private ApplicationUserManager _userManager;

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat
        {
            get;
            private set;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return this._userManager ?? base.Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                this._userManager = value;
            }
        }

        private IAuthenticationManager Authentication
        {
            get
            {
                return base.Request.GetOwinContext().Authentication;
            }
        }

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            this.UserManager = userManager;
            this.AccessTokenFormat = accessTokenFormat;
        }

        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            this.Authentication.SignOut(new string[]
			{
				"Cookies"
			});
            return this.Ok();
        }

        [AllowAnonymous, Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            IHttpActionResult result;
            try
            {
                if (!this.ModelState.IsValid)
                {
                    result = this.BadRequest(this.ModelState);
                }
                else
                {
                    ApplicationUser user = new ApplicationUser
                    {
                        UserName = model.Email,
                        Email = model.Email
                    };
                    IdentityResult identityResult = await this.UserManager.CreateAsync(user, model.Password);
                    if (!identityResult.Succeeded)
                    {
                        result = this.GetErrorResult(identityResult);
                    }
                    else
                    {
                        result = this.Ok();
                    }
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }

            return result;
        }

        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            IHttpActionResult result;
            try
            {
                if (!this.ModelState.IsValid)
                {
                    result = this.BadRequest(this.ModelState);
                }
                else
                {
                    IdentityResult identityResult = await this.UserManager.ChangePasswordAsync(this.User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (!identityResult.Succeeded)
                    {
                        result = this.GetErrorResult(identityResult);
                    }
                    else
                    {
                        result = this.Ok();
                    }
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return result;
        }

        [Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            IHttpActionResult result;
            try
            {
                if (!this.ModelState.IsValid)
                {
                    result = this.BadRequest(this.ModelState);
                }
                else
                {
                    IdentityResult identityResult = await this.UserManager.AddPasswordAsync(this.User.Identity.GetUserId(), model.NewPassword);
                    if (!identityResult.Succeeded)
                    {
                        result = this.GetErrorResult(identityResult);
                    }
                    else
                    {
                        result = this.Ok();
                    }
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }
            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this._userManager != null)
            {
                this._userManager.Dispose();
                this._userManager = null;
            }
            base.Dispose(disposing);
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return this.InternalServerError();
            }
            if (result.Succeeded)
            {
                return null;
            }
            if (result.Errors != null)
            {
                foreach (string current in result.Errors)
                {
                    base.ModelState.AddModelError("", current);
                }
            }
            if (base.ModelState.IsValid)
            {
                return this.BadRequest();
            }
            return this.BadRequest(base.ModelState);
        }


        [HttpGet]
        public IHttpActionResult Get()
        {

            ApplicationDbContext db = new ApplicationDbContext();
            return this.Ok(GetSelectListItems());
        }

        private IEnumerable<SelectListItemViewModel> GetSelectListItems()
        {
            var selectList = new List<SelectListItemViewModel>();

            try
            {
                // Get all values of the Industry enum
                var enumValues = Enum.GetValues(typeof(RTMS.Common.Enums.SystemRoleEnum)) as RTMS.Common.Enums.SystemRoleEnum[];
                if (enumValues == null)
                    return null;

                foreach (var enumValue in enumValues)
                {
                    // Create a new SelectListItem element and set its 
                    // Value and Text to the enum value and description.
                    if (enumValue == Common.Enums.SystemRoleEnum.Supervisor || enumValue == Common.Enums.SystemRoleEnum.Customer || ((this.User.IsInRole(SystemRoles.Admin)||this.User.IsInRole(SystemRoles.Super)) && enumValue == Common.Enums.SystemRoleEnum.Admin))
                    {
                        selectList.Add(new SelectListItemViewModel
                        {
                            value = (byte)enumValue,
                            // GetIndustryName just returns the Display.Name value
                            // of the enum - check out the next chapter for the code of this function.
                            text = enumValue.ToString()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                var method = MethodBase.GetCurrentMethod();
                ExceptionHandler.WriteLog(ex, string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())));
                throw;
            }

            return selectList;
        }


    }
}
